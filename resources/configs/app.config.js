// noinspection JSAnnotator
import environment from '../environment/env';

export default Object.freeze({
    admin_prefix: environment.admin_prefix,
    base_url: environment.base_url,
    status: {
    pending: 1,
        approved: 2,
        rejected: 3,
        paid: 4,
        unpaid: 5,
        purchase_drs: 6,
        preparation: 7,
        in_transit: 8,
        ready_to_pick_up_drs: 9,
        delivered: 10,
        walk_in: 11,
        use_credits: 12,
        exceed_credit_limit: 13,
        deleted: 14,
        completed: 15,
        pre_order: 16,
}

});