<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
            body{
                width: 800px;
                height: 200px;
                padding-top: 94px;
                margin-left: 0;
                font-family: "Verdana", sans-serif;
                font-size: 12px;
                font-weight: bolder;
            }
            .t-right{
                text-align: right;
            }
            .t-left{
                text-align: left;
            }
            .t-center{
                text-align: center;
            }
            .details{
                font-size: 11px;
            }
            .total{
                font-size: 12px;
            }
            p{
                margin: 0;
                width: 470px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .name-el{
                margin: 0;
                width: 470px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .qty{
                min-width: 80px;
            }
        </style>
    </head>
    <body>
        <table class="personal">
            <tr>
                <td width="500px" style="padding-left: 30px;">
                    {{-- <p class="name-el">{{$response->branch->name}}</p> --}}
                    <p class="name-el">{{$response->customer->branch->name}}</p>
                </td>
                <td class="t-left" style="padding-left: 40px;">
                    {{$response->created_at->format('M-d-Y')}}
                </td>
            </tr>
            <tr>
                <td width="500px" style="padding-left: 30px; padding-top: 9px;">
                    {{-- {{$response->customer->address}} --}}
                </td>
                <td width="110px" class="t-right" style="padding-left: 50px; padding-top: 9px;">
                    {{$response->tin_number}}
                </td>
            </tr>
        </table>
        <div style="height: 365px;">
            <table class="details" style="margin-top: 45px;">
                @foreach($response->hasManyOrderReference as $key => $res)
                <tr>
                    <td width="68px" class="t-left qty">
                        {{$res->quantity}} {{$res->item->package->name}}
                    </td>
                    <td width="470px" style="padding-left: 25px;">
                        {{$res->item->name}}
                    </td>
                    <td width="72px">
                        {{number_format($res->amount,2)}}
                    </td>
                    <td width="72px">
                        {{number_format($res->total,2)}}
                    </td>
                </tr>
                @endforeach

                @if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <tr>
                        <td width="68px" class="t-left">
                            {{$res->quantity}} {{$res->item->package->name}}
                        </td>
                        <td width="470px" style="padding-left: 25px;">
                            <p>{{$res->item->name}}</p>
                        </td>
                        <td width="72px">
                            {{number_format($res->amount,2)}}
                        </td>    
                        <td width="72px">
                            {{number_format($res->total,2)}}
                        </td>
                    </tr>
                @endforeach
                @endif
                {{-- <tr>
                    <td width="68px" class="t-left qty">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{$res->quantity}} {{$res->item->package->name}}
                        @endforeach

                        @if($response->returnOrderHeader)
                        @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                            {{$res->quantity}} {{$res->item->package->name}}
                        @endforeach
                        @endif
                    </td>
                    <td width="470px" style="padding-left: 25px;">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            <p>{{$res->item->name}}</p>
                        @endforeach

                        @if($response->returnOrderHeader)
                        @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                            <p>{{$res->item->name}}</p>
                        @endforeach
                        @endif
                    </td>
                    <td width="72px">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{number_format($res->amount,2)}}
                        @endforeach

                        @if($response->returnOrderHeader)
                            @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                                {{number_format($res->amount,2)}}
                            @endforeach
                        @endif
                    </td>
                    <td width="72px">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{number_format($res->total,2)}}
                        @endforeach

                        @if($response->returnOrderHeader)
                            @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                                {{number_format($res->total,2)}}
                            @endforeach
                        @endif
                    </td>
                </tr> --}}
                
            </table>
        </div>
        <table class="total">
            <tr>
                <td width="68px"></td>
                <td width="550px" class="t-right">GRAND TOTAL</td>
                <td width="72px" style="padding-left: 20px;">{{number_format($response->grand_total,2)}}</td>
            </tr>
        </table>
    </body>
</html>
