<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
            body{
                width: 453px;
                height: 400px;
                padding-top: 37px;
                margin-left: 0;
                font-family: "Verdana", sans-serif;
                font-size: 12px;
                font-weight: bolder;
            }
            .t-right{
                text-align: right;
            }
            .t-left{
                text-align: left;
            }
            .t-center{
                text-align: center;
            }
            .details{
                font-size: 11px;
            }
            p{
                margin: 0;
                width: 370px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .name-el{
                margin: 0;
                width: 270px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                padding-left: 15px;
            }
            .qty{
                min-width: 60px;
            }
        </style>
    </head>
    <body>
        <table class="personal">
            <tr>
                <td width="302px"><p class="name-el">{{$response->customer->branch->name}}</p></td>
                <td width="110px" class="t-right" style="padding-left: 35px;">
                    {{$response->created_at->format('m-d-Y')}}
                </td>
            </tr>
        </table>
        <table class="details" style="margin-top: 35px;">
            {{-- <tr>
                <td width="40px" class="qty t-left">
                    @foreach($response->hasManyOrderReference as $key => $res)
                        <p>{{$res->quantity}} {{$res->item->package->name}}</p>
                    @endforeach
                </td>
                <td width="405px" style="padding-left: 25px;">
                     @foreach($response->hasManyOrderReference as $key => $res)
                        <p>{{$res->item->name}}</p>
                    @endforeach
                </td>
            </tr> --}}
            @foreach($response->hasManyOrderReference as $key => $res)
                <tr>
                    <td width="40px" class="qty t-left">
                        {{-- @foreach($response->hasManyOrderReference as $key => $res) --}}
                            {{$res->quantity}} {{$res->item->package->name}}
                        {{-- @endforeach --}}
                    </td>
                    <td width="405px" style="padding-left: 25px;">
                         {{-- @foreach($response->hasManyOrderReference as $key => $res) --}}
                            <p>{{$res->item->name}}</p>
                        {{-- @endforeach --}}
                    </td>
                </tr>
            @endforeach
        </table>
    </body>
</html>
