<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
	<title>Lightforce Corporation</title>

	<style>
		@page{
            margin: 0;
        }
        body {
            font-family: "Verdana";
            font-weight: bolder;
        }
        .header{
        width: 90%;
        height: 60px;
        }
        .invoice-head{
        width:90%;
        /* font-family: Lucida Console;
        font-size: 10pt; */
        margin-top: 30px;
        }
        .personal-info{
        width:75%;
        float:left;
        }
        .sold-to{
        margin-left: 32px;
        text-indent: 35px;
        /*margin-left: 138px; */
        /* margin-left: 221px; */
        margin-top: 33px;
        }
        .address{
        margin-left: 32px;
        text-indent: 35px;
        /*margin-left: 138px;*/ 
        /* margin-left: 221px; */
        margin-top: -9px;
        }
        .date-TIN{
        width:25%;
        float:left;
        }
        .date{
        margin-left: 44px;
        margin-top: 35px;
        }
        .TIN-num{
        margin-left: 44px;
        margin-top: -11px;
        }

        .invoice-body{
        width:90%;
        /* font-family: Lucida Console;
        font-size: 10pt; */
        font-size: 12pt;
        margin-top: 80px;
        }
        .quantity{
        width:15%;
        float:left;
        }
        .quantity-num{
        margin-top: 29px;
        line-height: 0%;
        font-size: 10pt;
        text-align: center;
        }
        .desc{
        width:55%;
        float:left;
        }
        .description{
        margin-top: 29px;
        margin-left: -10px;
        text-indent: 30px;
        /* margin-left: 78px; */
        /* margin-left: 170px; */
        line-height: 0%;
        font-size: 10pt;
        }
        .unitPrice{
            width:15%;
            float:left;
        }
        .unitPrice_{
            margin-top: 29px;
            margin-left: 24px;
            line-height: 0%;
            font-size: 10pt;
        }

        .amount{
            width:15%;
            float:left;
        }
        .amount_{
            margin-top: 29px;
            margin-left: 39px;
            line-height: 0%;
            font-size: 10pt;
        }
        .invoice-footer{
            width:100%;
            margin-top: 479px;
            margin-left: -20px;
            font-size: 10pt;
        }
        .footer_title{
            margin-top: -49px;
            margin-left: 280px;
            width:30%;
        }
        .footer_amnt{
            margin-top: -60px;
            margin-left: 534px;
            width:20%;
        }

	</style>

</head>
<body>
	<div class="header"></div>
	<div class="invoice-head">
    	<div class="personal-info">
    		<div class="sold-to">
        	<p>{{$response->customer->name}}</p>
        	</div>
        	<div class="address">
        	<p>{{$response->customer->address}}</p>
        	</div>
    	</div>

    	<div class="date-TIN">
    		<div class="date">
        	<p>{{$response->created_at->format('M-d-Y')}}</p>
            <p>&nbsp</p>
        	</div>
        	<div class="TIN-num">
        	<p>{{$response->tin_number}}</p>
        	</div>
    	</div>
	</div>

	<div class="invoice-body">
		<div class="quantity">	
			<div class="quantity-num">
                @foreach($response->hasManyOrderReference as $key => $res)
					<p>{{$res->quantity}}</p>
                @endforeach

				@if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <p>{{$res->quantity}}</p>
                @endforeach
            	@endif
        	</div>
    	</div>
    	<div class="desc">
    		<div class="description">
                @foreach($response->hasManyOrderReference as $key => $res)
                    <p>{{$res->item->name}}</p>
                @endforeach

				@if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <p>{{$res->item->name}}</p>
                @endforeach
           		@endif
        	</div>
    	</div>
		<div class="unitPrice">
            <div class="unitPrice_">
            @foreach($response->hasManyOrderReference as $key => $res)
                <!-- <p>{{number_format($res->amount,2)}}</p> -->
            @endforeach

            @if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <!-- <p>{{number_format($res->amount,2)}}</p> -->
                @endforeach
            @endif
            </div>
        </div>
        <div class="amount">
            <div class="amount_">
            @foreach($response->hasManyOrderReference as $key => $res)
                <!-- <p>{{number_format($res->total,2)}}</p> -->
            @endforeach

            @if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <!-- <p>{{number_format($res->total,2)}}</p> -->
                @endforeach
            @endif
            </div>
        </div>
    </div>

	<div class="invoice-footer" align="right">
	<div class="footer_title" align="right">
            <!-- <p>Discount:</p> -->
			<!-- <p>Discount: {{number_format($response->discount,2)}}</p> -->
            <!-- <p>TOTAL AMOUNT DUE:</p> -->
        </div>
		<div class="total-amnt">
			<!-- <p>{{number_format($response->discount,2)}}</p> -->
			<!-- <p>{{number_format($response->grand_total,2)}}</p> -->
		</div>
	</div>
    
    <script type="text/javascript">

    window.print();
  
    </script>
</body>
</html>