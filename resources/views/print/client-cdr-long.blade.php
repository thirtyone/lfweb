<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
	<title>Lightforce Corporation</title>

	<style>
		@page{
            margin: 0;
        }
		body {
            font-family: "Verdana";
            font-weight: bolder;
        }
		.header{
		width: 90%;
		height: 20px;
		}
		.invoice-head{
    	width:90%;
    	/* font-family: Lucida Console;
    	font-size: 10pt; */
    	margin-top: 30px;
		}
		.personal-info{
    	width:75%;
    	float:left;
		}
		.sold-to{
		margin-left: 32px;
        /* margin-left: 138px; */
        /* margin-left: 221px; */
		margin-top: 43px;
		}
		.address{
		margin-left: 32px;
        /* margin-left: 138px; */
        /* margin-left: 221px; */
		margin-top: -6px;
		}
		.date-TIN{
    	width:25%;
    	float:left;
		}
		.date{
		margin-left: 44px;
		margin-top: 41px;
		}
		.TIN-num{
		margin-left: 44px;
		margin-top: -4px;
		}

		.invoice-body{
    	width:90%;
    	/* font-family: Lucida Console;
    	font-size: 10pt; */
        font-size: 9pt;
    	margin-top: 80px;
		}
		.quantity{
    	width:15%;
    	float:left;
		}
		.quantity-num{
		margin-top: 32px;
        line-height: 0%;
		}
		.desc{
    	width:85%;
    	float:left;
		}
		.description{
		margin-top: 32px;
		margin-left: -10px;
        /* margin-left: 78px; */
        /* margin-left: 170px; */
        line-height: 0%;
		}
		.invoice-footer{
            width:90%;
            font-family: Lucida Console;
            font-size: 10pt;
            margin-top: 334px;
            margin-left: -45px;
        }
        .total-amnt{
            margin-top: -2px;
            margin-left: 65px;
        }

	</style>

</head>
<body>
	<div class="header"></div>
	<div class="invoice-head">
    	<div class="personal-info">
    		<div class="sold-to">
        	<p>{{$response->customer->name}}</p>
        	</div>
        	<div class="address">
        	<p>{{$response->customer->address}}</p>
        	</div>
    	</div>

    	<div class="date-TIN">
    		<div class="date">
        	<p>{{$response->created_at}}</p>
        	</div>
        	<div class="TIN-num">
        	<p>{{$response->tin_number}}</p>
        	</div>
    	</div>
	</div>

	<div class="invoice-body">
		<div class="quantity">	
			<div class="quantity-num">
                @foreach($response->hasManyOrderReference as $key => $res)
					<p>{{$res->quantity}}</p>
                @endforeach

				@if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <p>{{$res->quantity}}</p>
                @endforeach
            	@endif
        	</div>
    	</div>
    	<div class="desc">
    		<div class="description">
                @foreach($response->hasManyOrderReference as $key => $res)
                    <p>{{$res->item->name}}</p>
                @endforeach

				@if($response->returnOrderHeader)
                @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                    <p>{{$res->item->name}}</p>
                @endforeach
           		@endif
        	</div>
    	</div>
    </div>

	<div class="invoice-footer" align="right">
		<div class="total-amnt">
			<p>{{number_format($response->grand_total,2)}}</p>
		</div>
	</div>
    
    <script type="text/javascript">

    window.print();
  
    </script>
</body>
</html>