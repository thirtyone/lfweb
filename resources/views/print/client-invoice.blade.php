<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
            body{
                width: 800px;
                height: 200px;
                padding-top: 75px;
                margin-left: 0;
                font-family: "Verdana", sans-serif;
                font-size: 12px;
                font-weight: bolder;
            }
            .t-right{
                text-align: right;
            }
            .t-left{
                text-align: left;
            }
            .t-center{
                text-align: center;
            }
            .details{
                font-size: 12px;
            }
            .total{
                font-size: 12px;
            }
            .details td{
                line-height: 2;
            }
            p{
                margin: 0;
                width: 290px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .name-el{
                margin: 0;
                width: 470px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                padding-left: 20px;
            }
            .address-el{
                margin: 0;
                width: 470px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                padding-left: 25px;
            }
            .qty{
                min-width: 50px;
            }
        </style>
    </head>
    <body>
        <table class="personal">
            <tr>
                <td class="t-left" style="padding-left: 535px;">
                    {{$response->created_at->format('m-d-Y')}}
                </td>
            </tr>
            <tr>
                <td width="500px" style="padding-left: 33px; padding-top: 7px;">
                    <p class="name-el">{{$response->customer->name}}</p>
                </td>
            </tr>
            <tr>
                <td width="500px" style="padding-left: 40px; padding-top: 9px;">
                    <p class="address-el">{{$response->customer->address}}</p>
                </td>
            </tr>
        </table>
        <div style="height: 234px;">
            <table class="details" style="margin-top: 57px;">
                <tr>
                    <td style="max-width:50px; width: 50px;" class="t-left qty">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{$res->quantity}}<br>
                        @endforeach

                        {{-- @if($response->returnOrderHeader)
                            @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                                {{$res->quantity}}
                            @endforeach
                        @endif --}}
                    </td>
                    <td style="width:50px;" class="t-left">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{$res->item->package->name}}<br>
                        @endforeach

                        {{-- @if($response->returnOrderHeader)
                            @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                                {{$res->item->package->name}}
                            @endforeach
                        @endif --}}
                    </td>
                    <td width="300px">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            <p>{{$res->item->name}}</p>
                        @endforeach

                        {{-- @if($response->returnOrderHeader)
                            @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                                <p>{{$res->item->name}}</p>
                            @endforeach
                        @endif --}}
                    </td>
                    <td width="100px">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{$res->item->hasOneItemColor['name'] != null ? $res->item->hasOneItemColor['name'] : 'N/A'}}<br>
                        @endforeach
                        {{-- dd($response->hasManyOrderReference[0]->item->hasOneItemColor->name); --}}
                    </td>
                    <td width="45px" class="t-right" style="padding-right: 25px;">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{number_format($res->amount,2)}}
                        @endforeach

                        {{-- @if($response->returnOrderHeader)
                        @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                            {{$res->amount}}
                        @endforeach
                        @endif --}}
                    </td>
                    <td width="70px" class="t-right" style="padding-left: 50px;">
                        @foreach($response->hasManyOrderReference as $key => $res)
                            {{number_format($res->total,2)}}
                        @endforeach

                       {{--  @if($response->returnOrderHeader)
                        @foreach($response->returnOrderHeader->hasManyReturnOrderDetail as $key => $res)
                            <p>{{$res->total}}</p>
                        @endforeach
                        @endif --}}
                    </td>
                </tr>
            </table>
        </div>
        <table class="total">
            <tr>
                <td width="395px"></td>
                <td width="160px" class="t-right" style="padding-right: 25px;">Discount</td>
                <td width="120px" class="t-right" style="">{{number_format($response->discount,2)}}</td>
            </tr>
            <tr>
                <td width="395px"></td>
                <td width="160px" class="t-right" style="padding-right: 25px;"></td>
                <td width="120px" class="t-right" style="padding-top: 10px;">{{number_format($response->total,2)}}</td>
            </tr>
            <tr>
                <td width="395px"></td>
                <td width="160px" class="t-right" style="padding-right: 25px;"></td>
                <td width="120px" class="t-right" style="padding-top: 10px;">{{number_format($response->vat,2)}}</td>
            </tr>
            <tr>
                <td width="395px"></td>
                <td width="160px" class="t-right" style="padding-right: 25px;"></td>
                <td width="120px" class="t-right" style="padding-top: 10px;">{{number_format($response->grand_total,2)}}</td>
            </tr>
        </table>
    </body>
</html>
