<!DOCTYPE html>
<html>
<head>
    <title>Lightforce Corporation</title>

    <style>
        @page {
            margin: 0;
        }


        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
        tr.header {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div style="width: 100%; max-width: 1024px;">
    <table style="width:100%;">
        <tr>
            <td colspan="8">
                <center><img src="{{url('images/logo_white.png')}}" width="240px" style="margin-top: 10px; margin-bottom: 10px;"></center>
            </td>
        </tr>
        <tr class="header">
            <td>Name</td>
            <td>SKU</td>
            <td>Image</td>
            <td>Description</td>
            <td>Discount</td>
            <td>Quantity</td>
            <td>Unit Price</td>
            <td>Total Price</td>
        </tr>
        @foreach($response->hasManyOrderReference as $key => $res)
            <tr>
                <td>{{$res->item->name}}</td>
                <td>{{$res->item->sku}}</td>

                <td>
                    @if($res->item->hasOneItemColor)
                        <center><img src="{{$res->item->hasOneItemColor->path.$res->item->hasOneItemColor->file_name}}" width="100px"></center>
                    @endif
                </td>
                <td>{{$res->item->description}}</td>
                <td>{{$res->discount}}%</td>
                <td>{{$res->quantity}}</td>
                <td>{{number_format($res->amount,2)}}</td>
                <td>{{number_format($res->total,2)}}</td>
            </tr>
        @endforeach
        <tr style="height: 30px;">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

        </tr>
        <tr>
        <td></td>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total Discount</td>
            <td>{{number_format($response->discount,2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total Sales</td>
            <td>{{number_format($response->total,2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Vat</td>
            <td>{{number_format($response->vat,2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Grand Total</td>
            <td>{{number_format($response->grand_total,2)}}</td>

        </tr>
    </table>
</div>
<script type="text/javascript">
    <!--
    window.print();
    //-->
</script>
</body>
</html>