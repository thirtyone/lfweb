<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>Client Name</th>
        <th>Invoice No.</th>
        <th>Invoice Amount</th>
        <th>Credit Limit</th>
        @if(Auth::guard('admin')->user()->admin_role_id == 1)
            <th>Total Cost</th>
        @endif
        <th>Paid Amount</th>
        <th>Balance</th>
    </tr>
    </thead>
    <tbody>

    @foreach($response as $res)

        <tr>
            <td>{{$res['created_at']}}</td>
            <td>{{$res['user']['name']}}</td>
            <td>{{$res['invoice_number']}}</td>
            <td>{{number_format($res['grand_total'],2)}}</td>

            <td>{{ $res['user']['credit_limit'] != '' ? number_format($res['user']['credit_limit'], 2, '.', ',') : '0.00'}} </td>

            @if(Auth::guard('admin')->user()->admin_role_id == 1)
                <td>
                    {{   number_format($res['has_many_order_detail'][0]['quantity'] * $res['has_many_order_detail'][0]['item']['has_one_supplier_order_detail']['amount'], 2, '.', ',')}}
                </td>
            @endif
            <td>{{$res['user']['payment_terms'] != 0 ?number_format($res['payment_status_option_id'] != 5 ? $res['grand_total'] : 0,2) : '0.00'}}</td>
            <td>{{$res['user']['payment_terms'] != 0 ?number_format($res['payment_status_option_id'] == 5 ? $res['grand_total'] : 0,2) : '0.00'}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
