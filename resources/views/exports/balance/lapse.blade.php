<table>
    <thead>
    <tr>
            <th>Client Name</th>
            <th>Invoice No.</th>
            <th>Invoice Date</th>
            <th>Invoice Due </th>
            <th>Amount</th>
            <th>Balance</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->user->name}}</td>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->created_at}}</td>
            <td>{{date("Y-m-d", strtotime($res->due_date))}} </td>
            <td>{{number_format($res->grand_total, 2, '.', ',')}}</td>
            <td>{{$res->user->payment_terms ? $res->payment_status_option_id == 5  ? $res->grand_total : '0.00' : '0.00'}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
