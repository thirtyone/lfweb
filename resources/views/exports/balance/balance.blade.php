<table>
    <thead>
    <tr>
            <th>Date</th>
            <th>Client Name</th>
            <th>Invoice No.</th>
            <th>Invoice Amount</th>
            <th>Credit Limit</th>
            <th>Paid Amount</th>
            <th>Balance</th>
            
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->created_at}}</td>
            <td>{{$res->user->name}}</td>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->total}}</td>
            <td>{{$res->user->credit_limit ? $res->user->credit_limit : 'NA'}}</td>
            <td>{{$res->payment_status_option_id != 5 ? $res->total : '0.00'}}</td>
            <td>{{$res->payment_status_option_id == 5 ? $res->total : '0.00'}}</td>
        </tr>
    @endforeach
    </tbody>
</table>