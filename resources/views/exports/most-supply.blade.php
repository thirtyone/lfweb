<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>SKU</th>
        <th>Quantity</th>
        
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->sku}}</td>
            <td>{{$res->has_many_branch_item_detail_count_quantity}}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>