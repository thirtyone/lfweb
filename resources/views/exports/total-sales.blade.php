<table>
    <thead>
    <tr>
        <th>Branch</th>
        <th>Gross</th>
        <th>Net</th>
        
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->has_many_order_header_count_grand_total ? number_format($res->has_many_order_header_count_grand_total, 2) : '0.00'}}</td>
            <td>{{$res->has_many_order_header_count_total ? number_format($res->has_many_order_header_count_total, 2) : '0.00'}}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>