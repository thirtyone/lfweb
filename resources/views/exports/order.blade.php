<table>
    <thead>
    <tr>
        <th>Item Name</th>
        <th>Branch</th>
        <th>Date / Time</th>
        <th>Quantity</th>
       
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->item->name ? $res->item->name : 'NA'}}</td>
            <td>{{$res->orderHeader->branch->name}}</td>
            <td>{{$res->orderHeader->string_date}}</td>
            <td>{{$res->quantity}}</td>
        </tr>
    @endforeach
    </tbody>
</table>