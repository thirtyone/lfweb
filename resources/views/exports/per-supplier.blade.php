<table>
    <thead>
    <tr>
            <th>Invoice</th>
            <th>Invoice Date</th>
            <th>Payment Terms</th>
            <th>Supplier Name</th>
            <th>Quantity</th>
            <th>Unit</th>
            <th>Item Name</th>
            <th>Unit Cost</th>
            <th>Amount</th>
          
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->supplier_invoice_number}}</td>
            <td>{{$res->supplier_invoice_date}}</td>
            <td>{{$res->supplier_payment_terms}}</td>
            <td>{{$res->supplier_name}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->package_id}}</td>
            <td>{{$res->package_id == '1' ? 'PC' : 'METER'}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->item_cost}}</td>
            <td>{{$res->amount}}</td>

            
           
        </tr>
    @endforeach
    </tbody>
</table>