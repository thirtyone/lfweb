<table>
    <thead>
    <tr>
        <th>First Invoice</th>
        <th>Last Invoice</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Item Name</th>
        <th>Average Cost</th>
        <th>Total Amount</th>
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->first_invoice_date}}</td>
            <td>{{$res->last_invoice_date}}</td>
            <td>{{$res->new_quantity}}</td>
            <td>{{$res->package_id == 1 ? 'PC' : 'METER'}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->avg_cost}}</td>
            <td>{{$res->amount}}</td>
            <th>-</th>
        </tr>
    @endforeach
    </tbody>
</table>