<table>
    <thead>
    <tr>
        <th>Client</th>
        <th>Item name</th>
        <th>Color</th>
        <th>Quantity </th>
        <th>Unit </th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->client_name}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->color_name ? $res->color_name : 'N/A'}}</td>
            <td>{{$res->orderQuantity}}</td>
            <td>{{$res->package_id == '1' ? 'PC' : 'METER'}}</td>
            <td>{{number_format($res->invoiceAmount, 2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>