<table>
    <thead>
    <tr>
        <th>Delivery Number</th>
        <th>Invoice Number</th>
        <th>Customer Name</th>
        <th>Branch</th>
        <!-- <th>Total</th> -->
        <th>Date Time</th>
        <th>Status</th>
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $key => $res)
        <tr>
            <td>{{$res->order_number}}</td>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->customer->name}}</td>
            <td>{{$res->branch->name}}</td>
            <!-- <td>

                @if($res->order_type_id == 1)
                    {{$res->grand_total}}
                @endif
                @if($res->order_type_id == 3)
                    {{$res->grand_total}}
                @endif

            </td> -->
            <td>{{$res->created_at}}</td>
            <td>{{$res->statusOption->name}}</td>
            <td>-</td>
        </tr>
    @endforeach
    </tbody>
</table>