<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>SKU</th>
        <th>Item Code</th>
        <th>Warehouse</th>
        <th>Color</th>
        <th>Quantity</th>
        <th>SRP</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->sku}}</td>
            <td>{{$res->code}}</td>
            <td>{{$res->branch_name}}</td>
            <td>{{$res->colors}}</td>
            <td>{{$res->has_many_branch_item_detail_count_quantity}}</td>
            <td>{{$res->srp}}</td>
        </tr>
    @endforeach
    </tbody>
</table>