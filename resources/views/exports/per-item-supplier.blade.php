<table>
    <thead>
    <tr>
            <th>Invoice</th>
            <th>Invoice Date</th>
            <th>Supplier Name</th>
            <th>Item</th>
            <th>Quantity </th>
            <th>Unit </th>
            <th>Unit Cost</th>
            <th>Total Cost</th>
          
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->supplier_invoice_number}}</td>
            <td>{{$res->supplier_invoice_date}}</td>
            <td>{{$res->supplier_name}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->package_id == '1' ? 'PC' : 'METER'}}</td>
            <td>{{number_format($res->item_cost, 2, '.', ',')}}</td>
            <td>{{number_format($res->amount, 2, '.', ',')}}</td>
           
        </tr>
    @endforeach
    </tbody>
</table>