<table>
    <thead>
    <tr>
            <th>Brand </th>
            <th>Category </th>
            <th>Color</th>
            <th>Invoice</th>
            <th>Invoice Date</th>
            <th>Supplier Name</th>
            <th>Item Name</th>
            <th>Quantity </th>
            <th>Unit </th>
            <th>Amount</th>
          
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->brand_name}}</td>
            <td>{{$res->category_name}}</td>
            <td>{{$res->color_name ? $res->color_name : ' N/A'}}</td>
            <td>{{$res->supplier_invoice_number}}</td>
            <td>{{$res->supplier_invoice_date}}</td>
            <td>{{$res->supplier_name}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->package_id == '1' ? 'PC' : 'METER'}}</td>
            <td>{{$res->amount}}</td>
           
        </tr>
    @endforeach
    </tbody>
</table>