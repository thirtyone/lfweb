<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>invoice # </th>
        <th>Invoice Amount</th>
        <th>Balance </th>
        <th>Paid Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->created_at}}</td>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->total}}</td>
            <td>{{$res->payment_status_option_id == 5 ? $res->total : '0.00'}}</td>
            <td>{{$res->payment_status_option_id != 5 ? $res->total : '0.00'}}</td>
        </tr>
    @endforeach
    </tbody>
</table>