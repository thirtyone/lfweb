<table>
    <thead>
    <tr>
        <th>Item # </th>
        <th>Client</th>
        <th>Credit Limit</th>
        <th>Total Paid</th>
        <th>Total Balance </th>

    </tr>
    </thead>
    <tbody>

    @foreach($response as $res)
        <tr>
            <td>{{$res['id']}}</td>
            <td>{{$res['name']}}</td>
            <td>{{   number_format($res['credit_limit'] ? $res['credit_limit'] : 0  , 2, '.', ',')}}</td>
            <td>{{   number_format($res['total_paid'] , 2, '.', ',')}}</td>
            <td>{{ $res['payment_terms'] != 0 && $res['payment_terms'] != '' ?  number_format($res['total_balance'] , 2, '.', ',' ) : '0'}}</td>

        </tr>
    @endforeach
    </tbody>
</table>
