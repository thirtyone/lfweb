<table>
    <thead>
    <tr>
        <th>Client Name</th>
        <th>Invoice#</th>
        <th>Invoice Date</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Item Name</th>
        <th>SRP</th>
        <th>Amount</th>
        
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->client_name}}</td>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->invoice_date}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->package_id == '1' ? 'PC' : 'METER'}}</td>
            <td>{{$res->name}}</td>
            <td>{{$res->amount}}</td>
            <td>{{number_format($res->total_amount, 2, '.', ',')}}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>