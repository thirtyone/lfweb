<table>
    <thead>
    <tr>
        <th>SKU</th>
        <th>NAME</th>
        <th>DESCRIPTION</th>
        <th>QUANTITY</th>
        <th>UNIT PRICE</th>
        <th>TOTAL AMOUNT</th>
        {{--<th>IMAGE</th>--}}

    </tr>
    </thead>
    <tbody>
    @foreach($response->hasManyOrderReference as $key => $res)
        <tr>
            <td>{{$res->item->sku}}</td>
            <td>{{$res->item->name}}</td>
            <td>{{$res->item->description}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->amount}}</td>
            <td>{{$res->total}}</td>
            {{--<td>{{$res->item->hasOneItemColor ? url( $res->item->hasOneItemColor->path.$res->item->hasOneItemColor->file_name ) : ""}}</td>--}}

        </tr>
    @endforeach

    <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Discount:</td>
            <td>{{number_format($response->discount, 2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>VAT:</td>
            <td>{{number_format($response->vat, 2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total Sales:</td>
            <td>{{number_format($response->total, 2)}}</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Grand Total:</td>
            <td>{{number_format($response->grand_total, 2)}}</td>
        </tr>
    </tbody>
</table>