<table>
    <thead>
    <tr>
        <th>Client Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Contact Number</th>
        <th>Bank Account</th>
        <th>Account #</th>
        <th>Credit Limit</th>
        <th>Payment Terms</th>
        <th>Tin #</th>
        <th>Business Style</th>

        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->address}}</td>
            <td>{{$res->email}}</td>
            <td>{{$res->contact_number}}</td>
            <td>{{$res->bank_name}}</td>
            <td>{{$res->bank_account_number}}</td>
            <td>{{$res->credit_limit}}</td>
            <td>{{$res->payment_terms}}</td>
            <td>{{$res->tin_number}}</td>
            <td>{{$res->business_style}}</td>

        </tr>
    @endforeach
    </tbody>
</table>
