<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Contact</th>
        <th>Address</th>
        <th>Terms</th>

    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->contact_number}}</td>
            <td>{{$res->address}}</td>
            <td>{{$res->payment_terms}}</td>

        </tr>
    @endforeach
    </tbody>
</table>
