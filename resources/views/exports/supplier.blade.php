<table>
    <thead>
    <tr>
        <th>Invoice #</th>
        <th>Date of Invoice</th>
        <th>Supplier Name</th>
        <th>Due Date</th>
        <th>Amount</th>
        <th>Payment Status</th>
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->invoice_number}}</td>
            <td>{{$res->invoice_date}}</td>
            <td>{{$res->name}}</td>
            <td>{{date('Y-m-d', strtotime($res->invoice_date . ' + ' . $res->payment_terms . ' days' ))}}</td>
            <td>{{number_format($res->grand_total, 2)}}</td>
            <td>{{$res->status_option_id == 4 ? 'Paid' : 'Unpaid'}}</td>
            <th>-</th>
        </tr>
    @endforeach
    </tbody>
</table>
