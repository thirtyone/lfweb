<table>
    <thead>
    <tr>
        <th>Order Number</th>
        <th>Status</th>
        <th>Client Name</th>
        <th>Branch</th>
        <th>Date/Time</th>


        <th>-</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $key => $res)
        <tr>
            <td>{{$res->order_number}}</td>
            <td>{{$res->statusOption->name}}</td>
            <td>{{$res->customer->name}}</td>
            <td>{{$res->branch->name}}</td>
            <td>{{$res->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
