<table>
    <thead>
    <tr>
        <th>Item</th>
        <th>Color</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($response as $res)
        <tr>
            <td>{{$res->name}}</td>
            <td>{{$res->color ? $res->color : 'N/A'}}</td>
            <td>{{$res->quantity}}</td>
            <td>{{$res->package_id == 1 ? 'PC' : 'METER'}}</td>
            <td>{{number_format($res->grand_total, 2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
