import axios from "axios";
import Config from "../../../configs/app.config";
import api from '../../server/client';
var promise;


export default class ResourceService {


    getAuth (){

        return JSON.parse(localStorage.getItem('authAdmin'));

    }

    getCategories() {

        promise = api.client.get(Config.base_url + '/api.public/service/get-categories');
        return promise;
    }


    getSubCategories(data) {

        promise = api.client.get(Config.base_url + '/api.public/service/get-sub-categories', {params: data});
        return promise;
    }

    getPackages(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-packages');
        return promise;
    }

    getBrands(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-brands');
        return promise;
    }
    getColors(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-colors');
        return promise;
    }

    getClientsByFilter(data){

        promise = api.client.get(Config.base_url + '/api.public/service/get-users-by-filter', {params: data});
        return promise;
    }


    getClients(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-users');
        return promise;
    }


    getSuppliers(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-suppliers');
        return promise;
    }


    getItems(data){

        promise = api.client.get(Config.base_url + '/api.public/service/get-items', {params: data});
        return promise;
    }

    getItemByStocks(data){

        promise = api.client.get(Config.base_url + '/api.public/service/get-item-by-stocks', {params: data});
        return promise;
    }


    getUserTypes() {

        promise = api.client.get(Config.base_url + '/api.public/service/get-user-types');
        return promise;
    }

    getSalesAgents(data) {

        promise = api.client.get(Config.base_url + '/api.public/service/get-sales-agent', {params: data});
        return promise;
    }



    getBranches(data){

        promise = api.client.get(Config.base_url + '/api.public/service/get-branches', {params: data});
        return promise;
    }

    getBranchTypes(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-branch-types');
        return promise;
    }

    getAdminRoles(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-admin-roles');
        return promise;
    }

    getStatusOptions(data) {

        promise = api.client.get(Config.base_url + '/api.public/service/get-status-options', {params: data});
        return promise;
    }

    getPromos(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-promos');
        return promise;
    }
    getPaymentTypes(){

        promise = api.client.get(Config.base_url + '/api.public/service/get-payment-types');
        return promise;
    }

    getNotifications(data){
        promise = api.client.get(Config.base_url + '/api.public/service/get-notifications', {params: data});
        return promise;
    }
    getPaymentModes(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-payment-modes');
        return promise;
    }

    getFrType(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-fr-type');
        return promise;
    }

    getSupplierType(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-supplier-type');
        return promise;
    }

    getUnpaidLocal(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-unpaid-local');
        return promise;
    }

    getUnpaidAbroad(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-unpaid-abroad');
        return promise;
    }

    getUnpaidClient(){
        promise = api.client.get(Config.base_url + '/api.public/service/get-unpaid-client');
        return promise;
    }
}
