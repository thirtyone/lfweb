import axios from "axios";
import Config from "../../../configs/app.config";
import api from '../../server/client';
var promise;


export default class UploadService {
    uploadImage(url, event) {

        let data = new FormData();
        data.append('file', event.target.files[0]);
        let config = {
            header: {
                'Content-Type': 'image/png'
            }
        }


        promise = api.client.post(Config.base_url + url, data, config);

        return promise;
    }

    uploadImport(url, event, model){
        let data = new FormData();
        data.append('file', event.target.files[0]);
        data.append('model', model);
        promise = model == 'ItemsImport' ?  api.client.post(Config.base_url + url, data, {timeout:90000}) :
        api.client.post(Config.base_url + url, data);
        return promise;
    }

}
