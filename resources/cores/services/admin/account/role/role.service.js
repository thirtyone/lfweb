import axios from "axios";
import Config from "../../../../../configs/app.config";
import api from '../../../../server/client';
var promise;


export default class RoleService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/admin-roles', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/admin-roles/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url + '/api.admin/admin-roles', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/admin-roles/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/admin-roles/' + id);
        return promise;
    }

    activate(id) {
        promise = api.client.post(Config.base_url + '/api.admin/admin-roles/activate/' + id);
        return promise;
    }

}
