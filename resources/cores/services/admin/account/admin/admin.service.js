import axios from "axios";
import Config from "../../../../../configs/app.config";
import api from '../../../../server/client';
var promise;


export default class AdminService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/accounts', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/accounts/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url + '/api.admin/accounts', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/accounts/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/accounts/' + id);
        return promise;
    }
    
    activate(id) {
        promise = api.client.post(Config.base_url + '/api.admin/accounts/activate/' + id);
        return promise;
    }

}
