import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class HistoryService {

    list(data) {

        promise = api.client.get(Config.base_url + '/api.admin/history-log', {params: data});
        return promise;
    }


    store(history) {

        promise = api.client.post(Config.base_url + '/api.admin/history-log', history);
        return promise;
    }
}
