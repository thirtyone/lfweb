import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class DashboardService {



    getRestockItem(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-restock-item');
        return promise;
    }

    getRestockItems(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-restock-items');
        return promise;
    }

    getMostSupplyItem(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-most-supply-item');
        return promise;
    }

    getMostSupplyItems(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-most-supply-items');
        return promise;
    }

    getFastMovingItem(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-fast-moving-item');
        return promise;
    }

    getFastMovingItems(data) {

        promise = api.client.get(Config.base_url + '/api.admin/dashboard/get-fast-moving-items');
        return promise;
    }


}
