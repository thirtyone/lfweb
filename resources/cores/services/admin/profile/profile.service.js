import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class ProfileService {

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/my-profile/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url + '/api.admin/my-profile', {params: data});
        return promise;
    }

    refreshAuthorization() {
        promise = api.client.post(Config.base_url + '/api.admin/my-profile/refresh-authorization');
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/my-profile/' + id);
        return promise;
    }

}
