import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class SupplierInvoiceService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/supplier-invoices', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/supplier-invoices/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url +'/api.admin/supplier-invoices', {params: data});
        return promise;
    }

    listAbroad(data) {

        promise = api.client.get(Config.base_url +'/api.admin/supplier-invoices-abroad', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/supplier-invoices/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/supplier-invoices/' + id);
        return promise;
    }

}
