import axios from "axios";
import Config from "../../../../configs/app.config";
import api from '../../../server/client';

var promise;


export default class ReportService {

    list(report,data) {
        promise = api.client.get(Config.base_url + '/api.admin/reports/' + report , {params: data});
        return promise;
    }


}
