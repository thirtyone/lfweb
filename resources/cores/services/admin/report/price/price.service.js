import axios from 'axios';
import Config from '../../../../../configs/app.config';
import api from "../../../../server/client.js"
var promise;

export default class PriceReportService {

    list(report,data) {
        promise = api.client.get(Config.base_url + '/api.admin/reports/price/' + report , {params: data});
        return promise;
    }

}
