import axios from 'axios';
import Config from '../../../../../configs/app.config';
import api from "../../../../server/client.js"
var promise;

export default class BalanceReportService {

    list(report,data) {
        promise = api.client.get(Config.base_url + '/api.admin/reports/balance/' + report , {params: data});
        return promise;
    }

}
