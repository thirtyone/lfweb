import axios from "axios";
import Config from "../../../../../configs/app.config";
import api from "../../../../server/client.js"
var promise;


export default class ItemBranchService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/item-branches', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/item-branches/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url +'/api.admin/item-branches', {params: data});
        return promise;
    }

    show(id,data) {
        promise = api.client.get(Config.base_url + '/api.admin/item-branches/' + id,{params: data});
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/item-branches/' + id);
        return promise;
    }



    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/item-branches/' + id);
        return promise;
    }


}
