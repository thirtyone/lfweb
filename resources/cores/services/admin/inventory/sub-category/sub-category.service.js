import axios from "axios";
import Config from "../../../../../configs/app.config";
import api from "../../../../server/client.js"
var promise;


export default class SubCategoryService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/sub-categories', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/sub-categories/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url +'/api.admin/sub-categories', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/sub-categories/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/sub-categories/' + id);
        return promise;
    }

    activate(id) {
        promise = api.client.post(Config.base_url + '/api.admin/sub-categories/activate/' + id);
        return promise;
    }


}
