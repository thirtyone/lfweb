import axios from "axios";
import Config from "../../../../../configs/app.config";
import api from '../../../../server/client';
var promise;


export default class SupplierService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/suppliers', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/suppliers/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url +'/api.admin/suppliers', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/suppliers/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/suppliers/' + id);
        return promise;
    }

    activate(id) {
        promise = api.client.post(Config.base_url + '/api.admin/suppliers/activate/' + id);
        return promise;
    }

}
