import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"

var promise;


export default class AgentDashboardService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/agent-dashboards', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/agent-dashboards/' + data.id, data);
        return promise;
    }

    list(data) {

        //promise = api.client.get(Config.base_url +'/api.admin/agent-dashboards', {params: data});
        promise = api.client.get(Config.base_url +'/api.admin/agent-dashboards', {params: data});
        return promise;
    }
    listOrder(data) {

        promise = api.client.get(Config.base_url +'/api.admin/agent-dashboards/orders', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/agent-dashboards/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/agent-dashboards/' + id);
        return promise;
    }

}
