import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class OrderService {


    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/orders', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/orders/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url + '/api.admin/orders', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/orders/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/orders/' + id);
        return promise;
    }

    returnItems(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/return/' + data.id, data);
        return promise;
    }


    updateDrSampleStatus(data) {
        promise = api.client.put(Config.base_url + '/api.admin/orders/update-dr-sample-status/' + data.id, data);
        return promise;
    }
    updateReturnItemsTicket(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/update-return-items-ticket', data);
        return promise;
    }

    deleteReturnItemsTicket(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/orders/delete-return-items-ticket/' + id);
        return promise;
    }

    returnItemsTicket(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/return-items-ticket', data);
        return promise;
    }

    showReturnItemsTicket(id) {
        promise = api.client.get(Config.base_url + '/api.admin/orders/get-return-items-by-ticket-id/' + id);
        return promise;
    }

    storePreOrderItem(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/store-pre-order-item', data);
        return promise;
    }


    nonFranchiseOrders(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/non-franchise-orders', data);
        return promise;
    }

    franchiseOrders(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/franchise-orders', data);
        return promise;
    }

    nonFranchiseValidateQuantity(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/non-franchise-validate-quantities', data);
        return promise;
    }

    orderToTransit(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/order-to-transit', data);
        return promise;
    }

    orderToReceive(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/order-to-receive', data);
        return promise;
    }

    franchiseValidateQuantity(data) {
        promise = api.client.post(Config.base_url + '/api.admin/orders/franchise-validate-quantities', data);
        return promise;
    }

    returnStock(data) {

        promise = api.client.post(Config.base_url + '/api.admin/orders/return-stock', data);
        return promise;
    }


}
