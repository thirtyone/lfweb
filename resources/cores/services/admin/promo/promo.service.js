import axios from "axios";
import Config from "../../../../configs/app.config";
import api from "../../../server/client.js"
var promise;


export default class PromoService {



    store(data) {

        promise = api.client.post(Config.base_url + '/api.admin/promos', data);
        return promise;
    }

    update(data) {
        promise = api.client.put(Config.base_url + '/api.admin/promos/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = api.client.get(Config.base_url +'/api.admin/promos', {params: data});
        return promise;
    }

    show(id) {
        promise = api.client.get(Config.base_url + '/api.admin/promos/' + id);
        return promise;
    }

    delete(id) {
        promise = api.client.delete(Config.base_url + '/api.admin/promos/' + id);
        return promise;
    }

    activate(id) {
        promise = api.client.post(Config.base_url + '/api.admin/promos/activate/' + id);
        return promise;
    }


}
