import axios from 'axios'
import AlertService from "../services/alert/alert.service";

const alertService = new AlertService();
let client = axios.create({
    timeout: 30000,
    headers: {
        'Content-Type': 'application/json',
    },
});

// Response
client.interceptors.response.use(
    (response) => {

        return response
    },
    (error) => {

        if (error.response.status === 401) {
            localStorage.removeItem('authAdmin');
            alertService.sessionExpired('Session Expired.');
        }
        return Promise.reject(error)
    }
);

export default {
    client
}
