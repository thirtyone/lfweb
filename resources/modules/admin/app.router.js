import Config from '../../configs/app.config';
import App from './app.component';
import LoginComponent from './components/auth/login.component';
import ListSampleComponent from './components/sample/list-sample.component';
import ReportRouter from './components/report/report.router';

//dashboard agent
import DashboardComponent from './components/dashboard/dashboard.component';
import ListRestockItem from './components/dashboard/stock/list-restock-items.component';
import ListMostSupplyItem from './components/dashboard/stock/list-most-supply-items.component';
import ListFastMovingItem from './components/dashboard/stock/list-fast-moving-items.component';


//import data
import ImportComponent from './components/import/import-component.component';

//dashboard agent
import ListAgentDashboard from './components/agent-dashboard/list-agent-dashboard.component';
import ListAgentOrderDashboard from './components/agent-dashboard/list-agent-order-dashboard.component';

//items
import CrupItemComponent from './components/inventory/item/crup-item.component';
import ListItemComponent from './components/inventory/item/list-item.component';
import ShowItemComponent from './components/inventory/item/show-item.component';

//brands
import CrupBrandComponent from './components/inventory/brand/crup-brand.component';
import ListBrandComponent from './components/inventory/brand/list-brand.component';

//categories
import CrupCategoryComponent from './components/inventory/category/crup-category.component';
import ListCategoryComponent from './components/inventory/category/list-category.component';

//sub-categories
import CrupSubCategoryComponent from './components/inventory/sub-category/crup-sub-category.component';
import ListSubCategoryComponent from './components/inventory/sub-category/list-sub-category.component';

//suppliers
import CrupSupplierComponent from './components/inventory/supplier/crup-supplier.component';
import ListSupplierComponent from './components/inventory/supplier/list-supplier.component';

//clients
import CrupClientComponent from './components/client/crup-client.component';
import ListClientComponent from './components/client/list-client.component';
import ListClientOrdersComponent from './components/client/list-client-orders.component';


//branches
import CrupBranchComponent from './components/branch/crup-branch.component';
import ListBranchComponent from './components/branch/list-branch.component';

//accounts
import CrupAccountAdminComponent from './components/account/admin/crup-account-admin.component';
import ListAccountAdminComponent from './components/account/admin/list-account-admin.component';
//accounts roles
import CrupAccountRoleComponent from './components/account/role/crup-account-roles.component';
import ListAccountRoleComponent from './components/account/role/list-account-roles.component';

//CrupSupplierInvoiceComponent
import CrupSupplierInvoiceComponent from './components/supplier-invoice/crup-supplier-invoice.component';
import ListSupplierInvoiceComponent from './components/supplier-invoice/list-supplier-invoice.component';

//CrupSupplierInvoiceAbroad
import CrupSupplierInvoiceAbroadComponent from './components/supplier-invoice-abroad/crup-supplier-invoice-abroad.component';
import ListSupplierInvoiceAbroadComponent from './components/supplier-invoice-abroad/list-supplier-invoice-abroad.component';

//franchise order
import CrupFranchiseOrderComponent from './components/order/franchise/crup-frachise-order.component';
import ListFranchiseOrderComponent from './components/order/franchise/list-franchise-order.component';
//inter-office order
import CrupInterOfficeOrderComponent from './components/order/inter-office/crup-inter-office-order.component';
import OrganizeInterOfficeOrderComponent from './components/order/inter-office/organize-inter-office-order.component';
import ListInterOfficeOrderComponent from './components/order/inter-office/list-inter-office-order.component';

//client-order
import CrupClientBranchOrderComponent from './components/order/client/crup-client-branch-order.component';
import CrupClientFranchiseOrderComponent from './components/order/client/crup-client-franchise-order.component';
import ListClientOfficeOrderComponent from './components/order/client/list-client-office-order.component';


//pre-order
import CrupClientPreOrderComponent from './components/order/pre-order/crup-client-pre-order.component';
import ListClientPreOrderComponent from './components/order/pre-order/list-client-pre-order.component';

//dr sample order
import CrupDrSampleOrderComponent from './components/order/dr-sample/crup-dr-sample-order.component';
import ListDrSampleOrderComponent from './components/order/dr-sample/list-dr-sample-order.component';


//quotation order
import CrupQuotationOrderComponent from './components/order/quotation/crup-quotation-order.component';
import ListQuotationOrderComponent from './components/order/quotation/list-quotation-order.component';

//profile
import ProfileComponent from './components/profile/profile.component';

//history logs
import ListHistoryComponent from './components/history/list-history.component';

//reports
import ListSupplierAbroadReportComponent from './components/report/list-supplier-abroad-report.component';
import ListSupplierReportComponent from './components/report/list-supplier-report.component';
import ListInventoryReportComponent from './components/report/list-inventory-report.component';
// import ListInterOfficeReportComponent from './components/report/list-inter-office-report.component';
import ListClientReportComponent from './components/report/most-sold/list-client-report.component';

import ListFranchiseReportComponent from './components/report/list-franchise-report.component';
import ListSalesPerItemReportComponent from './components/report/sales/list-sales-per-item-report.component';
import ListTotalSaleReportComponent from './components/report/list-total-sale-report.component';
import ListSalesPerColorReportComponent from './components/report/sales/list-sales-per-color-report.component';
import ListSalesPerBrandReportComponent from './components/report/sales/list-sales-per-brand-report.component';
import listPerBrandSupplierReportComponent from './components/report/purchases/list-per-brand-supplier-report.component';
import listPerSupplierReportComponent from './components/report/purchases/list-per-supplier-report.component';
import listPerItemColorSupplierReportComponent from './components/report/list-per-item-color-supplier-report.component';
import listPerItemSupplierReportComponent from './components/report/list-per-item-supplier-report.component';
// import ListSalesPerDealerReportComponent from './components/report/list-sales-per-dealer.component';

//promos
import ListPromoComponent from './components/promo/list-promo.component';
import CrupPromoComponent from './components/promo/crup-promo.component';


function isFranchise() {
    if (auth && auth.branch.branch_type_id === 2) {
        return true
    }


}


function requireRole(to, from, next) {

    const authRoles = JSON.parse(auth.admin_role.roles);


    const found = authRoles.some(el => el == to.meta.role);

    if (found) {
        next();

    }else{
        next(prefix + '/my-profile');
    }




}

export default [

    {
        name: 'app',
        path: Config.admin_prefix + '/',
        component: App,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Dashboard'
        }
    },
    {
        name: 'login',
        path: Config.admin_prefix + '/login',
        component: LoginComponent,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Login'
        }
    },
    {
        name: 'dashboard',
        path: Config.admin_prefix + '/dashboard',
        component: DashboardComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            role: 'Dashboard',
            title: 'Dashboard'
        }
    },
    {
        name: 'list-restock-item',
        path: Config.admin_prefix + '/dashboard/restock-items',
        component: ListRestockItem,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            role: 'Dashboard',
            title: 'Items that need to restock'
        }
    },
    {
        name: 'list-most-supply-item',
        path: Config.admin_prefix + '/dashboard/most-supply-items',
        component: ListMostSupplyItem,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            role: 'Dashboard',
            title: 'Item which has most supply'
        }
    },
    {
        name: 'list-fast-moving-item',
        path: Config.admin_prefix + '/dashboard/fast-moving-items',
        component: ListFastMovingItem,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            role: 'Dashboard',
            title: 'Fast Moving Item'
        }
    },
    //dashboard agent
    {
        name: 'list-agent-dashboard',
        path: Config.admin_prefix + '/agent-dashboards',
        component: ListAgentDashboard,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'agent-dashboards',
                child: '',
                hierarchy: [
                    {name: 'Agent Dashboard', is_route: false, route: 'list-agent-dashboard' },
                ]
            },
            role: 'Agent Dashboard',
            title: 'Agent Dashboards'
        }
    },
    {
        name: 'list-agent-order-dashboard',
        path: Config.admin_prefix + '/agent-dashboards/orders',
        component: ListAgentOrderDashboard,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'agent-dashboards',
                child: '',
                hierarchy: [
                    {name: 'Agent Dashboard', is_route: true, route: 'list-agent-dashboard' },
                    {name: 'Sales Summary', is_route: false, route: '' },
                ]
            },
            role: 'Agent Dashboard',
            title: 'Agent Dashboard Orders'
        }
    },

    //suppliers
    {
        name: 'supplier-create',
        path: Config.admin_prefix + '/inventory/suppliers/create',
        component: CrupSupplierComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'suppliers',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Suppliers', is_route: true, route: 'supplier-list' },
                    {name: 'New Supplier', is_route: false, route: 'supplier-create' },
                ]
            },
            role: "Suppliers",
            title: 'Create Supplier'
        }
    },
    {
        name: 'supplier-update',
        path: Config.admin_prefix + '/inventory/suppliers/:id/edit',
        component: CrupSupplierComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'suppliers',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Suppliers', is_route: true, route: 'supplier-list' },
                    {name: 'Edit Supplier', is_route: false, route: 'supplier-update' },
                ]
            },
            role: "Suppliers",
            title: 'Update Supplier'
        }
    },
    {
        name: 'supplier-list',
        path: Config.admin_prefix + '/inventory/suppliers',
        component: ListSupplierComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'suppliers',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Suppliers', is_route: false, route: 'supplier-list' },
                ]
            },
            role: "Suppliers",
            title: 'List of Suppliers'
        }
    },

    //brands
    {
        name: 'brand-create',
        path: Config.admin_prefix + '/inventory/brands/create',
        component: CrupBrandComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'brands',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Brands', is_route: true, route: 'brand-list' },
                    {name: 'New Brand', is_route: false, route: 'brand-create' },
                ]
            },
            role: "Brands",
            title: 'Create Brand'
        }
    },
    {
        name: 'brand-update',
        path: Config.admin_prefix + '/inventory/brands/:id/edit',
        component: CrupBrandComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'brands',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Brands', is_route: true, route: 'brand-list' },
                    {name: 'Edit Brand', is_route: false, route: 'brand-update' },
                ]
            },
            role: "Brands",
            title: 'Update Brand'
        }

    },
    {
        name: 'brand-list',
        path: Config.admin_prefix + '/inventory/brands',
        component: ListBrandComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'brands',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Brands', is_route: false, route: 'brand-list' },
                ]
            },
            role: "Brands",
            title: 'List of Brands'
        }
    },

    //Categories
    {
        name: 'category-create',
        path: Config.admin_prefix + '/inventory/categories/create',
        component: CrupCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Categories', is_route: true, route: 'category-list' },
                    {name: 'New Category', is_route: false, route: 'category-create' },
                ]
            },
            role: "Categories",
            title: 'Create Category'
        }
    },
    {
        name: 'category-update',
        path: Config.admin_prefix + '/inventory/categories/:id/edit',
        component: CrupCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Categories', is_route: true, route: 'category-list' },
                    {name: 'Edit Category', is_route: false, route: 'category-update' },
                ]
            },
            role: "Categories",
            title: 'Update Category'
        }
    },
    {
        name: 'category-list',
        path: Config.admin_prefix + '/inventory/categories',
        component: ListCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Categories', is_route: false, route: 'category-list' },
                ]
            },
            role: "Categories",
            title: 'List of Categories'
        }
    },

    //sub-categories
    {
        name: 'sub-category-create',
        path: Config.admin_prefix + '/inventory/sub-categories/create',
        component: CrupSubCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'sub-categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Sub Categories', is_route: true, route: 'sub-category-list' },
                    {name: 'New Sub Categories', is_route: false, route: 'sub-category-create' },
                ]
            },
            role: "Sub Categories",
            title: 'Create Sub Category'
        }
    },
    {
        name: 'sub-category-update',
        path: Config.admin_prefix + '/inventory/sub-categories/:id/edit',
        component: CrupSubCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'sub-categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Sub Categories', is_route: true, route: 'sub-category-list' },
                    {name: 'Edit Sub Categories', is_route: false, route: 'sub-category-update' },
                ]
            },
            role: "Sub Categories",
            title: 'Update Sub Category'
        }
    },
    {
        name: 'sub-category-list',
        path: Config.admin_prefix + '/inventory/sub-categories',
        component: ListSubCategoryComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'sub-categories',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Sub Categories', is_route: false, route: 'sub-category-list' },
                ]
            },
            role: "Sub Categories",
            title: 'List of Sub Categories'
        }
    },

    //client
    {
        name: 'client-create',
        path: Config.admin_prefix + '/clients/create',
        component: CrupClientComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'clients',
                child: '',
                hierarchy: [
                    {name: 'Clients', is_route: true, route: 'client-list' },
                    {name: 'New Client', is_route: false, route: 'client-create' },
                ]
            },
            role: "Clients",
            title: 'Create Client'
        }
    },
    {
        name: 'client-update',
        path: Config.admin_prefix + '/clients/:id/edit',
        component: CrupClientComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'clients',
                child: '',
                hierarchy: [
                    {name: 'Clients', is_route: true, route: 'client-list' },
                    {name: 'Edit Client', is_route: false, route: 'client-update' },
                ]
            },
            role: "Clients",
            title: 'Update Client'
        }
    },
    {
        name: 'client-list',
        path: Config.admin_prefix + '/clients',
        component: ListClientComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'clients',
                child: '',
                hierarchy: [
                    {name: 'Clients', is_route: false, route: 'client-list' },
                ]
            },
            role: "Clients",
            title: 'List of Clients'
        }
    },
    {
        name: 'client-orders-list',
        path: Config.admin_prefix + '/clients/:id/orders',
        component: ListClientOrdersComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'clients',
                child: '',
                hierarchy: [
                    {name: 'Clients', is_route: true, route: 'client-list' },
                    {name: 'Client Orders', is_route: false, route: '' },
                ]
            },
            role: "Clients",
            title: 'List of Client Orders'
        }
    },

    //items
    {
        name: 'item-create',
        path: Config.admin_prefix + '/inventory/items/create',
        component: CrupItemComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'items',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Items', is_route: true, route: 'item-list' },
                    {name: 'New Item', is_route: false, route: 'item-create' }
                ]
            },
            role: "Items",
            title: 'Create Item',

        }
    },
    {
        name: 'item-update',
        path: Config.admin_prefix + '/inventory/items/:id/edit',
        component: CrupItemComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'items',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Items', is_route: true, route: 'item-list' },
                    {name: 'Edit Item', is_route: false, route: 'item-update' }
                ]
            },
            role: "Items",
            title: 'Update Item'
        }
    },
    {
        name: 'item-list',
        path: Config.admin_prefix + '/inventory/items',
        component: ListItemComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'items',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Items', is_route: false, route: 'item-list' }
                ]
            },
            role: "Items",
            title: 'List of Items'
        }
    },

    {
        name: 'item-show',
        path: Config.admin_prefix + '/inventory/items/show/:id',
        component: ShowItemComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'inventory',
                child: 'items',
                hierarchy: [
                    {name: 'Inventory', is_route: false, route: '' },
                    {name: 'Items', is_route: true, route: 'item-list' },
                    {name: 'View Item ', is_route: false, route: 'item-show' },
                ]
            },
            role: "Items",
            title: 'Show Item'
        }
    },


    //branches
    {
        name: 'branch-create',
        path: Config.admin_prefix + '/branches/create',
        component: CrupBranchComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'branches',
                child: '',
                hierarchy: [
                    {name: 'Branches', is_route: true, route: 'branch-list' },
                    {name: 'New Branch', is_route: false, route: 'branch-create' },
                ]
            },
            role: "Branches",
            title: 'Create Warehouse'
        }
    },
    {
        name: 'branch-update',
        path: Config.admin_prefix + '/branches/:id/edit',
        component: CrupBranchComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'branches',
                child: '',
                hierarchy: [
                    {name: 'Branches', is_route: true, route: 'branch-list' },
                    {name: 'Edit Branch', is_route: false, route: 'branch-update' },
                ]
            },
            role: "Branches",
            title: 'Update Warehouse'
        }
    },
    {
        name: 'branch-list',
        path: Config.admin_prefix + '/branches',
        component: ListBranchComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'branches',
                child: '',
                hierarchy: [
                    {name: 'Branches', is_route: false, route: 'branch-list' },
                ]
            },
            role: "Branches",
            title: 'List of Warehouse'
        }
    },

    //accounts
    {
        name: 'admin-create',
        path: Config.admin_prefix + '/account/admins/create',
        component: CrupAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Admins', is_route: true, route: 'admin-list' },
                    {name: 'New Admin', is_route: false, route: 'admin-create' },
                ]
            },
            role: "Accounts",
            title: 'Create Admin'
        }
    },
    {
        name: 'admin-update',
        path: Config.admin_prefix + '/account/admins/:id/edit',
        component: CrupAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Admins', is_route: true, route: 'admin-list' },
                    {name: 'Edit Admin', is_route: false, route: 'admin-update' },
                ]
            },
            role: "Accounts",
            title: 'Update Admin'
        }

    },
    {
        name: 'admin-list',
        path: Config.admin_prefix + '/account/admins',
        component: ListAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Admins', is_route: false, route: 'admin-list' },
                ]
            },
            role: "Accounts",
            title: 'List of Admins'
        }
    },


    //roles
    {
        name: 'role-create',
        path: Config.admin_prefix + '/account/roles/create',
        component: CrupAccountRoleComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'roles',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Roles', is_route: true, route: 'role-list' },
                    {name: 'New Roles', is_route: false, route: 'role-create' },
                ]
            },
            role: "Accounts",
            title: 'Create Role'
        }
    },
    {
        name: 'role-update',
        path: Config.admin_prefix + '/account/roles/:id/edit',
        component: CrupAccountRoleComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'roles',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Roles', is_route: true, route: 'role-list' },
                    {name: 'Edit Roles', is_route: false, route: 'role-update' },
                ]
            },
            role: "Accounts",
            title: 'Update Role'
        }

    },
    {
        name: 'role-list',
        path: Config.admin_prefix + '/account/roles',
        component: ListAccountRoleComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'roles',
                hierarchy: [
                    {name: 'Accounts', is_route: false, route: '' },
                    {name: 'Roles', is_route: false, route: 'role-list' },
                ]
            },
            role: "Accounts",
            title: 'List of Roles'
        }
    },


    //supplier invoices
    {
        name: 'supplier-invoice-create',
        path: Config.admin_prefix + '/supplier-invoices/create',
        component: CrupSupplierInvoiceComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices', is_route: true, route: 'supplier-invoice-create' },
                    {name: 'New Supplier Invoice', is_route: false, route: 'supplier-invoice-create' },
                ]
            },
            role: "Supplier Invoices",
            title: 'Create Supplier Invoice'
        }
    },
    {
        name: 'supplier-invoice-update',
        path: Config.admin_prefix + '/supplier-invoices/:id/edit',
        component: CrupSupplierInvoiceComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices', is_route: true, route: 'supplier-invoice-create' },
                    {name: 'Edit Supplier Invoice', is_route: false, route: 'supplier-invoice-update' },
                ]
            },
            role: "Supplier Invoices",
            title: 'Update Supplier Invoice'
        }
    },
    {
        name: 'supplier-invoice-list',
        path: Config.admin_prefix + '/supplier-invoices',
        component: ListSupplierInvoiceComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices', is_route: false, route: 'supplier-invoice-list' },
                ]
            },
            role: "Supplier Invoices",
            title: 'List of Supplier Invoices'
        }
    },
    {
        name: 'supplier-invoice-abroad-list',
        path: Config.admin_prefix + '/supplier-invoices-abroad',
        component: ListSupplierInvoiceAbroadComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices-abroad',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices Abroad', is_route: false, route: 'supplier-invoice-abroad-list' },
                ]
            },
            role: "Supplier Invoices Abroad",
            title: 'List of Supplier Invoices Abroad'
        }
    },
    {
        name: 'supplier-invoice-abroad-create',
        path: Config.admin_prefix + '/supplier-invoices-abroad/create',
        component: CrupSupplierInvoiceAbroadComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices-abroad',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices Abroad', is_route: true, route: 'supplier-invoice-abroad-list' },
                    {name: 'New Supplier Invoice Abroad', is_route: false, route: 'supplier-invoice-abroad-create' },
                ]
            },
            role: "Supplier Invoices Abroad",
            title: 'Create Supplier Invoice Abroad'
        }
    },
    {
        name: 'supplier-invoice-abroad-update',
        path: Config.admin_prefix + '/supplier-invoices-abroad/:id/edit',
        component: CrupSupplierInvoiceAbroadComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-invoices-abroad',
                child: '',
                hierarchy: [
                    {name: 'Supplier Invoices Abroad', is_route: true, route: 'supplier-invoice-abroad-list' },
                    {name: 'Edit Supplier Invoice Abroad', is_route: false, route: 'supplier-invoice-abroad-update' },
                ]
            },
            role: "Supplier Invoices Abroad",
            title: 'Update Supplier Invoice Abroad'
        }
    },




    {
        name: 'franchise-order-create',
        path: Config.admin_prefix + '/order/franchises/create',
        beforeEnter: requireRole,
        component: CrupFranchiseOrderComponent,
        // get component() {
        //     if (isFranchise()) {
        //
        //         return CrupFranchiseOrderComponent
        //     } else {
        //         return OrganizeFranchiseOrderComponent
        //     }
        // },
        meta: {
            navigation: {
                parent: 'orders',
                child: 'franchises',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Franchises', is_route: true, route: 'franchise-order-list' },
                    {name: 'New Franchise', is_route: false, route: 'franchise-order-create' },
                ]
            },
            role: "Franchise Orders",
            title: 'Create Franchise Order'
        }
    },
    {
        name: 'franchise-order-update',
        path: Config.admin_prefix + '/order/franchises/:id/edit',
        beforeEnter: requireRole,

        component: CrupFranchiseOrderComponent,
        // get component() {
        //     if (isFranchise()) {
        //         return CrupFranchiseOrderComponent
        //     } else {
        //         return OrganizeFranchiseOrderComponent
        //     }
        // },

        meta: {
            navigation: {
                parent: 'orders',
                child: 'franchises',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Franchises', is_route: true, route: 'franchise-order-list' },
                    {name: 'Edit Franchise', is_route: false, route: 'franchise-order-update' },
                ]
            },
            role: "Franchise Orders",
            title: 'Update Franchise Order'
        }
    },

    {
        name: 'franchise-order-list',
        path: Config.admin_prefix + '/order/franchises',
        component: ListFranchiseOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'franchises',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Franchises', is_route: false, route: 'franchise-order-list' },
                ]
            },
            role: "Franchise Orders",
            title: 'List of Franchise Orders'
        }
    },

    // inter-office order
    {
        name: 'inter-office-order-create',
        path: Config.admin_prefix + '/order/inter-offices/create',
        component: CrupInterOfficeOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'inter-offices',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Inter Offices', is_route: true, route: 'inter-office-order-list' },
                    {name: 'New Inter Office', is_route: false, route: 'inter-office-order-create' },
                ]
            },
            role: "Inter Office Orders",
            title: 'Create Inter Office Order'
        }
    },
    {
        name: 'inter-office-order-update',
        path: Config.admin_prefix + '/order/inter-offices/:id/edit',
        component: CrupInterOfficeOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'inter-offices',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Inter Offices', is_route: true, route: 'inter-office-order-list' },
                    {name: 'Edit Inter Office', is_route: false, route: 'inter-office-order-update' },
                ]
            },
            role: "Inter Office Orders",
            title: 'Update Inter Office Order'
        }
    },


    {
        name: 'inter-office-order-organize',
        path: Config.admin_prefix + '/order/inter-offices/:id/organize',
        component: OrganizeInterOfficeOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'inter-offices',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Inter Offices', is_route: true, route: 'inter-office-order-list' },
                    {name: 'Organize Inter Office', is_route: false, route: 'inter-office-order-organize' },
                ]
            },
            role: "Inter Office Orders",
            title: 'Organize Inter Office Order'
        }
    },


    {
        name: 'inter-office-order-list',
        path: Config.admin_prefix + '/order/inter-offices',
        component: ListInterOfficeOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'inter-offices',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Inter Offices', is_route: false, route: 'inter-office-order-list' },
                ]
            },
            role: "Inter Office Orders",
            title: 'List of Inter Office Orders'
        }
    },


    // client order
    {
        name: 'client-order-create',
        path: Config.admin_prefix + '/order/clients/create',
        beforeEnter: requireRole,
        component: CrupClientBranchOrderComponent,
        // get component() {
        //     if (isFranchise()) {
        //         return CrupClientFranchiseOrderComponent
        //     } else {
        //         return CrupClientBranchOrderComponent
        //     }
        // },
        meta: {
            navigation: {
                parent: 'orders',
                child: 'clients',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Walk In', is_route: true, route: 'item-list' },
                    {name: 'New Walk In', is_route: false, route: 'client-order-create' },
                ]
            },
            role: "Walk In Orders",
            title: 'Create Walk In Orders'
        }
    },
    {
        name: 'client-order-update',
        path: Config.admin_prefix + '/order/clients/:id/edit',
        beforeEnter: requireRole,
        component: CrupClientBranchOrderComponent,
        // get component() {

        //     if (isFranchise()) {
        //         return CrupClientFranchiseOrderComponent
        //     } else {
        //         return CrupClientBranchOrderComponent
        //     }
        // },
        meta: {
            navigation: {
                parent: 'orders',
                child: 'clients',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Walk In', is_route: true, route: 'item-list' },
                    {name: 'Edit Walk In', is_route: false, route: 'client-order-update' },
                ]
            },
            role: "Walk In Orders",
            title: 'Update Walk In Order'
        }
    },

    {
        name: 'client-order-list',
        path: Config.admin_prefix + '/order/clients',
        component: ListClientOfficeOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'clients',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Walk In', is_route: false, route: 'client-order-list' },
                ]
            },
            role: "Walk In Orders",
            title: 'List of Walk In Orders'
        }
    },
    // client pre order
    {
        name: 'client-pre-order-update',
        path: Config.admin_prefix + '/order/pre-orders/:id/edit',
        beforeEnter: requireRole,
        component: CrupClientPreOrderComponent,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'pre-orders',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Pre Order', is_route: true, route: 'client-pre-order-update' },
                    {name: 'Edit Pre Order', is_route: false, route: 'client-pre-order-update' },
                ]
            },
            role: "Pre Orders",
            title: 'View Pre Order'
        }
    },

    {
        name: 'client-pre-order-list',
        path: Config.admin_prefix + '/order/pre-orders',
        component: ListClientPreOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'pre-orders',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Pre Order', is_route: false, route: 'client-pre-order-list' },
                ]
            },
            role: "Pre Orders",
            title: 'List of Pre Orders'
        }
    },


    //dr sample order
    {
        name: 'dr-sample-order-create',
        path: Config.admin_prefix + '/order/dr-samples/create',
        component: CrupDrSampleOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'dr-samples',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Dr Sample', is_route: true, route: 'dr-sample-order-list' },
                    {name: 'New Dr Sample', is_route: false, route: 'dr-sample-order-create' },
                ]
            },
            role: "DR Orders",
            title: 'Create Dr Sample Order'
        }
    },
    {
        name: 'dr-sample-order-update',
        path: Config.admin_prefix + '/order/dr-samples/:id/edit',
        component: CrupDrSampleOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'dr-samples',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Dr Sample', is_route: true, route: 'dr-sample-order-list' },
                    {name: 'Edit Dr Sample', is_route: false, route: 'dr-sample-order-update' },
                ]
            },
            role: "DR Orders",
            title: 'Update Dr Sample Order'
        }
    },
    {
        name: 'dr-sample-order-list',
        path: Config.admin_prefix + '/order/dr-samples',
        component: ListDrSampleOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'dr-samples',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Dr Sample', is_route: false, route: 'dr-sample-order-list' },
                ]
            },
            role: "DR Orders",
            title: 'List of Dr Sample Orders'
        }
    },

    //quotation order
    {
        name: 'quotation-order-create',
        path: Config.admin_prefix + '/order/quotations/create',
        component: CrupQuotationOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'quotations',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Quotations', is_route: true, route: 'quotation-order-list' },
                    {name: 'New Quotation', is_route: false, route: 'quotation-order-create' },
                ]
            },
            role: "Quotation Orders",
            title: 'Create Quotation Order'
        }
    },
    {
        name: 'quotation-order-update',
        path: Config.admin_prefix + '/order/quotations/:id/edit',
        component: CrupQuotationOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'quotations',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Quotations', is_route: true, route: 'quotation-order-list' },
                    {name: 'Edit Quotation', is_route: false, route: 'quotation-order-update' },
                ]
            },
            role: "Quotation Orders",
            title: 'Update Quotation Order'
        }
    },

    {
        name: 'quotation-order-list',
        path: Config.admin_prefix + '/order/quotations',
        component: ListQuotationOrderComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'orders',
                child: 'quotations',
                hierarchy: [
                    {name: 'Order', is_route: false, route: '' },
                    {name: 'Quotations', is_route: false, route: 'quotation-order-list' },
                ]
            },
            role: "Quotation Orders",
            title: 'List  of Quotation Orders'
        }
    },


    //profile
    {
        name: 'my-profile',
        path: Config.admin_prefix + '/my-profile',
        component: ProfileComponent,
        meta: {
            navigation: {
                parent: 'my-profile',
                child: '',
            },
            title: 'My Profile'
        }

    },

    //history test
    {
        name: 'history-list',
        path: Config.admin_prefix + '/history-log',
        component: ListHistoryComponent,

        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'history-log',
                child: '',
                hierarchy: [
                    {name: 'History Log', is_route: false, route: 'history-list' },
                ]
            },
            role: "Histories",
            title: 'List of History'
        }
    },

    //reports


    {
        name: 'report',
        path:  Config.admin_prefix + '/reports',
        component: require('./components/report/report.component'),
        beforeEnter: requireRole,
        children: ReportRouter,
        meta: {
            navigation: {
                parent: 'reports',
                child: '',
            },
            role: "Reports",
            title: 'Reports'
        }
    },
    {
        name: 'supplier-report-list',
        path: Config.admin_prefix + '/reports/supplier',
        component: ListSupplierReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-report',
                child: 'supplier-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Supplier Local', is_route: false, route: 'supplier-report-list' },
                ]
            },
            role: "Reports",
            title: 'Supplier Report'
        }
    },
    {
        name: 'supplier-abroad-report-list',
        path: Config.admin_prefix + '/reports/supplier-abroad',
        component: ListSupplierAbroadReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'supplier-report',
                child: 'supplier-abroad-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Supplier Abroad', is_route: false, route: 'supplier-abroad-report-list' },
                ]
            },
            role: "Reports",
            title: 'Supplier Abroad Report'
        }
    },
    {
        name: 'inventory-report-list',
        path: Config.admin_prefix + '/reports/inventory',
        component: ListInventoryReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'inventory-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inventory', is_route: false, route: 'inventory-report-list' },
                ]
            },
            role: "Reports",
            title: 'Inventory Report'
        }
    },

    {
        name: 'franchise-report-list',
        path: Config.admin_prefix + '/reports/franchise',
        component: ListFranchiseReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'most-sold-report',
                child: 'franchise-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Most Sold', is_route: false, route: '' },
                    {name: 'Franchise', is_route: false, route: 'franchise-report-list' },
                ]
            },
            role: "Reports",
            title: 'Franchise Report'
        }
    },
    {
        name: 'client-report-list',
        path: Config.admin_prefix + '/reports/clients',
        component: ListClientReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'most-sold-report',
                child: 'client-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Most Sold', is_route: false, route: '' },
                    {name: 'Client', is_route: false, route: 'client-report-list' },
                ]
            },
            role: "Reports",
            title: 'Client Report'
        }
    },
    // {
    //     name: 'invoice-order-report-list',
    //     path: Config.admin_prefix + '/reports/invoice-order',
    //     component: ListInvoiceOrderReportComponent,
    //     beforeEnter: requireRole,
    //     meta: {
    //         navigation: {
    //             parent: 'reports',
    //             child: 'invoice-order-report',
    //         },
    //         role: "Reports",
    //         title: 'Invoice Order Report'
    //     }
    // },
    // {
    //     name: 'cdr-order-report-list',
    //     path: Config.admin_prefix + '/reports/cdr-order',
    //     component: ListCdrOrderReportComponent,
    //     beforeEnter: requireRole,
    //     meta: {
    //         navigation: {
    //             parent: 'reports',
    //             child: 'cdr-order-report',
    //         },
    //         role: "Reports",
    //         title: 'CDR Order Report'
    //     }
    // },
    {
        name: 'sale-per-item-report-list',
        path: Config.admin_prefix + '/reports/sale-per-item',
        component: ListSalesPerItemReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'sales',
                child: 'sale-per-item',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Sales', is_route: false, route: '' },
                    {name: 'Sales Per Items', is_route: false, route: 'sale-per-item-report-list' },
                ]
            },
            role: "Reports",
            title: 'Sale per Item'
        }
    },
    {
        name: 'total-sale-report-list',
        path: Config.admin_prefix + '/reports/total-sale',
        component: ListTotalSaleReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'sales',
                child: 'total-sale',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Sales', is_route: false, route: '' },
                    {name: 'Total Sales', is_route: false, route: 'total-sale-report-list' },
                ]
            },
            role: "Reports",
            title: 'Total Sales'
        }
    },
    {
        name: 'sale-per-color-report-list',
        path: Config.admin_prefix + '/reports/sale-per-color',
        component: ListSalesPerColorReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'sales',
                child: 'sale-per-color',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Sales', is_route: false, route: '' },
                    {name: 'Sales Per Color', is_route: false, route: 'sale-per-color-report-list' },
                ]
            },
            role: "Reports",
            title: 'Sale per Color'
        }
    },
    //return
    {
        name: 'return-report',
        path:  Config.admin_prefix + '/reports/returns',
        component: require('./components/report/return/return-report.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'return-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Return', is_route: false, route: 'return-report' },
                ]
            },
            role: "Reports",
            title: 'Return Reports'
        }
    },
    //sale per price
    {
        name: 'sale-per-price-report-list',
        path: Config.admin_prefix + '/reports/sale-per-price',
        component: require('./components/report/sales/list-sale-per-price.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'sales',
                child: 'sale-per-price-report-list',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Sales', is_route: false, route: '' },
                    {name: 'Sale Per Price', is_route: false, route: 'sale-per-price-report-list' },
                ]
            },
            role: "Reports",
            title: 'Sale per Price'
        }
    },
    {
        name: 'sale-per-brand-report-list',
        path: Config.admin_prefix + '/reports/sale-per-brand',
        component: ListSalesPerBrandReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'sales',
                child: 'sale-per-brand',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Sales', is_route: false, route: '' },
                    {name: 'Sales Per Brand/CAT', is_route: false, route: 'sale-per-brand-report-list' },
                ]
            },
            role: "Reports",
            title: 'Sale per Brand'
        }
    },
    {
        name: 'per-item-supplier-report-list',
        path: Config.admin_prefix + '/reports/per-item-supplier',
        component: listPerItemSupplierReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'purchases-report',
                child: 'per-item-supplier',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Purchases', is_route: false, route: '' },
                    {name: 'Per Item/Supplier', is_route: false, route: 'per-item-supplier-report-list' },
                ]
            },
            role: "Reports",
            title: 'Per Item Supplier Report'
        }
    },
    //per brand-supplier-report
    {
        name: 'per-brand-supplier-report-list',
        path: Config.admin_prefix + '/reports/per-brand-supplier',
        component: listPerBrandSupplierReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'purchases-report',
                child: 'per-brand-supplier-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Purchases', is_route: false, route: '' },
                    {name: 'Per Brand', is_route: false, route: 'per-brand-supplier-report-list' },
                ]
            },
            role: "Reports",
            title: 'Per Brand Supplier Report'
        }
    },
    //per supplier-report
    {
        name: 'per-supplier-report-list',
        path: Config.admin_prefix + '/reports/per-supplier',
        component: listPerSupplierReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'purchases-report',
                child: 'per-supplier-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Purchases', is_route: false, route: '' },
                    {name: 'Per Supplier', is_route: false, route: 'per-supplier-report-list' },
                ]
            },
            role: "Reports",
            title: 'Per Supplier Report'
        }
    },
    //per item-color-supplier-report
    {
        name: 'per-item-color-supplier-report-list',
        path: Config.admin_prefix + '/reports/per-item-color-supplier',
        component: listPerItemColorSupplierReportComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'purchases-report',
                child: 'per-item-color-supplier-report',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Purchases', is_route: false, route: '' },
                    {name: 'Per Item/Color', is_route: false, route: 'per-item-color-supplier-report-list' },
                ]
            },
            role: "Reports",
            title: 'Per Item Color Supplier Report'
        }
    },
    //supplier report
    {
        name: 'report-supplier-summary',
        path: Config.admin_prefix + '/reports/supplier-summary',
        component: require('./components/report/supplier-summary.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-supplier-summary',
            },
            role: "Reports",
            title: 'Supplier Summary Report'
        }
    },

    //promos
    {
        name: 'promo-create',
        path: Config.admin_prefix + '/promos/create',
        component: CrupPromoComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'promos',
                child: '',
                hierarchy: [
                    {name: 'Promos', is_route: true, route: 'branch-create' },
                    {name: 'New Promo', is_route: false, route: 'promo-create' },
                ]
            },
            role: "Promos",
            title: 'Create Promos'
        }
    },
    {
        name: 'promo-update',
        path: Config.admin_prefix + '/promos/:id/edit',
        component: CrupPromoComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'promos',
                child: '',
                hierarchy: [
                    {name: 'Promos', is_route: true, route: 'branch-create' },
                    {name: 'Edit Promo', is_route: false, route: 'promo-update' },
                ]
            },
            role: "Promos",
            title: 'Update Promos'
        }
    },
    {
        name: 'promo-list',
        path: Config.admin_prefix + '/promos',
        component: ListPromoComponent,
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'promo',
                child: '',
                hierarchy: [
                    {name: 'Promos', is_route: false, route: 'promo-list' },
                ]
            },
            role: "Promos",
            title: 'List of Promos'
        }
    },

    {
        name: 'import-data',
        path: Config.admin_prefix + '/import-data',
        component: ImportComponent,
         meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Import Data'
        }
    },


    {
        name: 'sample-list',
        path: Config.admin_prefix + '/samples',
        component: ListSampleComponent
    }
];
