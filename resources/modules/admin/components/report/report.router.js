
import Config from '../../../../configs/app.config';

function requireRole(to, from, next) {

    const authRoles = JSON.parse(auth.admin_role.roles);


    const found = authRoles.some(el => el == to.meta.role);

    if (found) {
        next();

    }else{
        next(prefix + '/my-profile');
    }




}

export default [
    {
        name: 'report-balance-lapsed',
        path:  Config.admin_prefix + '/reports/balance/lapsed',
        component: require('./balance/lapsed.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-balance-lapsed',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Balance Lapsed', is_route: false, route: 'report-balance-lapsed' },
                ]
            },
            role: "Reports",
            title: 'Reports'
        }
    },

    {
        name: 'report-inter-office-items-in',
        path:  Config.admin_prefix + '/reports/inter-office/items-in',
        component: require('./inter-office/items-in.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'reports-inter-office-item-in',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Inter Office In', is_route: false, route: 'report-inter-office-items-in' },
                ]
            },
            role: "Reports",
            title: 'Reports'
        }
    },
    {
        name: 'report-inter-office-items-in-summary',
        path:  Config.admin_prefix + '/reports/inter-office/items-in-summary',
        component: require('./inter-office/items-in-summary.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'reports-inter-office-item-in-summary',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Inter Office Ins', is_route: false, route: 'report-inter-office-items-in-summary' },
                ]
            },
            role: "Reports",
            title: 'Reports'
        }
    },
    {
        name: 'report-inter-office-items-out',
        path:  Config.admin_prefix + '/reports/inter-office/items-out',
        component: require('./inter-office/items-out.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'reports-inter-office-item-out',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Inter Office Out', is_route: false, route: 'report-inter-office-items-out' },
                ]
            },
            role: "Reports",
            title: 'Reports'
        }
    },
    {
        name: 'report-inter-office-items-out-summary',
        path:  Config.admin_prefix + '/reports/inter-office/items-out-summary',
        component: require('./inter-office/items-out-summary.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'reports-inter-office-item-out-summary',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Inter Office Out Summary', is_route: false, route: 'report-inter-office-items-out-summary' },
                ]
            },
            role: "Reports",
            title: 'Reports'
        }
    },
    //walkin
    {
        name: 'report-walk-in',
        path:  Config.admin_prefix + '/reports/balance/walk-in',
        component: require('./balance/walk-in.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-walk-in',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Inter Office', is_route: false, route: '' },
                    {name: 'Balance Walk in', is_route: false, route: 'report-walk-in' },
                ]
            },
            role: "Reports",
            title: 'Walk in Report'
        }
    },

//bal price summary
    {
        name: 'report-price-a',
        path:  Config.admin_prefix + '/reports/price/summary-a',
        component: require('./price/price-a.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-price-a',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Price Summary A', is_route: false, route: 'report-price-a' },
                ]
            },
            role: "Reports",
            title: 'Price Summary A'
        }
    },
    {
        name: 'report-price-b',
        path:  Config.admin_prefix + '/reports/price/summary-b',
        component: require('./price/price-b.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-price-b',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Price Summary B', is_route: false, route: 'report-price-b' },
                ]
            },
            role: "Reports",
            title: 'Price Summary B'
        }
    },
    {
        name: 'report-price-c',
        path:  Config.admin_prefix + '/reports/price/summary-c',
        component: require('./price/price-c.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-price-c',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Price Summary C', is_route: false, route: 'report-price-c' },
                ]
            },
            role: "Reports",
            title: 'Price Summary C'
        }
    },
    {
        name: 'report-price-d',
        path:  Config.admin_prefix + '/reports/price/summary-d',
        component: require('./price/price-d.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-price-d',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Price Summary D', is_route: false, route: 'report-price-d' },
                ]
            },
            role: "Reports",
            title: 'Price Summary D'
        }
    },
    {
        name: 'report-price-summary',
        path:  Config.admin_prefix + '/reports/price/summary',
        component: require('./price/price-summary.component'),
        beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'reports',
                child: 'report-price-summary',
                hierarchy: [
                    {name: 'Report', is_route: false, route: '' },
                    {name: 'Price Summary', is_route: false, route: 'report-price-summary' },
                ]
            },
            role: "Reports",
            title: 'Price Summary Report'
        }
    },

]
