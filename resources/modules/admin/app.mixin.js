import AlertService from "../../cores/services/alert/alert.service";

const alertService = new AlertService();
export default {
    data: function () {
        return {};
    },
    methods: {

        formatDate(date) {

            return moment(date).format('YYYY-MM-DD');
        },
        todayDate(date) {
            return moment(date).format('YYYY-MM-DD')
        },
        beforeDue(date, days) {

            const today = this.todayDate(new Date);
            var end = moment(date);
            let remaining = end.diff(today, "days");


            if (remaining >= 1 && remaining <= days) {
                return true;
            }

            return false;

        },
        asQueryParams(data) {
            const searchParams = new URLSearchParams();
            const search = data;
            Object.keys(search).forEach(key => searchParams.append(key, search[key]));

            return "?" + searchParams.toString();
        },

        formatNumber(data) {
            var number = Math.round(parseFloat(data) * 100) / 100;

            number = number.toFixed(2);
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");


        },
        popErrors(errors) {
            $.notifyClose();
            Object.entries(errors).forEach(([key, value]) => {

                alertService.errorWithMessage(value[0]);

            });
        },
        hasCost: function () {
            let data = JSON.parse(localStorage.getItem('authAdmin'));
            let bool = false;
            if (data.admin_role_id == 1 || data.admin_role_id == 2|| data.admin_role_id == 3) {
                bool = true;
            }

            return bool;
        },
        isSuperAdmin: function () {
            let data = JSON.parse(localStorage.getItem('authAdmin'));
            let bool = false;
            if (data.admin_role_id == 1) {
                bool = true;
            }
            return bool;
        },
        isFranchise: function () {
            let data = JSON.parse(localStorage.getItem('authAdmin'));
            let bool = false;
            if (data.branch.branch_type_id == 1) {
                bool = true;
            }
            return bool;
        },
        isWarehousManager: function () {
            let data = JSON.parse(localStorage.getItem('authAdmin'));
            let bool = false;
            if (data.admin_role_id == 3) {
                bool = true;
            }
            return bool;
        },
        validateQuantity(element) {

            var max_chars = 2;
            if (arguments[1].char !== "\b" && arguments[1].key !== "Left" && arguments[1].key !== "Right") {
                if (element.value.length >= max_chars) {
                    return false;
                } else if (isNaN(arguments[1].char)) {
                    return false;
                }
            }
        },
        getOrganizeItemAmount(type, data) {
            let amount = 0;

            if (type == 1) {
                amount = data.item.price_a
            }
            if (type == 2) {
                amount = data.item.price_b
            }
            if (type == 3) {
                amount = data.item.price_c
            }
            if (type == 4) {
                amount = data.item.price_d
            }
            if (type == 5) {
                amount = data.item.srp
            }

            return amount;
        },
        getSelectedItemAmount(type, data) {
            let amount = 0;

            if (type == 1) {
                amount = data.price_a
            }
            if (type == 2) {
                amount = data.price_b
            }
            if (type == 3) {
                amount = data.price_c
            }
            if (type == 4) {
                amount = data.price_d
            }
            if (type == 5) {
                amount = data.srp
            }

            return amount;
        }

    },

};
