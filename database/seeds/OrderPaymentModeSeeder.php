<?php

use Illuminate\Database\Seeder;
use App\OrderPaymentMode;

class OrderPaymentModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        OrderPaymentMode::truncate();

        $data = [
            ['name' => 'None', 'label' => 'none'],
            ['name' => 'Cash Payment', 'label' => 'cash-payment'],
            ['name' => 'Credit Card', 'label' => 'credit-card'],
            ['name' => 'Cheque', 'label' => 'cheque'],
        ];

        OrderPaymentMode::insert($data);
    }
}
