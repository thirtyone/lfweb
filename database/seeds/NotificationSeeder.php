<?php

use Illuminate\Database\Seeder;
use App\NotificationType;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the notification type table first
         NotificationType::truncate();

        //Create the notification type
        $data = [
            ['name' => 'Item'],
            ['name' => 'Order'],
        ];

        NotificationType::insert($data);
    }
}
