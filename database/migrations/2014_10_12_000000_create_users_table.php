<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');

	        $table->integer('admin_id')->unsigned();
	        $table->foreign('admin_id')
		        ->references('id')
		        ->on('admins')
		        ->onDelete('cascade');

			$table->integer('user_type_id')->unsigned();
			$table->foreign('user_type_id')
				->references('id')
				->on('user_types')
				->onDelete('cascade');

			$table->string('name');
			$table->string('email')->unique();
			$table->string('password');

			$table->string('address');
			$table->string('contact_number');
			$table->string('bank_name');
			$table->string('bank_account_number');
			$table->string('bank_account_name');
			$table->string('credit_limit');
			$table->string('payment_terms');
			$table->string('tin_number');
			$table->string('business_style');

			$table->rememberToken();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
