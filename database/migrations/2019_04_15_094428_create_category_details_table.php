<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_details', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('category_header_id')->unsigned();
	        $table->foreign('category_header_id')
		        ->references('id')
		        ->on('category_headers')
		        ->onDelete('cascade');

	        $table->string('name');
	        $table->string('label');
	        $table->string('slug');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_details');
    }
}
