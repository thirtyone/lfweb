<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
	        $table->engine = "MyISAM";

            $table->increments('id');

	        $table->integer('promo_type_id')->unsigned();
	        $table->foreign('promo_type_id')
		        ->references('id')
		        ->on('promo_types')
		        ->onDelete('cascade');
	        $table->integer('admin_id')->unsigned();
	        $table->foreign('admin_id')
		        ->references('id')
		        ->on('admins')
		        ->onDelete('cascade');

            $table->string('name');
            $table->integer('amount');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
