<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesFieldItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            //
            $table->double('price_a');
            $table->double('price_b');
            $table->double('price_c');
            $table->double('price_d');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            //

            $table->dropColumn('price_a');
            $table->dropColumn('price_b');
            $table->dropColumn('price_c');
            $table->dropColumn('price_d');
        });
    }
}
