<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {

        	$table->engine = "MyISAM";
            $table->increments('id');

            $table->integer('admin_id')->unsigned();
	        $table->foreign('admin_id')
		        ->references('id')
		        ->on('admins')
                ->onDelete('cascade');
            $table->string('action');

            $table->integer('admin_inter_id')->unsigned();
	        $table->foreign('admin_inter_id')
		        ->references('id')
		        ->on('admins')
                ->onDelete('cascade');

            $table->integer('status_option_id')->unsigned();
            $table->foreign('status_option_id')
                ->references('id')
                ->on('status_options')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
