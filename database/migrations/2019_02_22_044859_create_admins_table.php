<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
	        $table->engine = "MyISAM";
	        $table->increments('id');

	        $table->integer('branch_id')->unsigned();
	        $table->foreign('branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');
	        $table->integer('admin_role_id')->unsigned();
	        $table->foreign('admin_role_id')
		        ->references('id')
		        ->on('admin_roles')
		        ->onDelete('cascade');

	        $table->string('address');
	        $table->string('contact_number');
	        $table->string('name');
	        $table->string('email')->unique();
	        $table->string('username')->unique();
	        $table->string('password');
	        $table->string('first_name');
	        $table->string('last_name');
	        $table->string('middle_name');
	        $table->integer('max_discount');

	        $table->rememberToken();
	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
