<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {

	        $table->engine = "MyISAM";
            $table->increments('id');

	        $table->integer('branch_type_id')->unsigned();
	        $table->foreign('branch_type_id')
		        ->references('id')
		        ->on('branch_types')
		        ->onDelete('cascade');
	        
	        
            $table->string('name');
            $table->string('contact_number');
            $table->string('address');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
