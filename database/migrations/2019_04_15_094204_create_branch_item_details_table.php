<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_item_details', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('item_id')->unsigned();
	        $table->foreign('item_id')
		        ->references('id')
		        ->on('items')
		        ->onDelete('cascade');

	        $table->integer('branch_id')->unsigned();
	        $table->foreign('branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');

	        $table->integer('supplier_id')->unsigned();
	        $table->foreign('supplier_id')
		        ->references('id')
		        ->on('suppliers')
		        ->onDelete('cascade');

	        $table->integer('branch_item_header_id')->unsigned();
	        $table->foreign('branch_item_header_id')
		        ->references('id')
		        ->on('branch_item_headers')
		        ->onDelete('cascade');

	        $table->double('amount');
	        $table->integer('quantity');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_item_details');
    }
}
