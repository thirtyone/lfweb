<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('category_header_id')->unsigned();
	        $table->foreign('category_header_id')
		        ->references('id')
		        ->on('category_headers')
		        ->onDelete('cascade');

	        $table->integer('category_detail_id')->unsigned();
	        $table->foreign('category_detail_id')
		        ->references('id')
		        ->on('category_details')
		        ->onDelete('cascade');

	        $table->integer('package_id')->unsigned();
	        $table->foreign('package_id')
		        ->references('id')
		        ->on('packages')
		        ->onDelete('cascade');

	        $table->integer('supplier_id')->unsigned();
	        $table->foreign('supplier_id')
		        ->references('id')
		        ->on('suppliers')
		        ->onDelete('cascade');


	        $table->integer('brand_id')->unsigned();
	        $table->foreign('brand_id')
		        ->references('id')
		        ->on('brands')
		        ->onDelete('cascade');

	        $table->string('name');
	        $table->integer('restock_at');
	        $table->string('sku');
	        $table->string('code');
			$table->double('srp');
			$table->double('srp_dealer');
			$table->double('srp_project');
			$table->double('srp_walkin');
			$table->double('srp_modern_trade');
	        $table->double('package_option');
			$table->longText('description');
	        $table->string('slug');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
