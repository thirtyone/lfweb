<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_colors', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('item_id')->unsigned();
	        $table->foreign('item_id')
		        ->references('id')
		        ->on('items')
		        ->onDelete('cascade');

	        $table->string('name');
	        $table->string('path');
	        $table->string('file_name');
	        $table->string('primary');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_colors');
    }
}
