<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchItemHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_item_headers', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('branch_id')->unsigned();
	        $table->foreign('branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');
	        $table->integer('is_order');

	        $table->integer('supplier_id')->unsigned();
	        $table->foreign('supplier_id')
		        ->references('id')
		        ->on('suppliers')
		        ->onDelete('cascade');


	        $table->integer('inter_branch_id')->unsigned();
	        $table->foreign('inter_branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');

	        $table->integer('status_option_id')->unsigned();
	        $table->foreign('status_option_id')
		        ->references('id')
		        ->on('status_options')
		        ->onDelete('cascade');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_item_headers');
    }
}
