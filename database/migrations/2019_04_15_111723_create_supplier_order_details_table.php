<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_order_details', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('supplier_order_header_id')->unsigned();
	        $table->foreign('supplier_order_header_id')
		        ->references('id')
		        ->on('supplier_order_headers')
		        ->onDelete('cascade');

	        $table->integer('item_id')->unsigned();
	        $table->foreign('item_id')
		        ->references('id')
		        ->on('items')
		        ->onDelete('cascade');
	        $table->integer('branch_id')->unsigned();
	        $table->foreign('branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');
	        $table->integer('quantity');
	        $table->string('amount');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_order_details');
    }
}
