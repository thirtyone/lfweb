<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
	        $table->integer('branch_item_detail_id')->unsigned();
	        $table->foreign('branch_item_detail_id')
		        ->references('id')
		        ->on('branch_item_details')
		        ->onDelete('cascade');


	        $table->integer('order_header_id')->unsigned();
	        $table->foreign('order_header_id')
		        ->references('id')
		        ->on('order_headers')
		        ->onDelete('cascade');

	        $table->integer('status_option_id')->unsigned();
	        $table->foreign('status_option_id')
		        ->references('id')
		        ->on('status_options')
		        ->onDelete('cascade');

	        $table->integer('item_id')->unsigned();
	        $table->foreign('item_id')
		        ->references('id')
		        ->on('items')
		        ->onDelete('cascade');

	        $table->integer('promo_id')->unsigned();
	        $table->foreign('promo_id')
		        ->references('id')
		        ->on('promos')
		        ->onDelete('cascade');
	        $table->string('discount');
	        $table->string('quantity');
	        $table->string('amount');
	        $table->string('total');
	        $table->integer('return_quantity');
//	        $table
	        $table->string('comment');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
