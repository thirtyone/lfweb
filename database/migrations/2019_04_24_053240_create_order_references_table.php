<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_references', function (Blueprint $table) {
	        $table->engine = "MyISAM";
	        $table->increments('id');

	        $table->integer('order_header_id')->unsigned();
	        $table->foreign('order_header_id')
		        ->references('id')
		        ->on('order_headers')
		        ->onDelete('cascade');

	        $table->integer('item_id')->unsigned();
	        $table->integer('discount');

	        $table->foreign('item_id')
		        ->references('id')
		        ->on('items')
		        ->onDelete('cascade');

	        $table->string('quantity');
	        $table->string('amount');
	        $table->string('total')->nullable()->default('0');
	        $table->string('return_quantity');
	        $table->string('comment');
	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_references');
    }
}
