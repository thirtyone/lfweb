<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPaymentNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payment_notes', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->integer('order_header_id')->unsigned();
            $table->foreign('order_header_id')
                ->references('id')
                ->on('order_headers')
                ->onDelete('cascade');
            $table->longText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_payment_notes');
    }
}
