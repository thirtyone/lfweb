<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentStatusOptionOrderHeaderTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_headers', function (Blueprint $table) {
            //


	        $table->integer('payment_status_option_id')->unsigned()->default(5)->after('id');
	        $table->foreign('payment_status_option_id')
		        ->references('id')
		        ->on('status_options')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_headers', function (Blueprint $table) {
            //

	        $table->dropColumn('payment_status_option_id');
        });
    }
}
