<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierOrderHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_order_headers', function (Blueprint $table) {
	        $table->engine = "MyISAM";
	        $table->increments('id');
	        $table->integer('admin_id')->unsigned();
	        $table->foreign('admin_id')
		        ->references('id')
		        ->on('admins')
		        ->onDelete('cascade');
	        $table->integer('supplier_id')->unsigned();
	        $table->foreign('supplier_id')
		        ->references('id')
		        ->on('suppliers')
		        ->onDelete('cascade');

	        $table->integer('status_option_id')->unsigned()->default('1');
	        $table->foreign('status_option_id')
		        ->references('id')
		        ->on('status_options')
		        ->onDelete('cascade');


	        $table->string('invoice_number');
	        $table->date('invoice_date');
	        $table->string('vat');
	        $table->string('total');
	        $table->string('sub_total');
	        $table->string('grand_total');
	        $table->longText('notes');
	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_order_headers');
    }
}
