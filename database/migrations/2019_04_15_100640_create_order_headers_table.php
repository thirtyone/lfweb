<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_headers', function (Blueprint $table) {
	        $table->engine = "MyISAM";
            $table->increments('id');
            $table->morphs('customer');
	        $table->integer('order_payment_type_id')->unsigned();
	        $table->foreign('order_payment_type_id')
		        ->references('id')
		        ->on('order_payment_types')
		        ->onDelete('cascade');


	        $table->integer('order_payment_mode_id')->unsigned()->default('1');
	        $table->foreign('order_payment_mode_id')
		        ->references('id')
		        ->on('order_payment_modes')
		        ->onDelete('cascade');



	        $table->integer('order_type_id')->unsigned();
	        $table->foreign('order_type_id')
		        ->references('id')
		        ->on('order_types')
		        ->onDelete('cascade');

	        $table->integer('branch_id')->unsigned();
	        $table->foreign('branch_id')
		        ->references('id')
		        ->on('branches')
		        ->onDelete('cascade');

	        $table->integer('status_option_id')->unsigned()->default('1');
	        $table->foreign('status_option_id')
		        ->references('id')
		        ->on('status_options')
		        ->onDelete('cascade');

	        $table->string('order_number');
	        $table->string('invoice_number');
	        $table->string('tin_number');
	        $table->date('invoice_date');
	        $table->longText('notes');
	        $table->string('order_payment_notes');
	        $table->string('vat')->nullable()->default('0');
	        $table->string('total')->nullable()->default('0');
	        $table->string('sub_total');
	        $table->string('grand_total')->nullable()->default('0');
	        $table->integer('discount');
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_headers');
    }
}
