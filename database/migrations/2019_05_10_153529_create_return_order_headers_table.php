<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnOrderHeadersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('return_order_headers', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');
			$table->integer('order_header_id')->unsigned();
			$table->foreign('order_header_id')
				->references('id')
				->on('order_headers')
				->onDelete('cascade');

			$table->integer('vat');
			$table->integer('total');
			$table->integer('grand_total');

			$table->integer('status_option_id')->unsigned();
			$table->foreign('status_option_id')
				->references('id')
				->on('status_options')
				->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('return_order_headers');
	}
}
