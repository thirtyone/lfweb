<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationTypeIdAndSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('histories', function (Blueprint $table) {
            //
            $table->string('slug')->after('action');
            $table->integer('notification_id')->after('slug');
            $table->integer('branch_id')->after('notification_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('histories', function (Blueprint $table) {
            //
            $table->dropColumn('slug');
            $table->dropColumn('notification_id');
            $table->dropColumn('branch_id');
        });
    }
}
