<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnOrderDetailsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('return_order_details', function (Blueprint $table) {

			$table->engine = "MyISAM";

			$table->increments('id');


			$table->integer('return_order_header_id')->unsigned();
			$table->foreign('return_order_header_id')
				->references('id')
				->on('return_order_headers')
				->onDelete('cascade');

			$table->integer('order_detail_id')->unsigned();
			$table->foreign('order_detail_id')
				->references('id')
				->on('order_details')
				->onDelete('cascade');

			$table->integer('item_id')->unsigned();
			$table->foreign('item_id')
				->references('id')
				->on('items')
				->onDelete('cascade');

			$table->integer('quantity');
			$table->integer('amount');
			$table->integer('total');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('return_order_details');
	}
}
