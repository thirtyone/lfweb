<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BranchItemDetail extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
	protected $fillable = [ 'created_at', 'supplier_id', 'updated_at','item_id', 'branch_item_header_id', 'branch_id','quantity', 'amount', 'deleted_at'];




	public function supplier() {
	    return $this->belongsTo('App\Supplier');
	}


	public function item() {
	    return $this->belongsTo('App\Item');
	}

	public function branch() {
	    return $this->belongsTo('App\Branch');
	}
}
