<?php

namespace App\Imports;

use App\Brand;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Validator;

class BrandsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return BrandsImport|null
     */
    protected $validator;
    public $collection;
  
    public function __construct(){
      $this->validator = new CustomValidationError();
    }

    public function collection(Collection $collection)
    {
        // $errorLines = array();
        // //checking the lines of the duplicate names
        // foreach ($collection as $k => $row){
        //   if(count($errorLines) < 10){
        //     if($this->brand($row['name']) != NULL){
        //       $errorLines[]= $k + 2;
        //     }
        //   } else {
        //     break;
        //   }
        // }
        // //error blocker
        // if(count($errorLines) > 0){
        //   $message = 'Duplicate brand name in line ';
        //   return $this->validator->customValidation($errorLines, $message);
        // }

        $this->validator->checkingDataBrand($collection);
        
        //saving data from bulk upload
        foreach ($collection as $row){
          $brand = Brand::create([
            'name'     => $row['name'],
            'label'    => '',
            'slug'     => str_slug($row['name']),
            'deleted_at' => NULL
          ]);
        }
    }

    // public function brand($data){
    //   $data = Brand::whereName($data)->first();
    //   return $data; 
    // }

}