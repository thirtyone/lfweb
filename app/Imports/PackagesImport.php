<?php

namespace App\Imports;

use App\Package;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PackagesImport implements ToCollection, WithHeadingRow
{

    public $collection;

    public function collection(Collection $collection)
    {
        foreach ($collection as $row){
            if($this->package($row['name']) == NULL){
                $package = Package::create([
                    'name'     => $row['name'],
                    'label'    => '',
                    'slug' => str_slug($row['name']),
                    'deleted_at' => NULL
                ]);
            }
        }
        // return new Package([
        //     'name'     => $row['name'],
        //     'label'    => '',
        //     'slug' => str_slug($row['name']),
        //     'deleted_at' => NULL
        // ]);
    }

    public function package($data){
      $data = Package::whereName($data)->first();
      return $data; 
    }
}
