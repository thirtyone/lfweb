<?php

namespace App\Imports;

use App\Supplier;
use App\SupplierType;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SuppliersImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return SuppliersImport|null
     */
    protected $validator;
    public $collection;
  
    public function __construct(){
      $this->validator = new CustomValidationError();
    }

    public function collection(Collection $collection)
    {

      $this->validator->checkingDataSupplier($collection);

      // $errorLines = array();
      // //checking the lines of the duplicate names
      // foreach ($collection as $k => $row){
      //   if(count($errorLines) < 10){
      //     if($this->supplier($row['name']) != NULL){
      //       $errorLines[]= $k + 2;
      //     }
      //   } else {
      //     break;
      //   }
      // }
      // //error blocker
      // if(count($errorLines) > 0){
      //   $message = 'Duplicate supplier name in line ';
      //   return $this->validator->customValidation($errorLines, $message);
      // }

      //saving data from bulk upload
      foreach ($collection as $row){
        $supplier = Supplier::create([
          'supplier_type_id'  => $this->supplierType($row['supplier_type']),
          'payment_terms'    => $row['payment_terms'] == null ? 0 : $row['payment_terms'],
          'name' => $row['name'],
          'contact_number' => $row['contact_number'] == null ? '' : $row['contact_number'],
          'address' => $row['address'] == null ? '' : $row['address'],
          'slug' => str_slug($row['name']),
          'deleted_at' => NULL
        ]);
      }
    }

    // public function supplier($data){
    //   $data = Supplier::whereName($data)->first();
    //   return $data; 
    // }

    public function supplierType($data){
      $data = SupplierType::whereName(strtoupper($data))->first();
      return $data->id;
    }

}