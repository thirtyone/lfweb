<?php

namespace App\Imports;

use App\BranchItemHeader;
use App\BranchItemDetail;
use App\Supplier;
use App\Branch;
use App\Item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BranchItemHeaderImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return BranchItemHeader|null
     */
    public $collection;

    public function collection(Collection $collection)
    {
      foreach ($collection as $row){
          $branchHeader = BranchItemHeader::create([
               'supplier_id'     => $this->suppplier($row['supplier']),
               'branch_id'    => $this->branch($row['branch']),
               'is_order' => 0,
               'inter_branch_id' => 0,
               'status_option_id' => 2,
               'deleted_at' => NULL
          ]);
          $branchDetail  = BranchItemDetail::create([  
              'supplier_id' => $this->suppplier($row['supplier']),              
              'branch_item_header_id' => $branchHeader->id,
              'item_id' => $this->item($row['name']),
              'branch_id'    => $this->branch($row['branch']),
              'amount'  => $row['srp'] == NULL ? '0' : $row['srp'],
              'quantity' => $row['quantity'] == NULL ? '0' : $row['quantity'],
          ]);
      }        
    }

    public function item($data){
      $data = Item::whereName($data)->first();
      return $data->id; 
    }

    public function suppplier($data){
      $data = Supplier::whereName($data)->first();
      return $data->id; 
    }

    public function branch($data){
      $data = Branch::whereName($data)->first();
      return $data->id;
    }

}