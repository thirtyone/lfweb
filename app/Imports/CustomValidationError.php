<?php

namespace App\Imports;
use App\Branch;
use App\CategoryDetail;
use App\CategoryHeader;
use App\Supplier;
use App\Brand;
use App\Admin;
use App\Item;
use App\SupplierType;
use App\StatusOption;
use App\SupplierOrderHeader;
use App\BranchItemDetail;
use App\Package;

use Illuminate\Support\Facades\Validator;

class CustomValidationError 
{
	public function customValidation($line, $message){
		$data = ['validator' => 1];
		$validator = Validator::make($data, ['validator' => 'string',],
		['validator.string' => $message . $line . '.']);
		return $validator->validate();
	}

  public function checkData($message){
    $data = ['validator' => 1];
    $validator = Validator::make($data, ['validator' => 'string',],
    ['validator.string' => $message . '.']);
    return $validator->validate();
  }

  public function checkingDataBrand($data){
    foreach ($data as $key => $value) {
      if(isset($value['name']) && $value['name']){
        if($this->brand($value['name']) != null){
          $message = 'Brand name already exist in line ';
          $this->validationAction($key, $message);
        }
      }
    }
  }

  public function checkingDataSupplier($data){
    foreach ($data as $key => $value) {
      if(isset($value['name']) && $value['name']){
        if($this->supplier($value['name']) != null){
          $message = 'Supplier name already exist in line ';
          $this->validationAction($key, $message);
        }
      }
    }
  }

  public function checkingData($data){
    
    foreach ($data as $key => $value) {
      //check for false value
      $value->filter(function($v, $k) use ($key) {
        if($v === false || $v === null){
          if($v == null){
            $message = ucfirst($k) . ' header data cannot be null in line ';
          } else {
            $message = ucfirst($k) . ' header data needs to be typed manually in line ';
          }
          $this->validationAction($key, $message);
        }
      });

      //subcategory import
      if(isset($value['category_header']) && $value['category_header']){
        if($this->subcategory($value['category_header']) != null && $this->subcategory($value['category_header']) != 'N/A'){
          $message = 'Sub category name already exist in line ';
          $this->validationAction($key, $message);
        }
      }

      //category import
      if((isset($value['name']) && $value['name']) && !isset($value['category_header'])){
        if($this->category($value['name']) != null && $this->category($value['name']) != 'N/A'){
          $message = 'Category name already exist in line ';
          $this->validationAction($key, $message);
        }
      }

      //item import
      if(isset($value['category']) && $value['category']){
        if($this->category($value['category']) == null && $this->category($value['category']) != 'N/A'){
          $message = 'Category name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['subcategory']) && $value['subcategory']){
        if($this->subcategory($value['subcategory']) == null && $this->subcategory($value['subcategory']) != 'N/A'){
          $message = 'Sub category name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['brand']) && $value['brand']){
        if($this->brand($value['brand']) == null && $this->brand($value['brand']) != 'N/A'){
          $message = 'Brand name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }


      if(isset($value['subcategory']) && $value['subcategory']){
        if($this->supplier($value['supplier']) == null && $this->supplier($value['supplier']) != 'N/A'){
          $message = 'Supplier name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['branch'])){
        if($this->branch($value['branch']) == null){
          $message = 'Branch name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['admin']) && $value['admin']){
        if($this->admin($value['admin']) == null && $this->admin($value['admin']) != 'N/A'){
          $message = 'Admin name does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['invoice_number']) && $value['invoice_number']){
        if($this->invoiceNumber($value['invoice_number']) == null && $this->invoiceNumber($value['invoice_number']) != 'N/A'){
          $message = 'Invoice number already exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['supplier_type']) && $value['supplier_type']){
        if($this->supplier_type($value['supplier_type']) == null && $this->supplier_type($value['supplier_type']) != 'N/A'){
          $message = 'Supplier type does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['package_type']) && $value['package_type']){
        if($this->package($value['package_type']) == null && $this->package($value['package_type']) != 'N/A'){
          $message = 'Package type does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['status_option']) && $value['status_option']){
        if($this->status_option($value['status_option']) == null && $this->status_option($value['status_option']) != 'N/A'){
          $message = 'Status option does not exist in line ';
          $this->validationAction($key, $message);
        }
      }

      if(isset($value['sku']) && $value['sku']){
        if($this->checkSku($value['name'], $value['branch'], $value['sku']) > 0){
          $message = 'SKU is already existing on this branch in line ';
          $this->validationAction($key, $message);
        }
      }

      if((isset($value['name']) && $value['name']) && (isset($value['sku']) && $value['sku'])){
        if($this->checkBranch($value['name'], $value['branch'], $value['sku']) > 0){
          // $message = 'Duplicate item details in line ';
          $message = "Item name is already exist on this branch in line ";
          $this->validationAction($key, $message);
        }
      }

    }
  }

  public function checkSku($name, $branch, $sku){
    // dd($sku);
    $branch = $this->branch($branch);
    $item = Item::whereSku($sku)->first();
    $data = BranchItemDetail::where('item_id', $item == null ? 0 : $item->id)
                      ->where('branch_id', $branch->id)
                      ->get();
    return count($data);
  }

  public function checkItemCode($data){
    $data = Item::whereCode($data)->first();
    return $data;
  }

  public function validationAction($line, $message){
    $errLine = $line + 2;
    $this->customValidation($errLine, $message);
  }

  private function checkBranch($name, $branch, $sku){
    $branch = $this->branch($branch);
    $item = Item::whereName(strval($name))->first();
    $data = BranchItemDetail::where('item_id', $item == null ? 0 : $item->id)
                      ->where('branch_id', $branch->id)
                      ->get();
    return count($data);
  }

	public function supplier($data){
      $data = Supplier::whereName(strval($data))->first();
      return $data;
  }

  public function package($data){
      $data = Package::whereName(strval($data))->first();
      return $data;
  }

  public function supplier_type($data){
      $data = SupplierType::whereName(strval($data))->first();
      return $data;
  }

  public function admin($data){
      $data = Admin::whereEmail(strval($data))->where('admin_role_id', 1)->orWhere('admin_role_id', 2)->first();
      return $data;
  }

  public function status_option($data){
      $data = StatusOption::whereName(strval($data))->first();
      return $data;
  }

	public function branch($data){
      $data = Branch::whereName(strval($data))->first();
      return $data;
    }

    public function subcategory($data){
      $data = CategoryDetail::whereName(strval($data))->first();
      return $data;
    }

    public function category($data){
      $data = CategoryHeader::whereName(strval($data))->first();
      return $data;
    }

    public function brand($data){
      $data = Brand::whereName(strval($data))->first();
      return $data;
    }

    public function invoiceNumber($data){
      $data = SupplierOrderHeader::where('invoice_number',$data)->get();
      return count($data);
    }
} 