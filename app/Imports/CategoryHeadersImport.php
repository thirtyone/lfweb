<?php

namespace App\Imports;

use App\CategoryHeader;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class CategoryHeadersImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return CategoryHeader|null
     */
    protected $validator;
    public $collection;

    public function __construct(){
      $this->validator = new CustomValidationError();
    }

    public function collection(Collection $collection)
    { 
      $this->validator->checkingData($collection);
      // $errorLines = array();
      // //checking the lines of the duplicate names
      // foreach ($collection as $k => $row){
      //   if(count($errorLines) < 10){
      //     if($this->categoryHeader($row['name']) != NULL){
      //       $errorLines[]= $k + 2;
      //     }
      //   } else {
      //     break;
      //   }
        
      // }
      // //error blocker
      // if(count($errorLines) > 0){
      //   $message = 'Duplicate category name in line ';
      //   return $this->validator->customValidation($errorLines, $message);
      // }
      //saving data from bulk upload
      foreach ($collection as $row) {
        $categoryHeader = CategoryHeader::create([
          'name'     => $row['name'],
          'label'    => '',
          'slug' => str_slug($row['name']),
          'deleted_at' => NULL
        ]);
      }
    }

    // public function categoryHeader($data){
    //   $data = CategoryHeader::whereName($data)->first();
    //   return $data; 
    // }

}