<?php

namespace App\Imports;

use App\Item;
use App\CategoryHeader;
use App\CategoryDetail;
use App\Package;
use App\Supplier;
use App\ItemColor;
use App\SrpHistory;
use App\Brand;
use App\SupplierOrderDetail;
use App\SupplierOrderHeader;
use App\SupplierType;
use App\StatusOption;
use App\BranchItemHeader;
use App\BranchItemDetail;
use App\Branch;
use App\Admin;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;


class ItemsImport implements ToCollection, WithHeadingRow, WithBatchInserts
{
    /**
     * @param array $row
     *
     * @return ItemsImport|null
     */
    protected $validator;
    public $collection;

    public function __construct(){
      $this->validator = new CustomValidationError();
    }

    public function collection(Collection $collection)
    {
        //check if theres a duplicate data in CSV (item name and sku)
        $name = array();
        $sku = array();
        foreach ($collection as $key => $value) {
          $name[] = $value['name'];
          $sku[] = $value['sku'];
        }
        $checkName = array_intersect($name, array_unique(array_diff_key($name, array_unique($name))));
        $checkSku = array_intersect($sku, array_unique(array_diff_key($sku, array_unique($sku))));
        if(!empty($checkName)) {
          foreach ($checkName as $key => $value) {
            $duplicateName = $value;
            break;
          }
          $message = 'Item name ' . $duplicateName . ' has a duplicate data in csv';
          $this->validator->checkData($message);
        } 
        if(!empty($checkSku)) {
          foreach ($checkSku as $key => $value) {
            $duplicateSku = $value;
            break;
          }
          $message = 'SKU ' . $duplicateSku . ' has a duplicate data in csv';
          $this->validator->checkData($message);
        } 

        //check if the datas are more than 1000
        if(count($collection) > 1001){
          $message = 'Bulk upload must not exceed 1000 datas';
          return $this->validator->checkData($message);
        }
        
        //validate datas
        $this->validator->checkingData($collection);
   
        //saving data from bulk upload
        foreach ($collection as $row){
          //change value non numeric to 0
          $row['srp'] = $this->isNumeric($row['srp']);
          $row['quantity'] = $this->isNumeric($row['quantity']);
          $row['price_a'] = $this->isNumeric($row['price_a']);
          $row['price_b'] = $this->isNumeric($row['price_b']);
          $row['price_c'] = $this->isNumeric($row['price_c']);
          $row['price_d'] = $this->isNumeric($row['price_d']);
          $row['unit_cost'] = $this->itemCost($row['name'], $this->isNumeric($row['unit_cost']));

          //saving items
          if($this->item($row['name']) == NULL){
            $item = Item::create([
               'status' => 1,
               'category_header_id' => $this->categoryHeader($row['category'])->id,
               'category_detail_id' => $this->categoryDetail($row['subcategory']) == null ? 0 : $this->categoryDetail($row['subcategory'])->id,
               'package_id' =>  $this->package($row['package_type'])->id,
               'package_option' =>  $row['package_type'],
               'supplier_id'  =>  $this->supplier($row['supplier'])->id,
               'brand_id' =>  $this->brand($row['brand'])->id,
               'name' =>  $row['name'],
               'restock_at' => $row['restock_at'],
               'sku'  =>  $this->sku($row['sku']),
               'code' =>  $row['item_code'],
               'srp'  =>  $row['srp'],
               'description' => $row['description'] == NULL ? '' : $row['description'],
               'item_color' => $row['item_color'] == NULL ? NULL : $row['item_color'],
               'slug' => str_slug($row['name']),
               'price_a'  =>  $row['price_a'],
               'price_b'  =>  $row['price_b'],
               'price_c'  =>  $row['price_c'],
               'price_d'  =>  $row['price_d'],
            ]);
            $srpHistory = SrpHistory::create([
              'srp' => $row['srp'],
              'srp_dealer' => $row['price_a'],
              'srp_project' => $row['price_b'],
              'srp_walkin' => $row['price_c'],
              'srp_modern_trade' => $row['price_d'],
              'item_id' => $item->id,
              'price_a'  =>  $row['price_a'],
              'price_b'  =>  $row['price_b'],
              'price_c'  =>  $row['price_c'],
              'price_d'  =>  $row['price_d'],
            ]);

            if($row['item_color'] != NULL){
              $itemColor = ItemColor::create([
                'item_id' => $this->item($row['name']) != null ? $this->item($row['name'])->id : $item->id,
                'name' => $row['item_color'],
                'path' => '',
                'file_name' => '',
                'primary' => 1
              ]);
            }

          }
          if($this->checkBranch($row['name'], $row['branch'], $row['sku']) === 0){
            $branchHeader = BranchItemHeader::create([
                 'supplier_id'     => $this->supplier($row['supplier'])->id,
                 'branch_id'    => $this->branch($row['branch'])->id,
                 'is_order' => 0,
                 'inter_branch_id' => 0,
                'status_option_id' => $this->statusOption($row['status_option'])->id, 
                 'deleted_at' => NULL
            ]);
            $branchDetail  = BranchItemDetail::create([  
                'supplier_id' => $this->supplier($row['supplier'])->id,              
                'branch_item_header_id' => $branchHeader->id,
                'item_id' => $this->item($row['name']) != null ? $this->item($row['name'])->id : $item->id,
                'branch_id'    => $this->branch($row['branch'])->id,
                'amount'  => $row['unit_cost'],
                'quantity' => $row['quantity'],
            ]);
            // $grand_total
            $supplierHeader = SupplierOrderHeader::create([
                'admin_id' => $this->admin($row['admin'])->id,
                'supplier_id' => $this->supplier($row['supplier'])->id, 
                'supplier_type_id' => $this->supplierType($row['supplier_type'])->id,
                'status_option_id' => $this->statusOption($row['status_option'])->id, 
                'invoice_number' => $row['invoice_number'], 
                'invoice_date' => date("Y-m-d"), 
                'grand_total' => $row['quantity'] * $row['unit_cost'], 
                // 'grand_total' => $row['quantity'] * $row['unit_cost'], 
            ]);

            $supplierDetail = SupplierOrderDetail::create([
                'supplier_order_header_id' => $supplierHeader->id,
                'item_id' =>$this->item($row['name']) != null ? $this->item($row['name'])->id : $item->id,
                'branch_id' => $this->branch($row['branch'])->id,
                'quantity' => $row['quantity'],
                'amount' => $row['unit_cost'],
            ]);

          }
        }
    }

    public function batchSize(): int
    {
        return 10;
    }

    private function checkBranch($name, $branch, $sku){
      $branch = $this->branch($branch);
      $item = Item::whereName(strval($name))->first();
      $data = BranchItemDetail::where('item_id', $item == null ? 0 : $item->id)
                        ->where('branch_id', $branch->id)
                        ->get();
      return count($data);
    }

    public function isNumeric($data){
      return is_numeric($data) ? $data : 0;
    }

    public function itemCost($data, $amount){
      $item = Item::whereName(strval($data))->first();
      if($item != null) {
        $amount = BranchItemDetail::whereId($item->id)->first()->amount;
      }
      return $amount;
    }

    public function item($data){
      $data = Item::whereName(strval($data))->first();
      return $data;
    }

    public function sku($data){
      $sku = Item::where('sku', strval($data))->first();
      return $sku == NULL ? $data : '';
    }

    public function categoryHeader($data){
      $data = CategoryHeader::whereName(strval($data))->first();
      return $data;
    }

    public function admin($data){
      $data = Admin::whereEmail(strval($data))->where('admin_role_id', 1)->orWhere('admin_role_id', 2)->first();
      return $data;
    }

    public function statusOption($data){
      $data = StatusOption::whereName(strval($data))->first();
      return $data;
    }

    public function supplierType($data){
      $data = SupplierType::wherename($data)->first();
      return $data;
    }

    public function categoryDetail($data){
      $data = CategoryDetail::whereName(strval($data))->first();
      return $data;
    }

    public function package($data){
      $data = Package::whereName(strval($data))->first();
      return $data;
    }

    public function supplier($data){
      $data = Supplier::whereName(strval($data))->first();
      return $data;
    }

    public function brand($data){
      $data = Brand::whereName(strval($data))->first();
      return $data;
    }

    public function branch($data){
      $data = Branch::whereName(strval($data))->first();
      return $data;
    }

    public function invoiceNumber($data){
      $data = SupplierOrderHeader::where('invoice_number',strval($data))->get();
      return count($data);
    }

}