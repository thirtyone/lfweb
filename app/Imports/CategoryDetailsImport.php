<?php

namespace App\Imports;

use App\CategoryDetail;
use App\CategoryHeader;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CategoryDetailsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return CategoryDetails|null
     */
    protected $validator;
    public $collection;

    public function __construct(){
      $this->validator = new CustomValidationError();
    }

    public function collection(Collection $collection)
    {

      $this->validator->checkingData($collection);
      // $errorLines = array();
      // //checking the lines of the duplicate names
      // foreach ($collection as $k => $row){
      //   if(count($errorLines) < 10){
      //     if($this->categoryDetail($row['name']) != NULL){
      //       $errorLines[]= $k + 2;
      //     }
      //   } else {
      //     break;
      //   }
      // }
      // //error blocker
      // if(count($errorLines) > 0){
      //   $message = 'Duplicate Sub Category name in line ';
      //   return $this->validator->customValidation($errorLines, $message);
      // }
      //saving data from bulk upload
      foreach ($collection as $row){
        $categoryDetail = CategoryDetail::create([
          'category_header_id'     => $this->checkCategoryId($row['category_header']),
          'name'    => $row['name'],
          'slug' => str_slug($row['name']),
          'deleted_at' => NULL
        ]);
      }
    }

    // public function categoryDetail($data){
    //   $data = CategoryDetail::whereName($data)->first();
    //   return $data; 
    // }

    public function checkCategoryId($data){
        $data = CategoryHeader::whereName($data)->first();
        return $data->id;
    }

}