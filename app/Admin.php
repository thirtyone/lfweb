<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $guarded = ['id', 'created_at', 'updated_at'];

	protected $fillable =     ["id","branch_id","admin_role_id","name","address","contact_number","email","username","password","first_name","last_name","middle_name","max_discount","remember_token","deleted_at","created_at","updated_at"];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function adminRole() {
	    return $this->belongsTo('App\AdminRole');
	}

	public function hasManyOrderHeader()
	{
		return $this->morphMany('App\OrderHeader', 'customer');
	}


	public function branch() {
	    return $this->belongsTo('App\Branch');
	}
}
