<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderReceivable extends Model
{
    //


	//
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = ["branch_item_detail_id","order_header_id","item_id","quantity","amount","deleted_at","created_at","updated_at"];




	public function item() {
		return $this->belongsTo('App\Item');
	}
	public function branchItemDetail() {
		return $this->belongsTo('App\BranchItemDetail');
	}

}
