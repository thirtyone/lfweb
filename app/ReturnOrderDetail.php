<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnOrderDetail extends Model
{
    //

	protected $fillable = ["id","return_order_header_id","order_detail_id","reason","item_id","quantity","amount","total","created_at","updated_at"];

    public function Item(){
        return $this->belongsTo('App\Item');
    }


    public function returnOrderHeader() {
        return $this->belongsTo('App\ReturnOrderHeader');
    }
}
