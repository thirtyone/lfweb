<?php

namespace App\Http\Controllers\Admin\Api;

use App\Notification;
use App\OrderHeader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{

    /**
     * @var Notification
     */
    private $notification;
    /**
     * @var OrderHeader
     */
    private $orderHeader;

    public function __construct(Notification $notification, OrderHeader $orderHeader){

        $this->notification = $notification;
        $this->orderHeader = $orderHeader;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $data = $request->all();
        // return $data;
        $response = $this->notification->whereAnswerType($data['answer_type'])->orderBy('id','DESC')->whereAnswerId($data['answer_id'])->paginate(10);

        // return $response;
        foreach($response as $key => $res){
            $link = '/auth/order/';
            $json = json_decode($res->data);
            if(isset($json->order_number)){
                $order = $this->orderHeader->where('order_number', $json->order_number)->first();
                if($json->order_type_id == 1){
                    $link .= 'franchises/';
                    $link .= isset($order->id) ? $order->id.'/edit' : '';
                } else if($json->order_type_id == 2){
                    $link .= 'inter-offices/';
                    $link .= isset($order->id) ? $order->id.'/edit' : '';
                } else if($json->order_type_id == 3) {
                    $link .= 'clients/';
                    $link .= isset($order->id) ? $order->id.'/edit' : '';
                } else if($json->order_type_id == 4) {
                    $link .= 'dr-samples/';
                    $link .= isset($order->id) ? $order->id.'/edit' : '';
                } else {
                    $link .= 'quotations/';
                    $link .= isset($order->id) ? $order->id.'/edit' : '';
                }
                $res['link'] = $link;
            } else {
                $res['link'] = '/auth/inventory/items/show/' .$json->item_id.'?branch=main';
            }

            
        }

        $totalUnread = $this->notification->whereToggled(0)->whereAnswerType($data['answer_type'])->whereAnswerId($data['answer_id'])->count();
        $response = collect([
            'total_unread' => $totalUnread,
            'next_page' => $data['page'] + 1
        ])->merge($response);

        return  response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();
        if($data['id'] == 0){
            $this->notification = $this->notification->whereAnswerType($data['answer_type'])->whereAnswerId($data['answer_id'])->update(['toggled' => '1']);
        } else{


            $this->notification=  $this->notification->find($data['id']);
            $this->notification->status = 'read';
            $this->notification->save();    

        }

        return response()->json($this->notification, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
