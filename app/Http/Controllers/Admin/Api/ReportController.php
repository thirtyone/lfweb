<?php

namespace App\Http\Controllers\Admin\Api;


use App\Http\Controllers\BaseController;
use App\Item;
use App\ReturnOrderDetail;
use App\User;
use DateTime;
use App\Branch;
use App\Supplier;
use App\OrderDetail;
use App\OrderHeader;
use App\OrderReference;
use App\SupplierOrderDetail;
use App\SupplierOrderHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Cms\CmsRepository;
use Carbon;
use function foo\func;


class ReportController extends BaseController {

    /**
     * @var SupplierOrderDetail
     */
    private $supplierOrderDetail;
    /**
     * @var SupplierOrderHeader
     */
    private $supplierOrderHeader;
    /**
     * @var Item
     */
    private $item;
    /**
     * @var Branch
     */
    private $branch;
    /**
     * @var OrderHeader
     */
    private $orderHeader;

    private $branchId;
    /**
     * @var OrderReference
     */
    private $orderReference;

    private $orderDetail;

    private $supplier;

    private $client;
    /**
     * @var ReturnOrderDetail
     */
    private $returnOrderDetail;

    public function __construct(User $client, Supplier $supplier, OrderDetail $orderDetail, SupplierOrderHeader $supplierOrderHeader, SupplierOrderDetail $supplierOrderDetail, Item $item, Branch $branch, OrderHeader $orderHeader, OrderReference $orderReference, ReturnOrderDetail $returnOrderDetail) {

        $this->client = $client;
        $this->orderDetail = $orderDetail;
        $this->supplierOrderHeader = $supplierOrderHeader;
        $this->supplierOrderDetail = $supplierOrderDetail;
        $this->item = $item;
        $this->branch = $branch;
        $this->orderHeader = $orderHeader;
        $this->branchId = (Auth::guard('admin')->user()->admin_role_id == 1 || Auth::guard('admin')->user()->admin_role_id == 2 || Auth::guard('admin')->user()->admin_role_id == 3) ? 0 : Auth::guard('admin')->user()->branch_id;
        $this->orderReference = $orderReference;
        $this->supplier = $supplier;
        $this->returnOrderDetail = $returnOrderDetail;
    }

    public function priceSummary(Request $request) {
        $data = $request->all();


        $response = $this->client->with('admin');
        if (isset($data['srp_id']) && $data['srp_id']) {
            $response = $response->where('user_type_id', $data['srp_id']);
        }

        $response = $response->get();
        $count = $response->count();
        foreach ($response->toArray() as $key => $res) {

            $response[ $key ]['total_paid'] = $this->orderHeader->with(['user'])
                ->whereHas('user', function ($query) use ($res) {
                    $query->whereCustomerId($res['id']);
                })->whereCustomerType('users')->wherePaymentStatusOptionId(4)->where(function ($q) {
                    if ($this->branchId != 0) {
                        $q->whereBranchId($this->branchId);
                    }
                })->whereStatusOptionId(10);

            $response[ $key ]['client'] = $res['admin']['name'];

            $response[ $key ]['total_balance'] = $this->orderHeader->with(['user'])
                ->whereHas('user', function ($query) use ($res) {
                    $query->whereCustomerId($res['id']);
                })->whereCustomerType('users')->wherePaymentStatusOptionId(5)->where(function ($q) {
                    if ($this->branchId != 0) {
                        $q->whereBranchId($this->branchId);
                    }
                })->whereStatusOptionId(10);

            if (isset($data['date_from']) && isset($data['date_to'])) {

                $response[ $key ]['total_paid'] = $response[ $key ]['total_paid']->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->get();
                $response[ $key ]['total_balance'] = $response[ $key ]['total_balance']->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->get();
            }

            $response[ $key ]['total_paid'] = (int) $response[ $key ]['total_paid']->sum('grand_total');
            $response[ $key ]['total_balance'] = $response[ $key ]['total_balance']->sum('grand_total');
        }

        $inputs = [

        ];
        foreach ($response as $key => $res) {
            $arr = [
                "name"          => $res['name'],
                "credit_limit"  => $res['credit_limit'],
                "total_paid"    => $res['total_paid'],
                "total_balance" => $res['total_balance'],
                "id"            => $res['id'],
                "client"        => $res['client'],
                "payment_terms" => $res['payment_terms'],
            ];
            array_push($inputs, $arr);

        }

        usort($inputs, function ($a, $b) {
            return $a['total_paid'] - $b['total_paid'];
        });
        $inputs = array_reverse($inputs);

        $response = collect($inputs)->forPage(isset($data['page']) && $data['page'] ? $data['page'] : 1, 10);

        $response = $response->all();

        $response = [
            "data"     => $response,
            "total"    => $count,
            "per_page" => 10,
        ];

        return response()->json($response, 200);
    }

    public function price(Request $request) {
        $data = $request->all();

        $response = $this->orderHeader->with(['hasManyOrderDetail',

            'user',

        ])->whereHas('user', function ($q) use ($data) {
            $q->whereUserTypeId($data['srp_type']);
        });

        if (isset($data['client']) && $data['client']) {

            $response = $response->whereHas('user', function ($query) use ($data) {
                $query->where('name', $data['client']);
            });

        }
        $response = $response->where(function ($query) {
            if ($this->branchId != 0) {
                $query->where('order_headers.branch_id', $this->branchId);
            }
        });
        $response = $response->whereOrderTypeId([3])->paginate(10);

        return response()->json($response, 200);

    }


    public function suppliers(Request $request) {
        $data = $request->all();

        $response = DB::table('supplier_order_headers')
            ->join('supplier_order_details', 'supplier_order_headers.id', '=', 'supplier_order_details.supplier_order_header_id')
            ->join('suppliers', 'supplier_order_headers.supplier_id', '=', 'suppliers.id')
            ->where(function ($query) use ($data) {
                $query->orWhere('suppliers.supplier_type_id', $data['supplier_type']);
            })
            ->groupBy('supplier_order_headers.invoice_number');


        if (isset($data['date_from']) && isset($data['date_to'])) {
            $date_from = date('Y-m-d', strtotime($data['date_from']));
            $date_to = date('Y-m-d', strtotime($data['date_to']));
            $response = $response->whereBetween('supplier_order_headers.created_at', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
        }

        if (isset($data['supplier']) && $data['supplier']) {
            $response = $response->where('suppliers.slug', $data['supplier']);
        }

        $response = $response->paginate(10);

        return response()->json($response, 200);
    }

    public function items(Request $request) {

        $data = $request->all();

        $branchName = 'All';
        $response = $this->item->with([
            'categoryHeader',
            'brand',
            'supplier',
            'hasManyItemColor',
            'hasManyBranchItemDetail' => function ($query) use ($data) {

//              if (isset($data['branch']) && $data['branch']) {
//
//                  $query->whereHas('branch', function ($query) use ($data) {
//                      $query->whereSlug($data['branch']);
//                  });
//
//              }


                $query->with(['branch']);

            },
        ])
            ->withCount([
                'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) use ($data) {

                    if (isset($data['branch']) && $data['branch']) {

                        $query->whereHas('branch', function ($query) use ($data) {
                            $query->whereSlug($data['branch']);
                        });

                    }

                    $query->select(DB::raw("SUM(quantity)"));
                },
            ]);

        if (isset($data['keyword']) && $data['keyword']) {


            $response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
        }

        if (isset($data['category']) && $data['category']) {


            $response = $response->whereHas('categoryHeader', function ($query) use ($data) {
                $query->whereSlug($data['category']);
            });
        }

        if (isset($data['brand']) && $data['brand']) {


            $response = $response->whereHas('brand', function ($query) use ($data) {
                $query->whereSlug($data['brand']);
            });
        }

        if (isset($data['supplier']) && $data['supplier']) {


            $response = $response->whereHas('supplier', function ($query) use ($data) {
                $query->whereSlug($data['supplier']);
            });
        }

        if (isset($data['branch']) && $data['branch']) {

            $branch = $this->branch->whereSlug($data['branch'])->first();

            $branchName = $branch ? $branch->name : 'All';

            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->whereHas('branch', function ($query) use ($data) {
                    $query->whereSlug($data['branch']);
                });
            });

        }

        if (!isset($data['zero_quantity']) || !json_decode($data['zero_quantity'])) {


            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->where('quantity', '!=', 0);
            });

        }


        $response = $response->paginate(isset($data['per_page']) ? $data['per_page'] : 10);
        $response->getCollection()->transform(function ($response) use ($branchName) {
            $colors = '';
            $response->branch_name = $branchName;

            foreach ($response->hasManyItemColor as $color) {
                $colors .= ', ' . $color->name;
            }
            $colors = substr($colors, 1);
            $response->colors = $colors ? $colors : '';

            return $response;
        });

        return response()->json($response, 200);
    }


    public function balance(Request $request) {
        $data = $request->all();

        $response = $this->orderHeader->with(['hasManyOrderDetail' => function ($q) {
            $q->with(['item' => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            }]);
        }, 'user'])->whereStatusOptionId(10)->whereReturnOrderHeaderId(0);

        if (isset($data['srp']) && $data['srp']) {

            $response = $response->whereHas('user', function ($query) use ($data) {
                $query->where('user_type_id', $data['srp']);
            });

        }

        $response = $response->whereHas('user', function ($q) use ($data) {
            $q->where('payment_terms', '!=', '');
            if (isset($data['keyword']) && $data['keyword']) {

                $q->where('name', 'LIKE', '%' . $data['keyword'] . '%');

            }
        });
        // $response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');

        if (isset($data['date_from']) && isset($data['date_to'])) {
            // $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);
            $date_from = date('Y-m-d', strtotime($data['date_from']));
            $date_to = date('Y-m-d', strtotime($data['date_to']));
            $response = $response->whereBetween('created_at', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
        }
        $response = $response->where(function ($query) {
            if ($this->branchId != 0) {
                $query->where('order_headers.branch_id', $this->branchId);
            }
        });
        $res = $response->whereOrderTypeId([3])->has('user')->get()->toArray();

        foreach ($res as $key => $r) {
            $res[ $key ]['customer_name'] = $r['user']['name'];
        }
        usort($res, function ($a, $b) {
            return $a['customer_name'] <=> $b['customer_name'];
        });
        $response = collect($res)->forPage(isset($data['page']) && $data['page'] ? $data['page'] : 1, 10);
        $response = $response->all();

        $response = [
            "data"     => $response,
            "total"    => count($res),
            "per_page" => 10,
        ];

        return response()->json($response, 200);
    }


    public function orders(Request $request) {

        $data = $request->all();
        // return $data;
        $response = $this->orderDetail->with(['item',
            'orderHeader' => function ($q) {
                $q->with(['branch']);
            },
        ])->orderBy('quantity', 'DESC')
            ->whereHas('orderHeader', function ($q) use ($data) {
                $q->where(function ($q) use ($data) {
                    $q->whereOrderTypeId($data['order_type_id']);
                })->where(function ($q) use ($data) {
                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $q->whereBranchId($data['branch_id']);
                    }
                });
            });

        if (isset($data['keyword']) && $data['keyword']) {

            $response = $response->whereHas('item', function ($q) use ($data) {
                $q->where('name', 'LIKE', '%' . $data['keyword'] . '%');
            });

        }

        if (isset($data['date_from']) && isset($data['date_to'])) {
            // $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);
            $date_from = date('Y-m-d', strtotime($data['date_from']));
            $date_to = date('Y-m-d', strtotime($data['date_to']));
            $response = $response->whereBetween('created_at', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
        }
        $response = $response->paginate(10);

        return response()->json($response, 200);

    }


    public function totalSale(Request $request) {

        $data = $request->all();

        $response = $this->branch->with(['hasManyOrderHeader' => function ($query) use ($data) {
            $query->whereIn('order_type_id', [1, 3]);

        },

        ]);

        $response = $response->withCount([
            'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($q) {
                $q->select(DB::raw("SUM(grand_total)"))->whereIn('order_type_id', [1, 3])
                    ->whereStatusOptionId(10)
                    ->whereIn('payment_status_option_id', [4, 5]);
            },
        ])->withCount([
            'hasManyOrderHeader as has_many_order_header_count_total' => function ($q) {
                $q->select(DB::raw("SUM(total)"))->whereIn('order_type_id', [1, 3])
                    ->whereStatusOptionId(10)
                    ->whereIn('payment_status_option_id', [4, 5]);
            },
        ]);
        if (isset($data['date_from']) && isset($data['date_to'])) {
            $response = $response->withCount([
                'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($query) use ($data) {
                    $query->select(DB::raw("SUM(grand_total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);
                },
            ])->withCount([
                'hasManyOrderHeader as has_many_order_header_count_total' => function ($query) use ($data) {
                    $query->select(DB::raw("SUM(total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);
                },
            ]);
        }

        if ($this->branchId != 0) {
            $response = $response->whereId($this->branchId);
        }
        $response = $response->paginate(10);

        return response()->json($response, 200);

    }

    public function salesPerColor(Request $request) {
        $data = $request->all();

        $data['branch_id'] = $this->branchId;
        $result = $this->item
            ->with(['hasOneItemColor'])
            ->withCount([
            'hasManyOrderReference as quantity' => function ($query) use ($data) {
                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->where('order_type_id', '3');

                        if ($data['branch_id'] != 0) {
                            $query->where('branch_id', $data['branch_id']);
                        }
                        if (isset($data['date_from']) && isset($data['date_to'])) {

                            $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                        }

                    })
                    ->select(DB::raw("SUM(quantity)"));
            },
            'hasManyOrderReference as grand_total' => function ($query) use ($data) {
                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data){
                        $query->where('order_type_id', '3');

                        if($data['branch_id'] != 0){
                            $query->where('branch_id', $data['branch_id']);
                        }

                        if (isset($data['date_from']) && isset($data['date_to'])) {

                            $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                        }

                    })
                    ->select(DB::raw("SUM(total)"));
            }
        ])->having('quantity', '>', 0);
        if (isset($data['keyword']) && $data['keyword']) {
            $result = $result->where('name', 'LIKE', '%' . $data['keyword'] . '%');
        }

        $response = $result->get();

        foreach($response as $res){
            $res->color = isset($res->hasOneItemColor) && $res->hasOneItemColor ? $res->hasOneItemColor->name : '';
        }
        $res_count = count($response);



        $response = collect($response)->forPage(isset($data['page']) && $data['page'] ? $data['page'] : 1, 10);


        $response = $response->all();
        $response = [
            "data"     => $response,
            "total"    => $res_count,
            "per_page" => 10,
        ];

        return response()->json($response, 200);
    }

    public function salesPerCategory(Request $request) {
        $data = $request->all();

        $response = DB::table('items')
            ->join('category_headers', 'items.category_header_id', '=', 'category_headers.id')
            ->join('order_details', 'items.id', '=', 'order_details.item_id')
            ->join('order_headers', 'order_details.order_header_id', '=', 'order_headers.id')
            ->select('items.*',
                'category_headers.name as categoryName',
                'order_headers.invoice_number as invoiceNumber',
                'order_headers.created_at as invoiceDate',
                'order_headers.grand_total as invoiceAmount',
                'order_details.quantity as orderQuantity')
            ->where(function ($query) {
                $query->where('order_headers.order_type_id', '=', '3');
                // ->orWhere('order_headers.order_type_id', '=', '1');
            })
            ->where(function ($query) {
                if ($this->branchId != 0) {
                    $query->where('order_headers.branch_id', $this->branchId);
                }
            });

        $response = $response->paginate(10);

        return response()->json($response, 200);
    }


    public function perItemSupplier(Request $request) {
        $data = $request->all();

        $response = DB::table('items')
            ->join('brands', 'items.brand_id', 'brands.id')
            ->join('category_headers', 'items.category_header_id', 'category_headers.id')
            ->join('supplier_order_details', 'items.id', 'supplier_order_details.item_id')
            ->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', 'supplier_order_headers.id')
            ->join('suppliers', 'supplier_order_headers.supplier_id', 'suppliers.id')
            ->select('items.*',
                'brands.name as brand_name',
                'category_headers.name as category_name',
                'supplier_order_headers.invoice_number as supplier_invoice_number',
                'supplier_order_headers.invoice_date as supplier_invoice_date',
                'supplier_order_details.quantity',
                'suppliers.name as supplier_name',
                'supplier_order_details.amount as item_cost',
                DB::raw('supplier_order_details.quantity * supplier_order_details.amount as amount')
            );
        if (isset($data['keyword']) && $data['keyword']) {
            $response = $response->where('items.name', 'LIKE', '%' . $data['keyword'] . '%');


        }

        if (isset($data['date_to']) && isset($data['date_from'])) {


            $response = $response->whereBetween('supplier_order_headers.invoice_date', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

        }
        $response = $response->paginate(10);

        return response()->json($response, 200);
    }


    public function perItemColorSupplier(Request $request) {
        $data = $request->all();

        $response = DB::table('items')
            ->join('item_colors', 'items.id', '=', 'item_colors.item_id')
            ->join('supplier_order_details', 'items.id', '=', 'supplier_order_details.item_id')
            ->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', '=', 'supplier_order_headers.id')
            ->select('items.*',
                'item_colors.name as item_color',
                'supplier_order_details.quantity as orderQuantity',
                DB::raw('supplier_order_details.quantity * supplier_order_details.amount as amount'))
            ->groupBy('supplier_order_details.id');

        if (isset($data['keyword']) && $data['keyword']) {
            $response = $response->where('items.name', 'LIKE', '%' . $data['keyword'] . '%');
        }

        if (isset($data['date_from']) && isset($data['date_to'])) {

            $response = $response->whereBetween('supplier_order_headers.created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
        }

        $response = $response->paginate(10);

        return response()->json($response, 200);

    }

    public function supplierSummary(Request $request) {
        $data = $request->all();

        $response = DB::table('items')
            ->join('brands', 'items.brand_id', 'brands.id')
            ->join('category_headers', 'items.category_header_id', 'category_headers.id')
            ->join('supplier_order_details', 'items.id', 'supplier_order_details.item_id')
            ->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', 'supplier_order_headers.id')
            ->join('suppliers', 'supplier_order_headers.supplier_id', 'suppliers.id')
            ->select('items.*',
                'supplier_order_details.quantity',
                'suppliers.name as supplier_name',
                'supplier_order_details.amount as item_cost',
                DB::raw('SUM(supplier_order_details.quantity) as new_quantity'),
                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) / SUM(supplier_order_details.quantity) as avg_cost'),
                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) as amount'),
                DB::raw('MAX(supplier_order_headers.created_at) as last_invoice_date'),
                DB::raw('MIN(supplier_order_headers.created_at) as first_invoice_date')
            );

        if (isset($data['date_from']) && isset($data['date_to'])) {
            $response = $response->select(
                'items.*',
                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) / SUM(supplier_order_details.quantity) as avg_cost'),
                DB::raw('SUM(supplier_order_details.quantity) as new_quantity'),
                DB::raw('MAX(supplier_order_headers.created_at) as last_invoice_date'),
                DB::raw('MIN(supplier_order_headers.created_at) as first_invoice_date'),
                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) as amount')

            )->whereBetween('supplier_order_headers.created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
        }

        $response = $response->groupBy('items.name');

        if (isset($data['supplier']) && $data['supplier']) {
            $response = $response->where('suppliers.slug', $data['supplier']);
        }

        $response = $response->paginate(10);

        return response()->json($response, 200);

    }

    public function returns(Request $request) {
        $data = $request->all();
        $response = [];
        $result = $this->returnOrderDetail->with(['Item', 'returnOrderHeader' => function ($query) use ($data) {
            $query->with(['orderHeader' => function ($query) {
                $query->with(['user']);
            }]);
        }]);

        $result = $result->whereHas('returnOrderHeader', function ($query) use ($data) {
            $query->whereStatusOptionId(15)
                ->whereHas('orderHeader', function ($query) use ($data) {
                    if (isset($data['date_from']) && isset($data['date_to'])) {
                        $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                    }

                    if (isset($data['keyword']) && $data['keyword']) {
                        $query->where('invoice_number', 'LIKE', '%' . $data['keyword'] . '%');

                    }


                });


        });
        $result = $result->get();

        foreach ($result as $res) {
            $orderReference = $this->orderReference->whereItemId($res->item->id)->whereOrderHeaderId($res->returnOrderHeader->order_header_id)->first();

            $input = [
                "name"             => $res->item->name,
                "quantity"         => $res->quantity,
                "amount"           => $res->amount,
                "invoice_number"   => $res->returnOrderHeader->orderHeader->invoice_number,
                "invoice_date"     => $res->returnOrderHeader->orderHeader->invoice_date,
                "id"               => $res->returnOrderHeader->orderHeader->id,
                "client_name"      => $res->returnOrderHeader->orderHeader->user->name,
                "srp_type"         => $res->returnOrderHeader->orderHeader->user->user_type_id,
                "discount"         => $orderReference->discount,
                "reference_amount" => $orderReference->amount,
                "package_id"       => $res->item->package_id,
            ];

            $input["total_amount"] = 0;
            if ($input["discount"] > 0) {
                $input["total_amount"] = $input["reference_amount"] * $input["quantity"] - (($input["reference_amount"] * $input["quantity"]) / $input["discount"]);
            }
            if ($input["discount"] == 0) {
                $input["total_amount"] = $input["reference_amount"] * $input["quantity"];
            }

            array_push($response, $input);
        }

        usort($response, function ($a, $b) {
            return $b['id'] <=> $a['id'];
        });
        $res_count = count($response);

        $response = collect($response)->forPage(isset($data['page']) && $data['page'] ? $data['page'] : 1, 10);
        $response = $response->all();

        $response = [
            "data"     => $response,
            "total"    => $res_count,
            "per_page" => 10,
        ];

        return response()->json($response, 200);
    }

    public function lapsed(Request $request) {
        $data = $request->all();

        $record = [];


        $response = $this->orderHeader->with(['user'])
            ->whereHas('user', function ($query) use ($data) {
                $query->whereNotNull('payment_terms');
            })
            ->whereCustomerType('users')->whereStatusOptionId(10);

        if (isset($data['branch_id']) && $data['branch_id']) {
            $response = $response->whereBranchId($data['branch_id']);
        }

        if (isset($data['keyword']) && $data['keyword']) {

            $response = $response->whereHas('user', function ($query) use ($data) {

                $query->where('name', 'LIKE', '%' . $data['keyword'] . '%');

            });
        }


//
//      if (isset($data['date_to']) && isset($data['date_from'])) {
//
//          $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);
//
//      }

        $response = $response->get();

        foreach ($response as $key => $res) {
            $response[ $key ]['due_date'] = $res->created_at->addDays($res->user ? $res->user->payment_terms : 1)->format('Y-m-d');
            if (strtotime(date("d-m-Y")) >= strtotime($response[ $key ]['due_date'])) {


                $due = new DateTime($response[ $key ]['due_date']);
                $res['before_due'] = $due->diff(new DateTime())->format("%a");

                array_push($record, $res);
            }

        }
        $tempRecord = [];
        if (isset($data['date_to']) && isset($data['date_from'])) {


            foreach ($record as $k => $d) {

                if ((strtotime($data['date_from']) <= strtotime($d['due_date']))) {

                    if ((strtotime($data['date_to'])) >= strtotime($d['due_date'])) {

                        array_push($tempRecord, $d);
                    }

                }
            }
            $record = $tempRecord;
        }


        usort($record, function ($a, $b) {
            return $b['before_due'] <=> $a  ['before_due'];
        });


        $response = collect($record)->forPage(isset($data['page']) && $data['page'] ? $data['page'] : 1, 10);
        $response = $response->all();

        $response = [
            "data"     => $response,
            "total"    => count($record),
            "per_page" => 10,
        ];

        return response()->json($response, 200);


    }
}
