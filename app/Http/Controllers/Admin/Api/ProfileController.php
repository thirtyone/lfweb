<?php

namespace App\Http\Controllers\Admin\Api;

use App\Admin;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;

class ProfileController extends BaseController {

	private $cmsRepository;
	/**
	 * @var Admin
	 */
	private $admin;

	public function __construct(Admin $admin) {
		// set the model
        $this->cmsRepository = new CmsRepository($admin);

        $this->auth = Auth::guard('admin')->user();
		$this->admin = $admin;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) {
		//
		// $data = $request->all();

		// $response = $this->cmsRepository->getModel();

		// if (isset($data['keyword'])) {


		// 	$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		// }

		// $response = $response->paginate(10);

		// return response()->json($response, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
//        $user = auth()->guard('admin')->user();


        $this->validate($request,[
            'name' => 'required|string|max:191',
//            'username' => 'required|string|max:191',
            'password' => 'sometimes|required|min:6'
        ]);

        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }
		//
        $user = $request->all();

		$this->cmsRepository->update($user, $id);

		return response()->json($this->refreshAuth(), 200);
	}


	public function refreshAuth() {


		$data = [
			'intended' => URL::previous(),
			'message'  => "Login Success",
			'data'     => $this->admin->with('branch', 'adminRole')->find(Auth::guard('admin')->user()->id),
		];
		return $data;
	}


	public function refreshAuthorization(Request $request) {

		$data = $request->all();
		return response()->json($this->refreshAuth(), 200);
	}

	public function getNotifications() {


	}

}
