<?php

namespace App\Http\Controllers\Admin\Api;

use App\Branch;
use App\BranchType;
use App\Http\Controllers\BaseController;

use App\Http\Requests\Admin\BranchRequest;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BranchController extends BaseController {

	private $cmsRepository;

	public function __construct(Branch $branch) {
		// set the model
		$this->cmsRepository = new CmsRepository($branch);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel()->with(['branchType']);

		if (isset($data['keyword'])) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('name', 'asc')->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(BranchRequest $request) {
		$data = $request->all();
		$data['slug'] = $this->makeSlug($data['name'], 'App\Branch');
//		$data['label'] = strtolower($data['name']);


		$this->cmsRepository->create($data);

		$data['message'] = "Created Branch: " . $data['name'];

		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(BranchRequest $request, $id) {
		//
		$data = $request->all();
		$this->cmsRepository->update($data, $id);

		$data['message'] = "Updated Branch: " . $data['name'];

		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$data['message'] = "Deleted Branch: " . $data['name'];

		$this->logs($data);

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}
}
