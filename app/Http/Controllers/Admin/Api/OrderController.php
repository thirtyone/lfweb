<?php

namespace App\Http\Controllers\Admin\Api;


use App\Item;

use App\Notifications\Order\OrderThread;
use App\OrderDetail;

use App\Admin;
use App\Branch;
use App\User;

use App\OrderHeader;

use App\BranchItemDetail;
use App\BranchItemHeader;
use App\OrderReceivable;
use App\OrderReference;
use App\OrderPaymentMode;

use App\Repositories\Order\OrderRepository;
use App\ReturnOrderHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Notifications\FranchiseThread;
use App\Repositories\Cms\CmsRepository;
use App\Http\Controllers\BaseController;
use App\Notifications\TransactionThread;
use App\Notifications\UpdateOrderThread;
use App\Http\Requests\Admin\OrderRequest;

class OrderController extends BaseController {

	private $cmsRepository;
	private $cmsBranchItems;
	/**
	 * @var OrderHeader
	 */
	private $orderHeader;
	/**
	 * @var BranchItemHeader
	 */
	private $branchItemHeader;
	/**
	 * @var BranchItemDetail
	 */
	private $branchItemDetail;
	/**
	 * @var Admin
	 */
	private $admin;
	/**
	 * @var Item
	 */
	private $item;
	/**
	 * @var OrderDetail
	 */
	private $orderDetail;
	private $orderPaymentMode;
	private $branch;
	private $user;
	private $auth;
	/**
	 * @var OrderReference
	 */
	private $orderReference;
	/**
	 * @var ReturnOrderHeader
	 */
	private $returnOrderHeader;
	/**
	 * @var OrderReceivable
	 */
	private $orderReceivable;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	/**
	 * @var OrderThread
	 */


	/**
	 * OrderController constructor.
	 * @param OrderHeader $orderHeader
	 * @param BranchItemHeader $branchItemHeader
	 * @param BranchItemDetail $branchItemDetail
	 * @param Admin $admin
	 * @param Item $item
	 */
	public function __construct(OrderHeader $orderHeader, BranchItemHeader $branchItemHeader, BranchItemDetail $branchItemDetail, Admin $admin, Item $item, OrderDetail $orderDetail, OrderReference $orderReference, ReturnOrderHeader $returnOrderHeader, OrderReceivable $orderReceivable, OrderRepository $orderRepository, Branch $branch, User $user, OrderPaymentMode $orderPaymentMode) {
		// set the model
		$this->cmsRepository = new CmsRepository($orderHeader);

		$this->orderHeader = $orderHeader;
		$this->branchItemHeader = $branchItemHeader;
		$this->branchItemDetail = $branchItemDetail;
		$this->admin = $admin;
		$this->item = $item;
		$this->branch = $branch;
		$this->user = $user;
		$this->orderDetail = $orderDetail;
		$this->orderPaymentMode = $orderPaymentMode;

		$this->auth = Auth::guard('admin')->user();

		$this->orderReference = $orderReference;
		$this->returnOrderHeader = $returnOrderHeader;
		$this->orderReceivable = $orderReceivable;

		$this->orderRepository = $orderRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel()->with([
			'customer' => function ($query) use ($data) {

				if ($data['customer_type'] == "admins") {
					$query->with(['branch']);
				} else {
					$query->with(['userType']);
				}

			},
			'branch',
			'admin'    => function ($query) {
				$query->with(['branch']);
			},
			'statusOption',
			'paymentStatusOption',
			'orderPaymentType',
			'user',
		])->whereCustomerType($data['customer_type']);

		$response = $response->whereOrderTypeId($data['order_type_id']);

		if (isset($data['keyword'])) {


			$response = $response
				->where('invoice_number', 'LIKE', '%' . $data['keyword'] . '%');

			if($data['customer_type'] == 'admins'){
				$response = $response
					->orWhere('order_number', 'LIKE', '%' . $data['keyword'] . '%');
			}

		}

		if (isset($data['payment_status_option_id']) && $data['payment_status_option_id']) {
			$response = $response->wherePaymentStatusOptionId($data['payment_status_option_id']);
		}

		if (isset($data['status_option_id']) && $data['status_option_id']) {
			$response = $response->whereStatusOptionId($data['status_option_id']);
		}

		if ($data['customer_type'] == "admins") {
			$response = $response->where(function ($query) use ($data) {
				if (Auth::guard('admin')->user()->admin_role_id == 1) {
					if (isset($data['branch_id']) && $data['branch_id']) {
						$query->where('branch_id', $data['branch_id']);
					}

				} else {
					if (isset($data['branch_id']) && $data['branch_id']) {
						$query->where('branch_id', $data['branch_id']);
					}

					$query->orWhereHas('admin', function ($query) use ($data) {
						if (isset($data['customer_branch_id']) && $data['customer_branch_id']) {
							$query->where('branch_id', $data['customer_branch_id']);
						}

					});


				}


			});

		}

		if ($data['customer_type'] == "users") {

			if (isset($data['branch_id']) && $data['branch_id']) {
				$response = $response->where('branch_id', $data['branch_id']);
			}

		}
		if (isset($data['item_keyword'])) {

			$response = $response->whereHas('user', function ($q) use ($data){
				$q->where('name', 'LIKE', '%' . $data['item_keyword'] . '%');
			});
			

		}

		if (isset($data['date_to']) && isset($data['date_from'])) {

			$response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

		}


		$response = $response->orderBy('id', 'DESC')->paginate(10);

		$totalReceivables = $this->cmsRepository->getModel();

		if (isset($data['branch_id']) && $data['branch_id']) {
			$totalReceivables = $totalReceivables->whereBranchId($data['branch_id']);
		}
		// $totalReceivables = $totalReceivables->wherePaymentStatusOptionId(5)->whereOrderTypeId($data['order_type_id'])->sum('grand_total');
		$totalReceivables = $totalReceivables->wherePaymentStatusOptionId(5)->where('status_option_id', '!=', 3)->whereOrderTypeId($data['order_type_id'])->sum('grand_total');

		$response = collect(['total_receivables' => $totalReceivables])->merge($response);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	// public function store(OrderRequest $request) {
	public function store(Request $request) {
		$data = $request->all();

		if (isset($data['prefix'])) {
			$data['order_number'] = $this->generateOrderNumber($data['prefix'], $data['order_type_id']);

		}

		$response = $this->cmsRepository->create($data);
		$response->hasManyOrderDetail()->createMany($data['has_many_order_detail']);
		$response->hasManyOrderReference()->createMany($data['has_many_order_reference']);

        if($data['order_type_id'] == 4) {
            foreach($data['has_many_order_detail'] as $od) {
                $branchItemDetail = $this->branchItemDetail->find($od['branch_item_detail_id']);
                $branchItemDetail->quantity = $branchItemDetail->quantity - $od['quantity'];
                $branchItemDetail->save();
            }

        }

		// $response->message = $this->getOrderType($data['order_type_id']) . $data['order_number'];

		// if($response['return_order_header_id'] != 0){
  //           $response->message = $response->message. ' - RETURN ORDER';
  //       }
        //old notifications and history
		// $this->auth->notify(new OrderThread($response));
		// $this->logs($response);

        $response->slug = $response->id;
        $response->notification_id = 2;
        //new notification and history function
		$this->notificationsHistories($data, $response);

		return response()->json($response, 200);

//		return $data;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->with(['customer', 'hasManyOrderReceivable','hasManyOrderPaymentNote',
			'hasManyOrderDetail'          => function ($query) {
				$query->with(['item', 'branchItemDetail']);
			},
			'hasManyOrderReference'       => function ($query) use ($id) {
				$query->with(['item' => function ($query) {
					$query->with(['hasOneItemColor']);
				}, 'hasManyOrderDetail'])
					->withCount(['hasManyOrderDetail as has_many_order_detail_count_quantity' => function ($query) use ($id) {

						$query->select(DB::raw("SUM(quantity)"))->whereOrderHeaderId($id);
					},
					]);
			}, 'hasManyReturnOrderHeader' => function ($query) {

				$query->with(['hasManyReturnOrderDetail', 'statusOption'])
					->withCount(['hasManyReturnOrderDetail as has_many_return_order_detail_count_quantity' => function ($query) {

						$query->select(DB::raw("SUM(quantity)"));
					},
					]);
			}])->find($id);

		return response()->json($response, 200);
	}


	public function showReturnItemsTicket($id) {


		$response = $this->returnOrderHeader->whereStatusOptionId(1)->whereId($id)->first();
		if ($response) {
			$response->status = true;

			return response()->json($response, 200);
		} else {
			return response()->json(["status" => false], 404);
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(OrderRequest $request, $id) {
		$data = $request->all();

		$response = $this->cmsRepository->show($id);
		$response->fill($data)->save();
		$response->deleteChildData();

		$response->hasManyOrderDetail()->createMany($data['has_many_order_detail']);
		$response->hasManyOrderReference()->createMany($data['has_many_order_reference']);
		$response->hasManyOrderPaymentNote()->createMany($data['has_many_order_payment_note']);

		//old notification no update for histories
		// $response->message = "Order Updated with Order Number: " . $data['order_number'];
		// $this->auth->notify(new OrderThread($response));

        $response->slug = $response->id;
        $response->notification_id = 2;

		//new notification and history function
		$this->notificationsHistories($data, $response);
		
		return response()->json($data, 200);


	}

	public function notificationsHistories($data, $response){

		//Get notification and history details for returned items
		if(isset($data['return_stock']) || isset($data['return_order_detail']) && $data['return_order_detail'] != NULL){
			$returnedItems = isset($data['return_stock']) ? $data['return_stock'] : '';
			$findOrder = $this->cmsRepository->show($response['order_header_id']);
			if(isset($data['return_order_detail'])){
				$returnedItems = array_sum(array_column($data['return_order_detail'],'quantity'));
			}
			$data = $findOrder;
			$response = $findOrder;
	        $response->slug = $response->id;
	        $response->notification_id = 2;
		}

		//Get notification and history details
		$id = isset($response->id) && $response->id ? $response->id : $data['id'];

		if($data['order_type_id'] != 3 && $data['order_type_id'] != 4 && $data['order_type_id'] != 5){
			$customerBranchId = $this->admin->find($data['customer_id'])->branch_id;
		} else {
			$adminId = $this->user->find($data['customer_id'])->admin_id;
			$customerBranchId = $this->admin->find($adminId)->branch_id;
		}

		$paymentMethod = isset($data['order_payment_mode_id']) ? $this->orderPaymentMode->find($data['order_payment_mode_id'])->name : null;
		$preOrderSavePayment = isset($data['pre_order_mark_as_paid']) ? $data['pre_order_mark_as_paid'] : null;
		$action = $this->orderHeader->find($id)->status_option_id;
		$paymentStatus = $this->orderHeader->find($id)->payment_status_option_id;

		if($action == 10 && $data['order_type_id'] != 3 && $data['order_type_id'] != 4) {
			$totalQuantity = $this->orderReceivable->whereOrderHeaderId($id)->get()->sum('quantity');
		} else {
			$totalQuantity = $this->orderReference->whereOrderHeaderId($id)->get()->sum('quantity');
			// if($data['order_type_id'] == 5){
			// 	$totalQuantity = $this->orderDetail->whereOrderHeaderId($id)->get()->sum('quantity');
			// }
		}

		if(isset($data['store_pre_order_item']) == true){
			$returnedItems = $data['quantity'];
		}

		//details needs for hitory and notification message
		$details = [
			'payment_status' => isset($data['payment_status']) ? $data['payment_status'] : NULL,
			'status_option' => $action,
			'total_quantity' => $totalQuantity,
			'customer_branch_id' => $customerBranchId,
			'branch_id' => $data['branch_id'],
			'order_number' => $data['order_number'],
			'order_type_id' => $data['order_type_id'],
			'customer_id' => $data['customer_id'],
			'payment_method' => $paymentMethod,
			'invoice_number' => isset($data['invoice_number']) ? $data['invoice_number'] : NULL,
			'pre_order_save_payment' => $preOrderSavePayment,
			'returned_items_quantity' => isset($returnedItems) ? $returnedItems : NULL,
			'store_pre_order_items' => isset($data['store_pre_order_item']) ? true : NULL
		];

		//Get notification and history message
		$notifications = $this->getNotification($details);

		// $notifications = $this->getNotification(isset($data['payment_status']) ? $data['payment_status'] : NULL, $action, $totalQuantity, $customerBranchId, $data['branch_id'], $data['order_number'], $data['order_type_id'], $data['customer_id'], $paymentMethod, isset($data['invoice_number']) ? $data['invoice_number'] : NULL, $preOrderSavePayment, isset($returnedItems) ? $returnedItems : NULL, isset($data['store_pre_order_item']) ? true : NULL);

		//notifications and histories for branch
		$response->message = isset($notifications['branch']) ? $notifications['branch'] : $notifications['customer'];
		$response->branch_id = $data['branch_id'];
		$this->auth->notify(new OrderThread($response));
		if(isset($notifications['branch'])){
			$this->logs($response);
		}

		//notifications and histories for customer
		$response->message = $notifications['customer'];
		$response->branch_id = $customerBranchId;
		$this->auth->notify(new OrderThread($response));
		$this->logs($response);

	}

    public function updateDrSampleStatus($id,Request $request) {
        $data = $request->all();

        $response = $this->cmsRepository->show($id);
        $response->fill($data)->save();


        //Old notifications and history
        // $response->message = "DR Sampled Updated with Order Number: " . $data['order_number'];

        // $this->auth->notify(new OrderThread($response));

        return $data;
    }

	public function returnItemsTicket(Request $request) {
		$data = $request->all();

		foreach ($data['return_order_detail'] as $key => $value) {
			if(isset($value['quantity'])){
				$response = $this->branchItemDetail->whereBranchId($data['branch_item_detail_id'])->whereItemId($value['item_id'])->first();
				$response->quantity = $response['quantity'] + $value['quantity'];
				$response->save();
			}	
		}
		// $response = $this->branchItemDetail->whereBranchId($data['branch_item_detail_id'])->whereItemId($data['return_order_detail'][0]['item_id'])->first();
		// $response->quantity = $response['quantity'] + $data['return_order_detail'][0]['quantity'];

		// $response->save();
		$data['return_order_detail'] = array_filter($data['return_order_detail'], function ($value) {
			return (((isset($value['order_detail_id']) && $value['order_detail_id'] != 0) && (isset($value['quantity']) && $value['quantity'] != 0)));
			});
		
		if (count($data['return_order_detail']) != 0) {
			$data['grand_total'] = 0;


			foreach ($data['return_order_detail'] as $key => $value) {
				$orderReference = $this->orderReference->whereOrderHeaderId($data['order_header_id'])->whereItemId($value['item_id'])->first();
				if ($orderReference) {
					if($orderReference->discount != 0){
						$changeVal = $orderReference->discount/100;
						$discount = ($orderReference->amount * $value['quantity']) * $changeVal;
					} else {
						$discount = 0;
					}
					
					$data['return_order_detail'][ $key ]['amount'] = $orderReference->amount;
					$data['return_order_detail'][ $key ]['total'] = ($orderReference->amount * $value['quantity']) - $discount;
					$data['grand_total'] = + $data['return_order_detail'][ $key ]['total'] + $data['grand_total'];
				}
			}

			$response = $this->returnOrderHeader->create($data);

			$response->hasManyReturnOrderDetail()->createMany($data['return_order_detail']);

			$response->slug = $data['order_header_id'];
        	$response->notification_id = 2;
        	
			$this->notificationsHistories($data, $response);

		}

		return $data;

	}


	public function updateReturnItemsTicket(Request $request) {

		$data = $request->all();
		$response = $this->returnOrderHeader->with(['hasManyReturnOrderDetail'])->whereId($data['id'])->whereStatusOptionId(1)->first();

		if ($response) {

			$response->status_option_id = 15;
			$response->save();

			foreach ($response->hasManyReturnOrderDetail as $key => $val) {
				$orderDetail = $this->orderDetail->find($val->order_detail_id);
				$orderDetail->return_quantity = $orderDetail->return_quantity + $val->quantity;
				$orderDetail->save();

				$branchItemDetail = $this->branchItemDetail->find($orderDetail->branch_item_detail_id);
				$branchItemDetail->quantity = $branchItemDetail->quantity + $val->quantity;

			}
		}

	}

	public function storePreOrderItem(Request $request) {
		$data = $request->all();

		$data['store_pre_order_item'] = true;
		$response = $this->cmsRepository->show($data['order_header_id']);
		$data['customer_id'] = $response->customer_id;
		$data['order_type_id'] = $response->order_type_id;
		$data['order_number'] = $response->order_number;

		$response->slug = $data['order_header_id'];
		$response->notification_id = 2;

		//new notification and history function
		$this->notificationsHistories($data, $response);

		$branchItemDetail = $this->branchItemDetail->find($data['branch_item_detail_id']);
		$branchItemDetail->quantity = $branchItemDetail->quantity - $data['quantity'];
		$branchItemDetail->save();

		$response = $this->orderDetail->create($data);

//		return $branchItemDetail;
		return $response;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

//		$this->cmsRepository->delete($id);
//
//		return response()->json(true, 200);


	}


	public function deleteReturnItemsTicket($id) {

		$response = $this->returnOrderHeader->find($id);
		$response->delete();
	}


	public function franchiseValidateQuantities(Request $request) {


		$data = $request->all();
		if ($data['status_option_id'] == 2 || $data['status_option_id'] == 10) {


			foreach ($data['has_many_order_reference'] as $key => $d) {
//


				$response = $this->item
					->withCount([
						'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) use ($data) {
							$query->whereBranchId($data['branch_id'])
								->select(DB::raw("SUM(quantity)"));
						},
					])->find($d['item_id']);

				if ($response->has_many_branch_item_detail_count_quantity < $d['quantity']) {
					return response()->json([
						'message' => "invalid_quantity",
						"errors"  => [["Not Enough Quantity"]],
					], 422);
				}

			}
		}

//
		return response()->json(
			true, 200);

	}

	public function nonFranchiseValidateQuantities(Request $request) {

		$data = $request->all();


		$result = [];
		if ($data['status_option_id'] == 2 || $data['status_option_id'] == 10 || $data['status_option_id'] == 8) {

			foreach ($data['has_many_order_detail'] as $row) {
				if (!isset($result[ $row['item_id'] ])) {
					$result[ $row['item_id'] ] = $row;
				} else {
					$result[ $row['item_id'] ]['quantity'] += $row['quantity'];
				}
			}


			foreach ($data['has_many_order_reference'] as $key => $value) {

				foreach ($result as $res) {
					if ($value['item_id'] == $res['item_id']) {
						if ($res['quantity'] < $value['quantity']) {
							return response()->json([
								'message' => "invalid_quantity",
								"errors"  => [["Not Enough Quantity: " . $value['item']['name']]],
							], 422);
						}
					}
				}
			}

//			foreach ($data['has_many_order_detail'] as $key => $d) {
//
//				$response = $this->branchItemDetail->find($d['branch_item_detail_id']);
//				if ($response->quantity < $d['quantity']) {
//					return response()->json([
//						'message' => "invalid_quantity",
//						"errors"  => [["Not Enough Quantity"]],
//					], 422);
//				}
//			}


		}

		return response()->json(true, 200);

	}


	public function nonFranchiseOrders(Request $request) {
		$data = $request->all();

//		$customer = $this->admin->find($data['customer_id']);

		if (count($data['has_many_order_detail']) != 0) {


			if ($data['order_type_id'] == 2 || $data['order_type_id'] == 1) {
				$inputs = [
					"branch_id"        => $data['customer']['branch_id'],
					"is_order"         => 1,
					"inter_branch_id"  => $data['branch_id'],
					"status_option_id" => 2,
				];

				$response = $this->branchItemHeader->create($inputs);
			}


			foreach ($data['has_many_order_detail'] as $key => $d) {

				if (!$this->orderRepository->removeBranchStockQuantity($d['quantity'], $d['branch_item_detail_id'])) {
					return response()->json([
						'message' => "invalid_quantity",
						"errors"  => [["Not Enough Quantity"]],
					], 422);
				}

				if ($data['order_type_id'] == 2 || $data['order_type_id'] == 1) {

					if (!$this->orderRepository->validateIfItemStockExist($d, $data['customer']['branch_id'])) {

						$inputs = [
							"item_id"   => $d['item_id'],
							'quantity'  => $d['quantity'],
							"branch_id" => $data['customer']['branch_id'],
							"amount"    => $d['amount'],
						];
						$response->hasManyBranchItemDetail()->create($inputs);
					}

				}


			}


			// $this->auth->notify(new TransactionThread($response));
		}

		// $notif->message = "Client Order Created with Order Number: " . $data['order_number'];

		//  var_dump($notif->message);

		// $this->auth->notify(new OrderThread($response));


		return $data;
	}

	public function franchiseOrders(Request $request) {
		$data = $request->all();


		if (count($data['has_many_order_reference']) != 0) {

			foreach ($data['has_many_order_reference'] as $key => $d) {
				$branchItems = $this->branchItemDetail->whereBranchId($data['branch_id'])->where('quantity', '!=', 0)->whereItemId($d['item_id'])->get();
				$quantity = $d['quantity'];
				foreach ($branchItems as $k => $b) {
					if ($quantity != 0) {


						if ($quantity > $b->quantity) {
							$quantity = $quantity - $b->quantity;
							$currentRowQuantity = 0;
						} elseif ($quantity <= $b->quantity) {
							$quantity = $b->quantity - $quantity;

							$currentRowQuantity = $quantity;
						}

						$b->quantity = $currentRowQuantity;

						$b->save();


					}
				}
			}


		}


		return $data;


	}


	public function returnStock(Request $request) {
		$data = $request->all();

		$response = [];
		$response['branch_item_detail'] = $this->branchItemDetail->find($data['branch_item_detail_id']);
		$response['branch_item_detail']->quantity = $response['branch_item_detail']->quantity + (int) $data['return_stock'];
		$response['branch_item_detail']->save();

		$response['order_detail'] = $this->orderDetail->find($data['id']);
		$response['order_detail']->return_quantity = $response['order_detail']->return_quantity + (int) $data['return_stock'];
		$response['order_detail']->save();

		$this->notificationsHistories($data, $data);
		return response()->json(true, 200);

	}


	public function orderToTransit(Request $request) {
		$data = $request->all();		

		foreach ($data['has_many_order_detail'] as $key => $d) {
			$this->orderRepository->removeBranchStockQuantity($d['quantity'], $d['branch_item_detail_id']);
			$this->orderReceivable->create($d);

		}

		//for notifications and histories

		return response()->json(true, 200);


	}


	public function orderToReceive(Request $request) {
		$data = $request->all();
		$inputs = [
			"branch_id"        => $data['customer']['branch_id'],
			"is_order"         => 1,
			"inter_branch_id"  => $data['branch_id'],
			"status_option_id" => 2,
		];
		$response = $this->branchItemHeader->create($inputs);
		foreach ($data['has_many_order_receivable'] as $key => $d) {

			if (!$this->orderRepository->validateIfItemStockExist($d, $data['customer']['branch_id'])) {
				$supplier = Item::whereId($d['item_id'])->first();
				$inputs = [
					"item_id"   => $d['item_id'],
					'quantity'  => $d['quantity'],
					"supplier_id" => $supplier->supplier_id,
					"branch_id" => $data['customer']['branch_id'],
					"amount"    => $d['amount'],
				];
				$response->hasManyBranchItemDetail()->create($inputs);
			}
		}

		foreach ($data['has_many_order_reference'] as $key => $d) {
			$quantity = $d['quantity'] - $d['receive'];

			$this->orderRepository->addToLatestStock($d['item_id'], $data['branch_id'], $quantity);
			$this->orderRepository->removeToLatestStock($d['item_id'], $data['customer']['branch_id'], $d['receive'] <= 0 ? abs($quantity) : 0);

		}

	}


}
