<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Requests\Admin\UserRequest;
use App\OrderHeader;
use App\User;
use App\Http\Controllers\BaseController;

use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends BaseController {

	private $cmsRepository;
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var OrderHeader
	 */
	private $orderHeader;

	public function __construct(User $user, OrderHeader $orderHeader) {
		// set the model
		$this->cmsRepository = new CmsRepository($user);
		$this->user = $user;
		$this->orderHeader = $orderHeader;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel()->with(['admin', 'hasManyOrderHeader' => function ($query) {
			$query->where('payment_status_option_id', '!=', 4);
		}])
			->withCount([
				'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($query) use ($data) {

					$query->select(DB::raw("SUM(grand_total)"))->where('payment_status_option_id', '!=', 4);
				},
			])->whereHas('admin', function ($query) {
				if(Auth::guard('admin')->user()->branch_id != 1){
					$query->whereBranchId(Auth::guard('admin')->user()->branch_id);
				}
				
					
				
				
			});
		if (isset($data['keyword'])) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
		}

		$response = $response->withTrashed()->orderBy('name', 'asc')->paginate(10);


		foreach ($response as $key => $res) {
			$res->credit_status = true;
			if ($res->has_many_order_header_count_grand_total > $res->credit_limit) {
				$res->credit_status = false;
			}

			foreach ($res->hasManyOrderHeader as $order) {
				$order->payment_due_date = explode(' ', $order->created_at->addDays($res->payment_terms))[0];

				if (date('Y-m-d') > $order->payment_due_date) {
					$res->credit_status = false;
				}
			}

		}

		return response()->json($response, 200);
	}


	public function orders($id) {
		$response = $this->orderHeader
			->with(['paymentStatusOption'])
			->whereCustomerId($id)->whereCustomerType('users')->where('payment_status_option_id', '!=', 4)->paginate(10);


		$totalReceivables = $this->orderHeader->wherePaymentStatusOptionId(5)->whereCustomerType('users')->whereCustomerId($id)->sum('grand_total');


		$response = collect(['total_receivables' => $totalReceivables])->merge($response);


		return response()->json($response, 200);


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(UserRequest $request) {
		$data = $request->all();
		// return $data;
//		$data['slug'] = $this->makeSlug($data['name'], 'App\User');
//		$data['label'] = strtolower($data['name']);
		$this->cmsRepository->create($data);

		$data['message'] = "Created Client " . $data['name'];

		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UserRequest $request, $id) {
		//
		$data = $request->all();
		$this->cmsRepository->update($data, $id);

		$data['message'] = "Updated Client " . $data['name'];

		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {


		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}

	public function activate($id){

		$response = User::withTrashed()->find($id);
		$data['message'] = "Activate User " . $response['name'];
		$response->restore();
		$this->logs($data);

		return response()->json($data, 200);
	}
}
