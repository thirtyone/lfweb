<?php

namespace App\Http\Controllers\Admin\Api;

use App\Admin;
use App\Http\Requests\Admin\AdminRequest;
use App\Http\Controllers\BaseController;
use App\OrderHeader;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentDashboardController extends BaseController {

	private $cmsRepository;
	/**
	 * @var OrderHeader
	 */
	private $orderHeader;

	public function __construct(Admin $admin, OrderHeader $orderHeader) {
		// set the model
		$this->cmsRepository = new CmsRepository($admin);
		$this->orderHeader = $orderHeader;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) {
		//
		$data = $request->all();
		
		$response = $this->cmsRepository->getModel()->with(['adminRole','branch','hasManyOrderHeader'])
			->withCount([
				'hasManyOrderHeader as has_many_order_header_grand_total' => function ($query) use ($data) {


					$query->select(DB::raw("SUM(grand_total)"))->wherePaymentStatusOptionId(4);
				},
			])

			->whereIn('admin_role_id',[5,6]);

		if (isset($data['keyword'])) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('has_many_order_header_grand_total', 'desc')->paginate(10);

		return response()->json($response, 200);
	}
	public function indexOrder(Request $request) {
		//
		$data = $request->all();

		$response =$this->orderHeader;

		$response = $response->orderBy('grand_total', 'desc')->whereCustomerType('users')->wherePaymentStatusOptionId(4)->whereCustomerId($data['id']);

        if (isset($data['date_to']) && isset($data['date_from'])) {

            $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

        }

        $response = $response->paginate(10);


		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(AdminRequest $request) {

		$data = $request->all();
		unset($data['password_confirmation']);
		$data['username'] =  $data['email'];
		$data['password'] = bcrypt($data['password']);

		$this->cmsRepository->create($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(AdminRequest $request, $id) {
		//
		$data = $request->all();
		$this->cmsRepository->update($data, $id);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}
}
