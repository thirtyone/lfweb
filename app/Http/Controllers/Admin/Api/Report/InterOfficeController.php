<?php

namespace App\Http\Controllers\Admin\Api\Report;

use App\Branch;
use App\Exports\Reports\InterOffice\InExport;
use App\Exports\Reports\InterOffice\InSummaryExport;
use App\Exports\Reports\InterOffice\OutExport;
use App\Exports\Reports\InterOffice\OutSummaryExport;
use App\OrderDetail;
use App\OrderHeader;
use App\OrderReference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class InterOfficeController extends Controller {
    //
    /**
     * @var OrderReference
     */
    private $orderReference;
    /**
     * @var OrderDetail
     */
    private $orderDetail;
    /**
     * @var OrderHeader
     */
    private $orderHeader;
    private $branch;


    /**
     * InterOfficeController constructor.
     * @param OrderReference $orderReference
     * @param OrderDetail $orderDetail
     * @param OrderHeader $orderHeader
     */
    public function __construct(OrderReference $orderReference,
                                OrderDetail $orderDetail,
                                OrderHeader $orderHeader,
                                Branch $branch) {

        $this->orderReference = $orderReference;
        $this->orderDetail = $orderDetail;
        $this->orderHeader = $orderHeader;
        $this->branch = $branch;
    }

    public function withBalance($data){

        $response = true;
        if (isset($data['branch_id']) && $data['branch_id'] && isset($data['customer_id']) && $data['customer_id']) {
            $from = $this->branch->find($data['branch_id']);
            $to = $this->branch->find($data['customer_id']);

            if($from->branch_type_id == 1 && $to->branch_type_id == 1){
                $response = false;
            }
        }

        return $response;
    }

    public function in(Request $request) {

        $data = $request->all();


        $response = $this->orderReference->with([
            'item'              => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            },
            'hasOneOrderDetail' => function ($q) {
                $q->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
            },
            'orderHeader',
        //old doubled the query of hasOneOrderDetail
        // ])->has('hasOneOrderDetail')

        //new
        ])->whereHas('orderHeader', function ($query) use ($data) {
        $query->with(['admin'])
            ->whereIn('order_type_id', [1, 2])
            ->whereStatusOptionId(10)
            ->whereIn('payment_status_option_id', [4, 5]);


            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereHas('admin', function ($query) use ($data) {
                    $query->whereBranchId($data['customer_id']);
                });
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->where('invoice_number', 'LIKE', '%' . $data['invoice_number'] . '%');
            }

            if (isset($data['date_from']) && isset($data['date_to'])) {
                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
            }


        });

        if (isset($data['item_name']) && $data['item_name']) {
            $response = $response->whereHas('item', function ($query) use ($data) {
                if (isset($data['item_name']) && $data['item_name']) {
                    $query->where('name', 'LIKE', '%' . $data['item_name'] . '%');
                }
            });
        }

        $response = $response->orderBy('created_at', 'DESC')->paginate(10);


        $balance = $this->withBalance($data);

        $response->getCollection()->transform(function ($data) use ($balance) {
            $array = $data->toArray();
            $array['cost'] = $array['has_one_order_detail']['branch_item_detail']['amount'];
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['payment_status_option_id'] = $array['order_header']['payment_status_option_id'];
            $array['order_type_id'] = $array['order_header']['order_type_id'];
            $array['item_name'] = $array['item']['name'];
            $array['balance'] = $balance ?  $array['order_header']['payment_status_option_id'] == 5 ? $array['order_header']['total'] : 0 : 0;

            return $array;
        });

        return response()->json($response, 200);
    }


    public function inExport(Request $request) {

        $data = $request->all();


        $response = $this->orderReference->with([
            'item'              => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            },
            'hasOneOrderDetail' => function ($q) {
                $q->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
            },
            'orderHeader',

        ])->has('hasOneOrderDetail')
            ->whereHas('orderHeader', function ($query) use ($data) {
            $query->with(['admin'])
                ->whereIn('order_type_id', [1, 2])
                ->whereStatusOptionId(10)
                ->whereIn('payment_status_option_id', [4, 5]);

            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereHas('admin', function ($query) use ($data) {
                    $query->whereBranchId($data['customer_id']);
                });
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->where('invoice_number', 'LIKE', '%' . $data['invoice_number'] . '%');
            }

            if (isset($data['date_from']) && isset($data['date_to'])) {
                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
            }


        });

        if (isset($data['item_name']) && $data['item_name']) {
            $response = $response->whereHas('item', function ($query) use ($data) {
                if (isset($data['item_name']) && $data['item_name']) {
                    $query->where('name', 'LIKE', '%' . $data['item_name'] . '%');
                }
            });
        }

        $response = $response->orderBy('created_at', 'DESC')->get()->toArray();

        $balance = $this->withBalance($data);
        $response = collect($response)->transform(function ($data) use ($balance) {
            $array = $data;
            $array['cost'] = $array['has_one_order_detail']['branch_item_detail']['amount'];
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['payment_status_option_id'] = $array['order_header']['payment_status_option_id'];
            $array['order_type_id'] = $array['order_header']['order_type_id'];
            $array['item_name'] = $array['item']['name'];
            $array['balance'] = $balance ?  $array['order_header']['payment_status_option_id'] == 5 ? $array['order_header']['total'] : 0 : 0;

            return $array;
        });

        return Excel::download(new InExport($response->toArray()), $data['file_name'] . '.csv');

    }


    public function inSummary(Request $request) {
        $data = $request->all();
        $response = $this->branch
            ->with([
            ])
            ->withCount([
                'hasManyOrderHeader as total_srp'     => function ($query) use ($data) {
                    $query->with(['admin'])
                        // ->whereIn('order_type_id', [1, 2])
                        ->whereIn('order_type_id', [1])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);

                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }

                    $query->select(DB::raw("SUM(grand_total)"));
                },
                'hasManyOrderHeader as total_balance' => function ($query) use ($data) {
                    $query->with(['admin'])
                        // ->whereIn('order_type_id', [1, 2])
                        ->whereIn('order_type_id', [1])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [5]);

                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }
                    $query->select(DB::raw("SUM(grand_total)"));
                },
            ])
            ->get();
        foreach ($response as $key => $res) {
            $cost = 0;
            $orderDetail = $this->orderReference
                ->with([
                    'orderHeader',
                    'hasOneOrderDetail' => function ($query) {
                        $query->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
                    },
                ])
                ->whereHas('orderHeader', function ($query) use ($data, $res) {

                    $query->with(['admin'])
                        ->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5])
                        ->whereBranchId($res['id']);

                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }


                })
                ->get()->toArray();

            foreach ($orderDetail as $detail) {

                $cost += $detail['quantity'] * $detail['has_one_order_detail']['branch_item_detail']['amount'];
            }

            $res['total_cost'] = $cost;
            $res['total_srp'] = $res['total_srp'] ? $res['total_srp'] : 0;
            $res['total_balance'] = $res['total_balance'] ? $res['total_balance'] : 0;
        }

        return response()->json($response, 200);
    }

    public function inSummaryExport(Request $request) {
        $data = $request->all();
        $response = $this->branch
            ->with([
            ])
            ->withCount([
                'hasManyOrderHeader as total_srp'     => function ($query) use ($data) {

                    $query->with(['admin'])->whereIn('order_type_id', [1])
                    // $query->with(['admin'])->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);
                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }

                    $query->select(DB::raw("SUM(grand_total)"));
                },
                'hasManyOrderHeader as total_balance' => function ($query) use ($data) {
                    $query->with(['admin'])->whereIn('order_type_id', [1])
                    // $query->with(['admin'])->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [5]);

                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }

                    $query->select(DB::raw("SUM(grand_total)"));
                },
            ])
            ->get();
        foreach ($response as $key => $res) {
            $cost = 0;

            $orderDetail = $this->orderReference
                ->with([
                    'orderHeader',
                    'hasOneOrderDetail' => function ($query) {
                        $query->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
                    },
                ])
                ->whereHas('orderHeader', function ($query) use ($data, $res) {
                    $query->with(['admin'])
                        ->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5])
                        ->whereBranchId($res['id']);

                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereHas('admin', function ($query) use ($data) {
                            $query->whereBranchId($data['customer_id']);
                        });
                    }

                })
                ->get()->toArray();

            foreach ($orderDetail as $detail) {

                $cost += $detail['quantity'] * $detail['has_one_order_detail']['branch_item_detail']['amount'];
            }


            $res['total_cost'] = $cost;
            $res['total_srp'] = $res['total_srp'] ? $res['total_srp'] : 0;
            $res['total_balance'] = $res['total_balance'] ? $res['total_balance'] : 0;
        }

        return Excel::download(new InSummaryExport($response->toArray()), $data['file_name'] . '.csv');

    }

//    out


    public function out(Request $request) {

        $data = $request->all();


        $response = $this->orderReference->with([
            'item'              => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            },
            'hasOneOrderDetail' => function ($q) {
                $q->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
            },
            'orderHeader',
        //old doubled the query of hasOneOrderDetail
        // ])->has('hasOneOrderDetail')
            ])->whereHas('orderHeader', function ($query) use ($data) {
            $query->with(['admin'])
                ->whereIn('order_type_id', [1, 2])
                ->whereStatusOptionId(10)
                ->whereIn('payment_status_option_id', [4, 5]);

            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereHas('admin', function ($query) use ($data) {
                    $query->whereBranchId($data['customer_id']);
                });
            }
            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->where('invoice_number', 'LIKE', '%' . $data['invoice_number'] . '%');
            }

            if (isset($data['date_from']) && isset($data['date_to'])) {
                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
            }


        });

        if (isset($data['item_name']) && $data['item_name']) {
            $response = $response->whereHas('item', function ($query) use ($data) {
                if (isset($data['item_name']) && $data['item_name']) {
                    $query->where('name', 'LIKE', '%' . $data['item_name'] . '%');
                }
            });
        }

        $response = $response->orderBy('created_at', 'DESC')->paginate(10);

        $balance = $this->withBalance($data);

        $response->getCollection()->transform(function ($data) use ($balance) {
            $array = $data->toArray();
            $array['cost'] = $array['has_one_order_detail']['branch_item_detail']['amount'];
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['payment_status_option_id'] = $array['order_header']['payment_status_option_id'];
            $array['order_type_id'] = $array['order_header']['order_type_id'];
            $array['item_name'] = $array['item']['name'];
            $array['balance'] = $balance ?  $array['order_header']['payment_status_option_id'] == 5 ? $array['order_header']['total'] : 0 : 0;
            return $array;
        });

        return response()->json($response, 200);
    }


    public function outExport(Request $request) {

        $data = $request->all();


        $response = $this->orderReference->with([
            'item'              => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            },
            'hasOneOrderDetail' => function ($q) {
                $q->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
            },
            'orderHeader',

        ])->has('hasOneOrderDetail')
            ->whereHas('orderHeader', function ($query) use ($data) {
            $query->with(['admin'])
                ->whereIn('order_type_id', [1, 2])
                ->whereStatusOptionId(10)
                ->whereIn('payment_status_option_id', [4, 5]);

            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereHas('admin', function ($query) use ($data) {
                    $query->whereBranchId($data['customer_id']);
                });
            }
            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->where('invoice_number', 'LIKE', '%' . $data['invoice_number'] . '%');
            }

            if (isset($data['date_from']) && isset($data['date_to'])) {
                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
            }


        });

        if (isset($data['item_name']) && $data['item_name']) {
            $response = $response->whereHas('item', function ($query) use ($data) {
                if (isset($data['item_name']) && $data['item_name']) {
                    $query->where('name', 'LIKE', '%' . $data['item_name'] . '%');
                }
            });
        }

        $response = $response->orderBy('created_at', 'DESC')->get()->toArray();
        $balance = $this->withBalance($data);

        $response = collect($response)->transform(function ($data) use ($balance) {
            $array = $data;
            $array['cost'] = $array['has_one_order_detail']['branch_item_detail']['amount'];
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['payment_status_option_id'] = $array['order_header']['payment_status_option_id'];
            $array['order_type_id'] = $array['order_header']['order_type_id'];
            $array['item_name'] = $array['item']['name'];
            $array['balance'] = $balance ?  $array['order_header']['payment_status_option_id'] == 5 ? $array['order_header']['total'] : 0 : 0;


            return $array;
        });

        return Excel::download(new OutExport($response->toArray()), $data['file_name'] . '.csv');
    }


    public function outSummary(Request $request) {
        $data = $request->all();
        $response = $this->branch->get();
        foreach ($response as $key => $res) {
            $cost = 0;
            $res['total_balance'] = 0;

            $orderDetail = $this->orderReference
                ->with([
                    'orderHeader',
                    'hasOneOrderDetail' => function ($query) {
                        $query->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
                    },
                ])
                ->whereHas('orderHeader', function ($query) use ($data, $res) {
                    $query->with(['admin'])
                        ->whereHas('admin', function ($query) use ($data, $res) {
                            $query->whereBranchId($res['id']);
                        })->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->whereBranchId($data['branch_id']);
                    }

                })
                ->get()->toArray();

            foreach ($orderDetail as $detail) {

                $cost += $detail['quantity'] * $detail['has_one_order_detail']['branch_item_detail']['amount'];
            }

            $srp = $this->orderHeader
                ->with(['admin'])
                ->whereHas('admin', function ($query) use ($data, $res) {
                    $query->whereBranchId($res['id']);
                // })->whereIn('order_type_id', [1])
                })->whereIn('order_type_id', [1,2])
                ->whereStatusOptionId(10);
            if (isset($data['branch_id']) && $data['branch_id']) {
                $srp = $srp->whereBranchId($data['branch_id']);
            }

            if($res->branch_type_id == 2){
                $res['total_cost'] = $cost;
                $res['total_srp'] = $srp->whereIn('payment_status_option_id', [4, 5])->get()->sum('grand_total');
                $res['total_balance'] = $srp->whereIn('payment_status_option_id', [5])->get()->sum('grand_total');
            } else {
                $res['total_srp'] = $srp->whereIn('payment_status_option_id', [4, 5])->get()->sum('grand_total');
            }
            
        }

        return response()->json($response, 200);
    }


    public function outSummaryExport(Request $request) {
        $data = $request->all();
        $response = $this->branch->get();
        foreach ($response as $key => $res) {
            $cost = 0;
            $res['total_balance'] = 0;

            $orderDetail = $this->orderReference
                ->with([
                    'orderHeader',
                    'hasOneOrderDetail' => function ($query) {
                        $query->with(['branchItemDetail'])->orderBy('created_at', 'DESC');
                    },
                ])
                ->whereHas('orderHeader', function ($query) use ($data, $res) {
                    $query->with(['admin'])
                        ->whereHas('admin', function ($query) use ($data, $res) {
                            $query->whereBranchId($res['id']);
                        })->whereIn('order_type_id', [1, 2])
                        ->whereStatusOptionId(10)
                        ->whereIn('payment_status_option_id', [4, 5]);

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->whereBranchId($data['branch_id']);
                    }

                })
                ->get()->toArray();

            foreach ($orderDetail as $detail) {

                $cost += $detail['quantity'] * $detail['has_one_order_detail']['branch_item_detail']['amount'];
            }


            $srp = $this->orderHeader
                ->with(['admin'])
                ->whereHas('admin', function ($query) use ($data, $res) {
                    $query->whereBranchId($res['id']);
                })->whereIn('order_type_id', [1, 2])
                ->whereStatusOptionId(10);

            if (isset($data['branch_id']) && $data['branch_id']) {
                $srp = $srp->whereBranchId($data['branch_id']);

            }

            // $res['total_cost'] = $cost;
            // $res['total_srp'] = $srp->whereIn('payment_status_option_id', [4, 5])->get()->sum('grand_total');
            // $res['total_balance'] = $srp->whereIn('payment_status_option_id', [5])->get()->sum('grand_total');
            if($res->branch_type_id == 2){
                $res['total_cost'] = $cost;
                $res['total_srp'] = $srp->whereIn('payment_status_option_id', [4, 5])->get()->sum('grand_total');
                $res['total_balance'] = $srp->whereIn('payment_status_option_id', [5])->get()->sum('grand_total');
            } else {
                $res['total_srp'] = $srp->whereIn('payment_status_option_id', [4, 5])->get()->sum('grand_total');
            }
        }

        return Excel::download(new OutSummaryExport($response->toArray()), $data['file_name'] . '.csv');
    }

}
