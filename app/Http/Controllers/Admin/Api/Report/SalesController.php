<?php

namespace App\Http\Controllers\Admin\Api\Report;


use App\Exports\Reports\Sales\PerBrandExport;
use App\Exports\Reports\Sales\PerItemExport;
use App\Exports\Reports\Sales\PerPriceExport;
use App\Item;
use App\OrderDetail;
use App\OrderHeader;
use App\OrderReference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SalesController extends Controller {
    //

    /**
     * @var OrderHeader
     */
    private $orderHeader;
    /**
     * @var OrderReference
     */
    private $orderReference;
    /**
     * @var OrderDetail
     */
    private $orderDetail;
    /**
     * @var Item
     */
    private $item;

    /**
     * SalesController constructor.
     * @param OrderHeader $orderHeader
     * @param OrderReference $orderReference
     * @param OrderDetail $orderDetail
     */
    public function __construct(OrderHeader $orderHeader,
                                OrderReference $orderReference,
                                OrderDetail $orderDetail, Item $item) {

        $this->orderHeader = $orderHeader;
        $this->orderReference = $orderReference;
        $this->orderDetail = $orderDetail;
        $this->item = $item;
    }

    public function perItem(Request $request) {
        $data = $request->all();
        // return $data['keyword'];
        $response = $this->orderReference->with([
            'orderHeader' => function ($query) use ($data) {
                $query->with(['user']);
            },
            'item'        => function ($query) use ($data) {
                $query->with(['package', 'hasOneItemColor']);
            },
        ])->whereHas('orderHeader', function ($query) use ($data) {
            $query->whereIn('order_type_id', [3])
                ->whereStatusOptionId(10);

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->whereInvoiceNumber($data['invoice_number']);
            }


            if (isset($data['date_to']) && isset($data['date_from'])) {

                $query->whereBetween('invoice_date', [date($data['date_from']), date($data['date_to'])]);

            }
            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereCustomerId($data['customer_id']);
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

        });

        if (isset($data['keyword']) && $data['keyword']) {
            $response->whereHas('item', function ($q) use ($data) {
                $q->where('name', 'LIKE', '%' . $data['keyword'] . '%');
            });
        }

        $response = $response->orderBy('created_at', 'DESC')->paginate(10);

        $response->getCollection()->transform(function ($data) {
            $array = $data->toArray();
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['invoice_date'] = $array['order_header']['invoice_date'];
            $array['item_name'] = $array['item']['name'];
            $array['item_color'] = $array['item']['has_one_item_color']['name'];
            $array['client_name'] = $array['order_header']['user']['name'];
            $array['unit'] = $array['item']['package']['name'];

            return $array;
        });

        return response()->json($response, 200);
    }


    public function perItemExport(Request $request) {
        $data = $request->all();

        $response = $this->orderReference->with([
            'orderHeader' => function ($query) use ($data) {
                $query->with(['user']);
            },
            'item'        => function ($query) use ($data) {
                $query->with(['package', 'hasOneItemColor']);
            },
        ])->whereHas('orderHeader', function ($query) use ($data) {
            $query->whereIn('order_type_id', [3])
                ->whereStatusOptionId(10);

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->whereInvoiceNumber($data['invoice_number']);
            }

            if (isset($data['date_to']) && isset($data['date_from'])) {

                $query->whereBetween('invoice_date', [date($data['date_from']), date($data['date_to'])]);

            }
            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereCustomerId($data['customer_id']);
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

        });


        $response = $response->orderBy('created_at', 'DESC')->get()->toArray();

        $response = collect($response)->transform(function ($data) {
            $array = $data;
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['invoice_date'] = $array['order_header']['invoice_date'];
            $array['item_name'] = $array['item']['name'];
            $array['item_color'] = $array['item']['has_one_item_color']['name'];
            $array['client_name'] = $array['order_header']['user']['name'];
            $array['unit'] = $array['item']['package']['name'];

            return $array;
        });

        return Excel::download(new PerItemExport($response->toArray()), $data['file_name'] . '.csv');;
    }


    public function perPrice(Request $request) {
        $data = $request->all();

        $response = $this->orderReference->with([
            'orderHeader' => function ($query) use ($data) {
                $query->with(['user']);
            },
            'item'        => function ($query) use ($data) {
                $query->with(['package']);
            },
        ])->whereHas('orderHeader', function ($query) use ($data) {
            $query->whereIn('order_type_id', [3])
                ->whereStatusOptionId(10);

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->whereInvoiceNumber($data['invoice_number']);
            }

            if (isset($data['date_to']) && isset($data['date_from'])) {

                $query->whereBetween('invoice_date', [date($data['date_from']), date($data['date_to'])]);

            }
            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereCustomerId($data['customer_id']);
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['user_type_id']) && $data['user_type_id']) {
                $query->whereHas('user', function ($query) use ($data) {
                    $query->whereUserTypeId($data['user_type_id']);
                });
            }
        });

        if (isset($data['keyword']) && $data['keyword']) {
            $response = $response->whereHas('item', function ($q) use ($data) {
                $q->where('name', 'LIKE', '%' . $data['keyword'] . '%');
            });
        }


        $response = $response->orderBy('created_at', 'DESC')->paginate(10);

        $response->getCollection()->transform(function ($data) {
            $array = $data->toArray();
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['invoice_date'] = $array['order_header']['invoice_date'];
            $array['item_name'] = $array['item']['name'];
            $array['description'] = $array['item']['description'];
            $array['client_name'] = $array['order_header']['user']['name'];
            $array['unit'] = $array['item']['package']['name'];

            return $array;
        });

        return response()->json($response, 200);
    }


    public function perPriceExport(Request $request) {
        $data = $request->all();

        $response = $this->orderReference->with([
            'orderHeader' => function ($query) use ($data) {
                $query->with(['user']);
            },
            'item'        => function ($query) use ($data) {
                $query->with(['package']);
            },
        ])->whereHas('orderHeader', function ($query) use ($data) {
            $query->whereIn('order_type_id', [3])
                ->whereStatusOptionId(10);

            if (isset($data['invoice_number']) && $data['invoice_number']) {
                $query->whereInvoiceNumber($data['invoice_number']);
            }

            if (isset($data['date_to']) && isset($data['date_from'])) {

                $query->whereBetween('invoice_date', [date($data['date_from']), date($data['date_to'])]);

            }
            if (isset($data['customer_id']) && $data['customer_id']) {
                $query->whereCustomerId($data['customer_id']);
            }

            if (isset($data['branch_id']) && $data['branch_id']) {
                $query->whereBranchId($data['branch_id']);
            }

            if (isset($data['user_type_id']) && $data['user_type_id']) {
                $query->whereHas('user', function ($query) use ($data) {
                    $query->whereUserTypeId($data['user_type_id']);
                });
            }

        });


        $response = $response->orderBy('created_at', 'DESC')->get()->toArray();

        $response = collect($response)->transform(function ($data) {
            $array = $data;
            $array['invoice_number'] = $array['order_header']['invoice_number'];
            $array['invoice_date'] = $array['order_header']['invoice_date'];
            $array['item_name'] = $array['item']['name'];
            $array['client_name'] = $array['order_header']['user']['name'];
            $array['unit'] = $array['item']['package']['name'];
            $array['description'] = $array['item']['description'];

            return $array;
        });

        // dd($response);
        return Excel::download(new PerPriceExport($response->toArray()), $data['file_name'] . '.csv');;
    }


    public function perBrandCatClient(Request $request) {
        $data = $request->all();


        $response = $this->item->with([
            'hasOneItemColor',
            'package',
            'hasManyOrderReference' => function ($query) use ($data) {
                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }
                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }
                    });
            },

        ])->withCount([
            'hasManyOrderReference as quantity' => function ($query) use ($data) {

                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }

                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }

                    })->select(DB::raw("SUM(quantity - return_quantity)"));
            },
            'hasManyOrderReference as amount'   => function ($query) use ($data) {

                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }

                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }

                    })->select(DB::raw("SUM(total)"));
            },
        ])
            ->whereHas('hasManyOrderReference', function ($query) use ($data) {
                $query->whereHas('orderHeader', function ($query) use ($data) {
                    $query->whereOrderTypeId(3);
                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereCustomerId($data['customer_id']);
                    }

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->whereBranchId($data['branch_id']);
                    }

                });
            });


        if (isset($data['brand_id']) && $data['brand_id']) {
            $response = $response->whereBrandId($data['brand_id']);
        }

        if (isset($data['category_header_id']) && $data['category_header_id']) {
            $response = $response->whereCategoryHeaderId($data['category_header_id']);
        }


        $response = $response->paginate(10);

        $response->getCollection()->transform(function ($data) {
            $array = $data->toArray();
            $array['amount'] = $array['amount'] ? $array['amount']  : 0;
            $array['quantity'] = $array['quantity'] ? $array['quantity']  : 0;
            $array['color'] = $array['has_one_item_color'] &&  $array['has_one_item_color']['name'] ? $array['has_one_item_color']['name']  :  'N/A';
            $array['package_name'] = $array['package'] &&  $array['package']['name'] ? $array['package']['name']  :  'N/A';
            return $array;
        });

        return $response;
    }


    public function perBrandCatClientExport(Request $request) {
        $data = $request->all();

        $response = $this->item->with([
            'hasOneItemColor',
            'package',
            'hasManyOrderReference' => function ($query) use ($data) {
                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }
                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }
                    });
            },

        ])->withCount([
            'hasManyOrderReference as quantity' => function ($query) use ($data) {

                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }

                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }

                    })->select(DB::raw("SUM(quantity)"));
            },
            'hasManyOrderReference as amount'   => function ($query) use ($data) {

                $query->with(['orderHeader'])
                    ->whereHas('orderHeader', function ($query) use ($data) {
                        $query->whereOrderTypeId(3);
                        if (isset($data['customer_id']) && $data['customer_id']) {
                            $query->whereCustomerId($data['customer_id']);
                        }

                        if (isset($data['branch_id']) && $data['branch_id']) {
                            $query->whereBranchId($data['branch_id']);
                        }

                    })->select(DB::raw("SUM(total)"));
            },
        ])
            ->whereHas('hasManyOrderReference', function ($query) use ($data) {
                $query->whereHas('orderHeader', function ($query) use ($data) {
                    $query->whereOrderTypeId(3);
                    if (isset($data['customer_id']) && $data['customer_id']) {
                        $query->whereCustomerId($data['customer_id']);
                    }

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->whereBranchId($data['branch_id']);
                    }

                });
            });


        if (isset($data['brand_id']) && $data['brand_id']) {
            $response = $response->whereBrandId($data['brand_id']);
        }

        if (isset($data['category_header_id']) && $data['category_header_id']) {
            $response = $response->whereCategoryHeaderId($data['category_header_id']);
        }


        $response = $response->get()->toArray();

        $response = collect($response)->transform(function ($data) {
            $array = $data;
            $array['amount'] = $array['amount'] ? $array['amount']  : 0;
            $array['quantity'] = $array['quantity'] ? $array['quantity']  : 0;
            $array['color'] = $array['has_one_item_color'] &&  $array['has_one_item_color']['name'] ? $array['has_one_item_color']['name']  :  'N/A';
            $array['package_name'] = $array['package'] &&  $array['package']['name'] ? $array['package']['name']  :  'N/A';
            return $array;
        });

        return Excel::download(new PerBrandExport($response->toArray()), $data['file_name'] . '.csv');
    }

}
