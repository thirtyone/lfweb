<?php

namespace App\Http\Controllers\Admin\Api\Report;


use App\Brand;
use App\Exports\PerBrandSupplierExport;
use App\Exports\PerSupplierExport;
use App\SupplierOrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class PurchasesController extends Controller {

    private $supplierOrderDetail;

    private $brand;

    public function __construct(SupplierOrderDetail $supplierOrderDetail, Brand $brand) {
       $this->supplierOrderDetail = $supplierOrderDetail;
       $this->brand = $brand;
    }
    public function perSupplier(Request $request) {
        $data = $request->all();
        $response = $this->supplierOrderDetail->with([
            'supplierOrderHeader' => function($q) {
                $q->with(['supplier']);
            },
            'item' => function($query) {
                $query->with('hasOneItemColor');
            }])
            ->whereHas('supplierOrderHeader', function($query) use ($data) {
                if(isset($data['supplier_id']) && $data['supplier_id']) {
                    $query->whereHas('supplier', function($q) use($data) {
                        $q->where('id', $data['supplier_id']);
                    });
                }
                if(isset($data['keyword']) && $data['keyword']){
                    $query->where('invoice_number', $data['keyword']);
                }
                if(isset($data['date_from']) && isset($data['date_to'])) {
                    // $date_from = date('Y-m-d', strtotime($data['date_from']));
                    // $date_to = date('Y-m-d', strtotime($data['date_to']));
                    $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                }
            });

        $response = $response->paginate(10);
        $response->getCollection()->transform(function ($data) {
            $array = $data->toArray();
            $array['supplier_invoice_number'] = $array['supplier_order_header']['invoice_number'];
            $array['supplier_invoice_date'] = $array['supplier_order_header']['invoice_date'];
            $array['supplier_payment_terms'] = $array['supplier_order_header']['supplier']['payment_terms'];
            $array['supplier_name'] = $array['supplier_order_header']['supplier']['name'];
            $array['name'] =  $array['item']['name'];
            $array['item_color'] = $array['item']['has_one_item_color']['name'];
            $array['item_cost'] = $array['amount'];
            $array['package_id'] = $array['item']['package_id'];
            $array['total_amount'] = $array['quantity'] * $array['amount'];
            return $array;
        });


        return response()->json($response, 200);
    }

    public function perSupplierExport(Request $request) {
        $data = $request->all();

        $response = $this->supplierOrderDetail->with([
            'supplierOrderHeader' => function($q) {
                $q->with(['supplier']);
            },
            'item' => function($query) {
                $query->with('hasOneItemColor');
            }])
            ->whereHas('supplierOrderHeader', function($query) use ($data) {
                if(isset($data['supplier_id']) && $data['supplier_id']) {
                    $query->whereHas('supplier', function($q) use($data) {
                        $q->where('id', $data['supplier_id']);
                    });
                }

                if(isset($data['date_from']) && isset($data['date_to'])) {
                    /*$date_from = date('Y-m-d', strtotime($data['date_from']));
                    $date_to = date('Y-m-d', strtotime($data['date_to']));
                    $query->whereBetween('invoice_date', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);*/
                    $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                }
            });

        $response = $response->get()->toArray();

        $response = collect($response)->transform(function ($data) {
            $array = $data;
            $array['supplier_invoice_number'] = $array['supplier_order_header']['invoice_number'];
            $array['supplier_invoice_date'] = $array['supplier_order_header']['invoice_date'];
            $array['supplier_payment_terms'] = $array['supplier_order_header']['supplier']['payment_terms'];
            $array['supplier_name'] = $array['supplier_order_header']['supplier']['name'];
            $array['name'] =  $array['item']['name'];
            $array['item_color'] = $array['item']['has_one_item_color']['name'];
            $array['item_cost'] = $array['amount'];
            $array['package_id'] = $array['item']['package_id'];
            $array['total_amount'] = $array['quantity'] * $array['amount'];
            return $array;
        });

        return Excel::download(new PerSupplierExport($response->toArray()), 'per_supplier_' . date('Y-m-d') . '.csv');
    }

    public function perBrand(Request $request) {
        $data = $request->all();

        $response = $this->supplierOrderDetail->with([
            'supplierOrderHeader' => function($q) {
                $q->with(['supplier']);
            },
            'item' => function($query) {
                $query->with(['hasOneItemColor', 'category', 'brand']);
            }])
            ->whereHas('supplierOrderHeader', function($query) use ($data) {
                if(isset($data['supplier_id']) && $data['supplier_id']) {
                    $query->whereHas('supplier', function($q) use($data) {
                        $q->where('id', $data['supplier_id']);
                    });
                }

                if(isset($data['date_from']) && isset($data['date_to'])) {
                    $date_from = date('Y-m-d', strtotime($data['date_from']));
                    $date_to = date('Y-m-d', strtotime($data['date_to']));
                    $query->whereBetween('invoice_date', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
                }
            })
            ->whereHas('item', function($query) use ($data) {
                if(isset($data['brand_id']) && $data['brand_id']) {
                    $query->whereHas('brand', function($q) use ($data) {
                        $q->where('id', $data['brand_id']);
                    });
                }
            });

            $response = $response->paginate(10);

            $response->getCollection()->transform(function ($data) {
                $array = $data->toArray();
                $array['name'] =  $array['item']['name'];
                $array['color_name'] = $array['item']['has_one_item_color']['name'];
                $array['brand_name'] = $array['item']['brand']['name'];
                $array['category_name'] = $array['item']['category']['name'];;
                $array['supplier_invoice_number'] = $array['supplier_order_header']['invoice_number'];
                $array['supplier_invoice_date'] = $array['supplier_order_header']['invoice_date'];
                $array['package_id'] = $array['item']['package_id'];
                $array['supplier_name'] = $array['supplier_order_header']['supplier']['name'];
                $array['total_amount'] = $array['quantity'] * $array['amount'];
                return $array;
            });


        return response()->json($response, 200);
    }

    public function perBrandExport(Request $request) {
        $data = $request->all();

        $response = $this->supplierOrderDetail->with([
            'supplierOrderHeader' => function($q) {
                $q->with(['supplier']);
            },
            'item' => function($query) {
                $query->with(['hasOneItemColor', 'category', 'brand']);
            }])
            ->whereHas('supplierOrderHeader', function($query) use ($data) {
                if(isset($data['supplier_id']) && $data['supplier_id']) {
                    $query->whereHas('supplier', function($q) use($data) {
                        $q->where('id', $data['supplier_id']);
                    });
                }

                if(isset($data['date_from']) && isset($data['date_to'])) {
                    $date_from = date('Y-m-d', strtotime($data['date_from']));
                    $date_to = date('Y-m-d', strtotime($data['date_to']));
                    $query->whereBetween('invoice_date', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
                }
            })
            ->whereHas('item', function($query) use ($data) {
                if(isset($data['brand_id']) && $data['brand_id']) {
                    $query->whereHas('brand', function($q) use ($data) {
                        $q->where('id', $data['brand_id']);
                    });
                }
            });

        $response = $response->get()->toArray();

        $response = collect($response)->transform(function ($data) {
            $array = $data;
            $array['name'] =  $array['item']['name'];
            $array['color_name'] = $array['item']['has_one_item_color']['name'];
            $array['brand_name'] = $array['item']['brand']['name'];
            $array['category_name'] = $array['item']['category']['name'];;
            $array['supplier_invoice_number'] = $array['supplier_order_header']['invoice_number'];
            $array['supplier_invoice_date'] = $array['supplier_order_header']['invoice_date'];
            $array['package_id'] = $array['item']['package_id'];
            $array['supplier_name'] = $array['supplier_order_header']['supplier']['name'];
            $array['total_amount'] = $array['quantity'] * $array['amount'];
            return $array;
        });

        return Excel::download(new PerBrandSupplierExport($response->toArray()), 'per_brand_supplier_' . date('Y-m-d') . '.csv');

    }
}