<?php

namespace App\Http\Controllers\Admin\Api;

use App\History;
use App\Http\Controllers\BaseController;
use App\User;
use App\Item;
use App\Branch;
use App\OrderHeader;
use App\Http\Requests\Admin\BrandRequest;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryController extends BaseController {

	private $cmsRepository;
    /**
     * @var OrderHeader
     */
    private $orderHeader;
    private $item;
    private $branch;

	public function __construct(History $history, OrderHeader $orderHeader, Item $item, Branch $branch) {
		// set the model
		$this->cmsRepository = new CmsRepository($history);
        $this->orderHeader = $orderHeader;
        $this->item = $item;
        $this->branch = $branch;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) 
	{
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel();

		$response = $response->with('admin');

		if (isset($data['keyword'])) {


			$response = $response->where('action', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('created_at', 'desc')->paginate(10);
		foreach ($response as $key => $res) {
			
			if(isset($res->slug)){
				if($res->notification_id == 2){
					$link = '/auth/order/';
					$order = $this->orderHeader->find($res->slug);
					if($order['order_type_id'] == 1){
		                $link .= 'franchises/';
		                $link .= isset($order->id) ? $order->id.'/edit' : '';
		            } else if($order['order_type_id'] == 2){
		                $link .= 'inter-offices/';
		                $link .= isset($order->id) ? $order->id.'/edit' : '';
		            } else if($order['order_type_id'] == 3) {
		                $link .= 'clients/';
		                $link .= isset($order->id) ? $order->id.'/edit' : '';
		            } else if($order['order_type_id'] == 4) {
		                $link .= 'dr-samples/';
		                $link .= isset($order->id) ? $order->id.'/edit' : '';
		            } else {
		                $link .= 'quotations/';
		                $link .= isset($order->id) ? $order->id.'/edit' : '';
		            }
		            $res['link'] = $link;
				}
				if($res->notification_id == 1){
					$item = $this->item->find($res->slug);
					$res['link'] = '/auth/inventory/items/show/' . $res->slug .'?branch=' . $this->branch->find($res->branch_id)->slug;
				}
				
			}
		}

		return response()->json($response, 200);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$data = $request->all();
	
		$this->cmsRepository->create($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}


}
