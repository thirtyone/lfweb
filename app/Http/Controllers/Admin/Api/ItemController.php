<?php

namespace App\Http\Controllers\Admin\Api;

use App\Admin;
use App\Branch;
use App\BranchItemDetail;
use App\BranchItemHeader;
use App\Http\Requests\Admin\ItemRequest;
use App\Item;
use App\SrpHistory;
use App\Http\Controllers\BaseController;

use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ItemController extends BaseController {

    private $cmsRepository;
    /**
     * @var Item
     */
    private $item;
    /**
     * @var Branch
     */
    private $branch;
    /**
     * @var Admin
     */
    private $admin;
    /**
     * @var BranchItemHeader
     */
    private $branchItemHeader;
    /**
     * @var BranchItemDetail
     */
    private $branchItemDetail;
    /**
     * @var SrpHistory
     */
    private $srpHistory;

    public function __construct(SrpHistory $srpHistory, Item $item, Branch $branch, Admin $admin, BranchItemHeader $branchItemHeader, BranchItemDetail $branchItemDetail) {
        // set the model
        $this->cmsRepository = new CmsRepository($item);
        $this->srpHistory = $srpHistory;
        $this->item = $item;
        $this->branch = $branch;
        $this->admin = $admin;
        $this->branchItemHeader = $branchItemHeader;
        $this->branchItemDetail = $branchItemDetail;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        //
        $data = $request->all();

        $branchName = 'All';
        $response = $this->cmsRepository->getModel()->with([
            'categoryHeader',
            'brand',
            'supplier',
            'hasManySrpHistory',
            'hasManyItemColor',
            'hasManyBranchItemDetail' => function ($query) use ($data) {

//				if (isset($data['branch']) && $data['branch']) {
//
//					$query->whereHas('branch', function ($query) use ($data) {
//						$query->whereSlug($data['branch']);
//					});
//
//				}


                $query->with(['branch']);

            },
        ])
            ->withCount([
                'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) use ($data) {

                    if (isset($data['branch']) && $data['branch']) {

                        $query->whereHas('branch', function ($query) use ($data) {
                            $query->whereSlug($data['branch']);
                        });

                    }

                    $query->select(DB::raw("SUM(quantity)"));
                },
            ]);


        if (isset($data['keyword']) && $data['keyword']) {


            $response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
        }


        if (isset($data['status']) && json_encode($data['status']) && $data['status'] != 'All') {

            $response = $response->where('status', $data['status']);
        }

        if (isset($data['category']) && $data['category']) {


            $response = $response->whereHas('categoryHeader', function ($query) use ($data) {
                $query->whereSlug($data['category']);
            });
        }

        if (isset($data['brand']) && $data['brand']) {


            $response = $response->whereHas('brand', function ($query) use ($data) {
                $query->whereSlug($data['brand']);
            });
        }

        if (isset($data['supplier']) && $data['supplier']) {


            $response = $response->whereHas('supplier', function ($query) use ($data) {
                $query->whereSlug($data['supplier']);
            });
        }

        if (isset($data['branch']) && $data['branch']) {

            $branch = $this->branch->whereSlug($data['branch'])->first();

            $branchName = $branch ? $branch->name : 'All';

            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->whereHas('branch', function ($query) use ($data) {
                    $query->whereSlug($data['branch']);
                });
            });

        }

        if (!isset($data['zero_quantity']) || !json_decode($data['zero_quantity'])) {


            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->where('quantity', '!=', 0);
            });

        }


        $response = $response->orderBy('created_at', 'DESC')->paginate(isset($data['per_page']) ? $data['per_page'] : 10);
        $response->getCollection()->transform(function ($response) use ($branchName) {
            $colors = '';
            $response->branch_name = $branchName;
            $colors = '';
            foreach ($response->hasManyItemColor as $color) {
                $colors .= ', ' . $color->name;
            }
            $colors = substr($colors, 1);
            $response->colors = $colors ? $colors : '';

            return $response;
        });

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
//	    return view('admin.app');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request) {
        $data = $request->all();



        // $saveSrp = $this->srpHistory->create($srp);

        $itemColors = isset($data['has_many_item_color']) ? $data['has_many_item_color'] : [];
        unset($data['has_many_item_color']);

        $data['slug'] = $this->makeSlug($data['name'], 'App\Item');
        // return 1;
        $response = $this->cmsRepository->create($data);
        $response->hasManyItemColor()->createMany($itemColors);
        $response->hasManySrpHistory()->create($data);

        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

        $response = $this->cmsRepository->with(['hasManyItemColor', 'hasManySrpHistory', 'hasOneSupplierOrderDetail' => function ($q) {
            // $q->orderBy('amount', 'DESC')->first();
            $q->orderBy('id', 'DESC')->first();
        }])->find($id);


        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, $id) {
        //
        $data = $request->all();


        $itemColors = isset($data['has_many_item_color']) ? $data['has_many_item_color'] : [];
        unset($data['has_many_item_color']);

        $response = $this->cmsRepository->show($id);
        $response->fill($data)->save();
        $response->deleteChildData();
        $response->hasManyItemColor()->createMany($itemColors);
        $response->hasManySrpHistory()->create($data);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //

        $response = $this->cmsRepository->getModel()->whereId($id)->first();

        if ($response) {
            $inputs = [
                "status" => $response->status == 1 ? 0 : 1,
            ];


            $response->fill($inputs)->save();
        }


        return response()->json(true, 200);


    }


}
