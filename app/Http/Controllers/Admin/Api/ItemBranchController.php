<?php

namespace App\Http\Controllers\Admin\Api;

use App\BranchItemDetail;
use App\Branch;
use App\Item;

use App\Http\Controllers\BaseController;
use App\Notifications\Item\ItemThread;

use App\Repositories\Cms\CmsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ItemBranchController extends BaseController {

	private $cmsRepository;
	private $auth;
	private $branch;
	private $item;

	public function __construct(BranchItemDetail $branchItemDetail, Branch $branch, Item $item) {
		// set the model
		$this->cmsRepository = new CmsRepository($branchItemDetail);
		$this->auth = Auth::guard('admin')->user();
		$this->branch = $branch;
		$this->item = $item;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel()->with(['branch']);


		if (isset($data['branch']) && $data['branch']) {

			$response = $response->whereHas('branch', function ($query) use ($data){
				$query->whereSlug($data['branch']);
			});

		}


		$response = $response->paginate(isset($data['per_page']) ? $data['per_page'] : 10);


		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$data = $request->all();


		$response = $this->cmsRepository->create($data);


		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id, Request $request) {

		$data = $request->all();

		$response = $this->cmsRepository->getModel()->with(['branch'])->whereItemId($id);


		if (isset($data['branch']) && $data['branch']) {

			$response = $response->whereHas('branch', function ($query) use ($data) {
				$query->whereSlug($data['branch']);
			});

		}


		$response = $response->paginate(isset($data['per_page']) ? $data['per_page'] : 10);


		return response()->json($response, 200);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) 
	{
		//
		$data = $request->all();
		$response = $this->cmsRepository->show($id);
		$data['current_quantity'] = $response['quantity'];
		$response->fill($data)->save();
		$this->notification($data, $response);
		return response()->json($data, 200);
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}

	public function notification($data, $response){

		$item = $this->item->find($data['item_id']);
		//Notification and history message, slugs and notification type
		$response->message = $data['branch']['name'] . ' account with ' . $this->auth->name . ' user updated the quantity of item ' . $item->name . ' from ' . $data['current_quantity'] . ' to ' . $data['quantity'] . ' with item ID: ' . $data['id'];
		if($data['current_quantity'] == $data['quantity']){
			$response->message = $data['branch']['name'] . ' account with ' . $this->auth->name . ' user the quantity for item ' . $item->name . ' has not changed from ' . $data['current_quantity'] . ' to ' . $data['quantity'] . ' with item ID: ' . $data['id'];
		}
        $response->slug = $data['item_id'];
        $response->notification_id = 1;
        $response->branch_id = $data['branch_id'];
        //Generate Item history and notification
		$this->auth->notify(new ItemThread($response));
		$this->logs($response);
	}
}
