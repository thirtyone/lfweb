<?php

namespace App\Http\Controllers\Admin\Api;


use auth;
use App\Promo;
use Illuminate\Http\Request;
use App\Helpers\HistoryHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Cms\CmsRepository;
use App\Http\Controllers\BaseController;
use App\Notifications\TransactionThread;
use App\Http\Requests\Admin\PromoRequest;

class PromoController extends BaseController {

	private $cmsRepository;

	public function __construct(Promo $promo) {
		// set the model
		$this->cmsRepository = new CmsRepository($promo);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) {

		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->withTrashed()->orderBy('name', 'asc')->paginate(10);

		

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PromoRequest $request) {
		$data = $request->all();

		$this->cmsRepository->create($data);
		// auth()->guard('admin')->user()->notify(new TransactionThread());
		// auth()->guard('admin')->user();
		
		$data['message'] = "Created Promo " . $data['name'];

		$this->logs($data);

	
		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(PromoRequest $request, $id) {
		//
		$data = $request->all();
        $request->validate([
            'name'=>'required|unique:promos,name,'.$id.',id,deleted_at,NULL',
        ]);
		$this->cmsRepository->update($data, $id);

		$data['message'] = "Updated Promo " . $data['name'];

		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
		$data = ['message' => 'Deleted Promo '];

		$this->logs($data);

		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}

	public function activate($id){

		$response = Promo::withTrashed()->find($id);
		$data['message'] = "Activate Supplier " . $response['name'];
		$response->restore();
		$this->logs($data);

		return response()->json($data, 200);
	}


}
