<?php

namespace App\Http\Controllers\Admin\Api;

use App\BranchItemHeader;
use App\Http\Controllers\BaseController;

use App\Http\Requests\Admin\SupplierInvoiceRequest;
use App\Repositories\Cms\CmsRepository;
use App\SupplierOrderDetail;
use App\SupplierOrderHeader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierInvoiceController extends BaseController {

	private $cmsRepository;
	private $cmsBranchItems;


	public function __construct(SupplierOrderHeader $supplierOrderHeader, BranchItemHeader $branchItemHeader) {
		// set the model
		$this->cmsRepository = new CmsRepository($supplierOrderHeader);
		$this->cmsBranchItems = new CmsRepository($branchItemHeader);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$data = $request->all();
	
		$response = $this->cmsRepository->getModel()->with(['supplier', 'statusOption']);

		if (isset($data['keyword']) && $data['keyword']) {

			$response = $response->whereHas('supplier', function ($query) use ($data) {
				$query->where('name', 'LIKE', '%' . $data['keyword'] . '%');
			});
		}
		$response = $response
				->whereSupplierTypeId(1)
				->orderBy('id','DESC')
				->paginate(10);

		return response()->json($response, 200);
	}

	public function abroad(Request $request){
		$data = $request->all();
		$response = $this->cmsRepository->getModel()->with(['supplier', 'statusOption']);

		if (isset($data['keyword']) && $data['keyword']) {

			$response = $response->whereHas('supplier', function ($query) use ($data) {
				$query->where('name', 'LIKE', '%' . $data['keyword'] . '%');
			});
		}
		$response = $response
				->whereSupplierTypeId(2)
				->orderBy('id','DESC')
				->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(SupplierInvoiceRequest $request) {
		$data = $request->all();


		$response = $this->cmsRepository->create($data);
		$response->hasManySupplierOrderDetail()->createMany($data['has_many_supplier_order_detail']);

		$data['branch_id'] = $data['has_many_supplier_order_detail'][0]['branch_id'];
		$cmsBranchItems = $this->cmsBranchItems->create($data);
		$cmsBranchItems->hasManyBranchItemDetail()->createMany($data['has_many_supplier_order_detail']);

		return response()->json($data, 200);

//		return $data;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->with(['hasManySupplierOrderDetail' => function ($query) {
			$query->with(['item']);
		}])->find($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
		$data = $request->all();

		$this->cmsRepository->update($data, $id);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}
}
