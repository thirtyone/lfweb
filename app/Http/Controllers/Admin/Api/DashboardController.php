<?php

namespace App\Http\Controllers\Admin\Api;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {
	//

	private $branchId;
	/**
	 * @var Item
	 */
	private $item;

	public function __construct(Item $item) {

		$this->item = $item;
		$this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;
//		$this->branchId  =
	}

	public function getRestockItem() {


		$response = $this->item->with(['hasOneItemColor'])->withCount([
			'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) {
				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


				$query->select(DB::raw("SUM(quantity)"));
			},
			"hasManyBranchItemDetail"                                               => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			},
		])->orderBy('has_many_branch_item_detail_count_quantity', 'ASC')
			->havingRaw('has_many_branch_item_detail_count_quantity <= restock_at');
			// return $this->branchId;
		if ($this->branchId != 0) {
			// return '1';
			$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
				$query->whereBranchId($this->branchId);
			});
		}

		$response = $response->first();
		return response()->json($response, 200);
	}

	public function getRestockItems() {

		$response = $this->item->with(['hasOneItemColor'])->orderBy('id', 'DESC')->orderBy('name', 'ASC')->withCount([
			'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) {
				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


				$query->select(DB::raw("SUM(quantity)"));
			},
			"hasManyBranchItemDetail"                                               => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			},
		])->orderBy('has_many_branch_item_detail_count_quantity', 'ASC')
			->havingRaw('has_many_branch_item_detail_count_quantity <= restock_at');
			// return $this->branchId;
		if ($this->branchId != 0) {
			// return '1';
			$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
				$query->whereBranchId($this->branchId);
			});
		}

		$response = $response->get();
		
		return response()->json($response, 200);
	}

	public function getMostSupplyItem() {

		$response = $this->item->with(['hasOneItemColor',
			'hasManyBranchItemDetail'])
			->rightJoin('branch_item_details', 'branch_item_details.item_id', '=', 'items.id');
			if ($this->branchId != 0) {
			$response = $response->whereBranchId($this->branchId);
			}
			$response = $response->select(DB::raw('SUM(branch_item_details.quantity) as has_many_branch_item_detail_count_quantity'), 'items.*')
			->groupBy('items.name')
			->orderBy('has_many_branch_item_detail_count_quantity' ,'DESC');
			if ($this->branchId != 0) {

			$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
				$query->whereBranchId($this->branchId);
			});
		}

		$response = $response->first();


		return response()->json($response, 200);

		// $response = $this->item->with(['hasOneItemColor',
		// 	"hasManyBranchItemDetail" => function ($query) {

		// 		if ($this->branchId != 0) {
		// 			$query->whereBranchId($this->branchId);
		// 		}


		// 	},
		// ])
		// 	->rightJoin('branch_item_details', 'branch_item_details.item_id', '=', 'items.id')
		// 	->select(DB::raw('SUM(branch_item_details.quantity) as has_many_branch_item_detail_count_quantity'), 'items.*')
		// 	->orderBy('has_many_branch_item_detail_count_quantity', 'DESC');
		// 	if ($this->branchId != 0) {

		// 	$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
		// 		$query->whereBranchId($this->branchId);
		// 	});
		// }


		// $response = $response->first();


		// return response()->json($response, 200);
	}

	public function getMostSupplyItems() {

		$response = $this->item->with(['hasOneItemColor',
			'hasManyBranchItemDetail'])
			->rightJoin('branch_item_details', 'branch_item_details.item_id', '=', 'items.id');
			if ($this->branchId != 0) {
			$response = $response->whereBranchId($this->branchId);
			}
			$response = $response->select(DB::raw('SUM(branch_item_details.quantity) as has_many_branch_item_detail_count_quantity'), 'items.*')
			->groupBy('items.name')
			->orderBy('has_many_branch_item_detail_count_quantity' ,'DESC');
			if ($this->branchId != 0) {

			$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
				$query->whereBranchId($this->branchId);
			});
		}

		$response = $response->get()->take(50);


		return response()->json($response, 200);
	}

	public function getFastMovingItem() {
		//test
		$response = $this->item->with(['hasOneItemColor',
			"hasManyBranchItemDetail" => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			}])
			->join('branch_item_details', 'branch_item_details.item_id', 'items.id')
			->join('order_references', 'order_references.item_id', 'items.id')
			->join('order_headers', 'order_headers.id', 'order_references.order_header_id')
			->select(DB::raw('SUM(order_references.quantity) as has_many_order_reference_count_quantity'), 'items.*')
			
			->groupBy('items.name')
			->orderBy('has_many_order_reference_count_quantity', 'DESC');
			if ($this->branchId != 0) {

				$response = $response->whereRaw('order_headers.branch_id = '. $this->branchId);
			}

		$response = $response->first();


		return response()->json($response, 200);
	}

	public function getFastMovingItems() {
		//test
		$response = $this->item->with(['hasOneItemColor',
			"hasManyBranchItemDetail" => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			}])
			->join('branch_item_details', 'branch_item_details.item_id', 'items.id')
			->join('order_references', 'order_references.item_id', 'items.id')
			->join('order_headers', 'order_headers.id', 'order_references.order_header_id')
			->select(DB::raw('SUM(order_references.quantity) as has_many_order_reference_count_quantity'), 'items.*')
			->groupBy('items.name')
			->orderBy('has_many_order_reference_count_quantity', 'DESC');

			if ($this->branchId != 0) {

			$response = $response->whereRaw('order_headers.branch_id = '. $this->branchId);
		}

		$response = $response->get()->take(50);

		return response()->json($response, 200);
	}


}
