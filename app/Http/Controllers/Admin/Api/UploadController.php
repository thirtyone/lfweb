<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\BranchItemHeaderImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Supplier;
use Input;
use Response;
use File;
use Validator;

class UploadController extends Controller
{
    //

	protected $uploadPath;
	public function __construct(){

		$this->uploadPath = "images/uploads/";
	}

	public function importUpload(Request $request){

		$model = '\\App\\Imports\\' . $request->model;
		$request->validate([
            'file' => 'required'
        ]);
		Excel::import(new $model, request()->file('file'));
		// if($request->model === 'ItemsImport'){
		// 	Excel::import(new BranchItemHeaderImport, request()->file('file'));
		// }
		return back();
	}


	public function uploadImage(Request $request) {



		if (!File::exists($this->uploadPath)) {
			File::makeDirectory($this->uploadPath);
		}


		$file = Input::file('file');

		$name = $file->getClientOriginalName();
		$fileName = time() . $name;

		$file->move($this->uploadPath, $fileName);

		$res = array(
			'file_name' => $fileName,
			'path' => "/".$this->uploadPath
		);

		return Response::json($res);

	}

}
