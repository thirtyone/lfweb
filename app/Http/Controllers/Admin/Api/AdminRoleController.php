<?php

namespace App\Http\Controllers\Admin\Api;


use App\AdminRole;
use App\Http\Requests\Admin\AdminRequest;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminRoleController extends BaseController {

	private $cmsRepository;
	/**
	 * @var AdminRole
	 */
	private $adminRole;

	public function __construct(AdminRole $adminRole) {
		// set the model
		$this->cmsRepository = new CmsRepository($adminRole);
		$this->adminRole = $adminRole;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->withTrashed()->orderBy('id', 'asc')->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$data = $request->all();



		$this->cmsRepository->create($data);

//		$data['message'] = "Created User with username: " . $data['username'];
//
//		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *resources/modules/admin/components/order/franchise/crup-frachise-order.component.vue
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
		$data = $request->all();
		$this->cmsRepository->update($data, $id);

//		$data['message'] = "Updated User with username: " . $data['username'];
//
//		$this->logs($data);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$data = ['message' => 'Deleted User '];

		$this->logs($data);

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}

	public function activate($id){

		$response = AdminRole::withTrashed()->find($id);
		$data['message'] = "Activate Admin role " . $response['name'];
		$response->restore();
		$this->logs($data);

		return response()->json($data, 200);
	}
}
