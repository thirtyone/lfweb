<?php

namespace App\Http\Controllers;

use App\Exports\BrandExport;
use App\Exports\ClientExport;
use App\Exports\DrOrderExport;
use App\Exports\ItemSupplierExport;
use Carbon\Carbon;
use App\Exports\ItemsExport;
use App\Exports\OrderExport;
use App\SupplierOrderDetail;
use http\Client;
use Illuminate\Http\Request;

use App\Exports\ReturnExport;
use App\Exports\restockExport;
use App\Exports\SupplierExport;
use App\Exports\allClientExport;
use App\Exports\FranchiseExport;
use App\Exports\FastMovingExport;
use App\Exports\MostSupplyExport;
use App\Exports\TotalSalesExport;
use App\Exports\InterOfficeExport;
use App\Exports\PerSupplierExport;
use App\Exports\Price\PriceExport;
use App\Exports\SalePerColorExport;
use App\Exports\InventoryItemExport;

use App\Exports\Balance\LapseExport;
use App\Exports\SalesPerPriceExport;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\QuotationOrderExport;
use App\Exports\SupplierAbroadExport;
use App\Exports\Balance\BalanceExport;
use App\Exports\PerItemSupplierExport;
use App\Exports\SupplierSummaryExport;
use App\Exports\PerBrandSupplierExport;
use App\Exports\PerColorSupplierExport;
use App\Exports\Price\PriceSummaryExport;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Exports\InterOffice\ItemsInExport;
use App\Exports\InterOffice\ItemsOutExport;
use App\Exports\InterOffice\ItemsInSummaryExport;
use App\Exports\InterOffice\ItemsOutSummaryExport;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportController extends Controller {
    //

    /**
     * @var SupplierOrderDetail
     */


    public function __construct() {


    }

    public function dateToday() {
        return $date = Carbon::now()->format('m-d-Y');
    }


    public function drSamples(Request $request) {

        $data = $request->all();


        return Excel::download(new DrOrderExport($data), $data['filename'] . '_' . date('Y-m-d') . '_' . '.csv');
    }

    public function priceSummary(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'price-summary';

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new PriceSummaryExport($data), $filename . '_' . date('Y-m-d') . '_' . '.csv');
    }

    public function price(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'price';

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new PriceExport($data), $filename . '_' . date('Y-m-d') . '_' . '.csv');
    }

    public function returns(Request $request) {
        $data = $request->all();

        return Excel::download(new ReturnExport($data), 'returns_' . date('Y-m-d') . '.csv');
    }

    public function balance(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'walkin';

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new BalanceExport($data), $filename . '_' . date('Y-m-d') . '_' . '.csv');
    }

    public function balanceSummary(Request $request) {
        $data = $request->all();

        return Excel::download(new BalanceSummaryExport($data), 'balance_summary_' . date('Y-m-d') . '.csv');
    }

    public function lapse(Request $request) {
        $data = $request->all();

        return Excel::download(new LapseExport($data), 'lapsed_' . date('Y-m-d') . '.csv');
    }


    public function suppliers(Request $request) {

        $data = $request->all();

        return Excel::download(new SupplierExport($data), 'suppliers_' . date('Y-m-d') . '.csv');

    }

    public function supplierAbroad(Request $request) {

        $data = $request->all();

        return Excel::download(new SupplierAbroadExport($data), 'suppliers-abroad_' . date('Y-m-d') . '.csv');

    }

    public function brands(Request $request) {
        $data = $request->all();

        return Excel::download(new BrandExport($data), 'brands_' . date('Y-m-d') . '.csv');
    }

    public function itemSuppliers(Request $request) {
        $data = $request->all();

        return Excel::download(new ItemSupplierExport($data), 'item-suppliers' . date('Y-m-d') . '.csv');
    }

    public function items(Request $request) {


        $data = $request->all();

        return Excel::download(new ItemsExport($data), 'items_' . date('Y-m-d') . '.csv');

    }

    public function inventories(Request $request) {
        ini_set('memory_limit','5000M');
        $data = $request->all();

        return Excel::download(new ItemsExport($data), 'items_' . date('Y-m-d') . '.csv');

    }


    public function salesPerColor(Request $request) {

        $data = $request->all();

        return Excel::download(new SalePerColorExport($data), 'sale-per-color_' . date('Y-m-d') . '.csv');
    }

    public function orders(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'most-sold';

        return Excel::download(new OrderExport($data), $filename . '_' . date('Y-m-d') . '.csv');
    }

    public function interOffice(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'orders';

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new InterOfficeExport($data), $filename . '_' . date('Y-m-d') . '.csv');
    }

    public function franchise(Request $request) {
        $data = $request->all();
        $filename = isset($data['filename']) && $data['filename'] ? $data['filename'] : 'franchise';

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new FranchiseExport($data), $filename . '_' . date('Y-m-d') . '.csv');
    }

    public function totalSales(Request $request) {
        $data = $request->all();

//		return Excel::download(new OrderHeader($data), 'orders.csv');
        return Excel::download(new TotalSalesExport($data), 'total-sales_' . date('Y-m-d') . '.csv');
    }

    public function getAllClients(Request $request) {

        $data = $request->all();


        return Excel::download(new ClientExport($data), 'clients_' . date('Y-m-d') . '.csv');

    }

    public function restock(Request $request) {
        $data = $request->all();

        return Excel::download(new restockExport($data), 'restock_' . date('Y-m-d') . '.csv');
    }

    public function mostSupply(Request $request) {
        $data = $request->all();

        return Excel::download(new MostSupplyExport($data), 'most_supply_' . date('Y-m-d') . '.csv');
    }

    public function fastMoving(Request $request) {
        $data = $request->all();

        return Excel::download(new FastMovingExport($data), 'fast_moving_' . date('Y-m-d') . '.csv');
    }

    public function perBrandSupplier(Request $request) {
        $data = $request->all();

        return Excel::download(new PerBrandSupplierExport($data), 'per_brand_suplier_' . date('Y-m-d') . '.csv');
    }

    public function perColorSupplier(Request $request) {
        $data = $request->all();

        return Excel::download(new PerColorSupplierExport($data), 'per_color_supplier_' . date('Y-m-d') . '.csv');
    }

    public function perItemSupplier(Request $request) {
        $data = $request->all();

        return Excel::download(new PerItemSupplierExport($data), 'per_item_supplier_' . date('Y-m-d') . '.csv');
    }

    public function supplierSummary(Request $request) {
        $data = $request->all();

        return Excel::download(new SupplierSummaryExport($data), 'supplier_summary_' . date('Y-m-d') . '.csv');
    }

    public function quotationOrder(Request $request) {
        $data = $request->all();

//		$spreadsheet = new Spreadsheet();
//		$rowArray = [
//			['SKU', 'NAME', 'DESCRIPTION', 'QUANTITY','UNIT PRICE','TOTAL AMOUNT','IMAGE']
//		];
//		$spreadsheet->getActiveSheet()
//			->fromArray(
//				$rowArray,   // The data to set
//				NULL,        // Array values with this value will not be set
//				'A1'         // Top left coordinate of the worksheet range where
//			//    we want to set these values (default is A1)
//			);
//
//		$writer = new Xlsx($spreadsheet);
//
//		$response =  new StreamedResponse(
//			function () use ($writer) {
//				$writer->save('php://output');
//			}
//		);
//		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
//		$response->headers->set('Content-Disposition', 'attachment;filename="ExportScan.xls"');
//		$response->headers->set('Cache-Control','max-age=0');
//		return $response;
//		$spreadsheet = new Spreadsheet();
//		$sheet = $spreadsheet->getActiveSheet();
//
//		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
//		$drawing->setName('Paid');
//		$drawing->setDescription('Paid');
//		$drawing->setPath('images/logo_white.png'); // put your path and image here
//		$drawing->setCoordinates('A1');
//
//		$drawing->setWorksheet($spreadsheet->getActiveSheet());
//
//		$writer = new Xlsx($spreadsheet);
//		$writer->save('image.xlsx');

        return Excel::download(new QuotationOrderExport($data), 'quotation_order_' . $this->dateToday() . '.csv');

    }
}
