<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

use App\Branch;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller {
	//

	public function __construct(Branch $branch){
		$this->branch = $branch;
	}

    public function paginate($data, $perPage) {

        $collection = new Collection($data);

        $paginator = new Paginator($collection, $perPage);
        $response = collect(['total' => count($data)])->merge($paginator);

        return $response;
    }
	public function makeSlug($name, $model) {

		$m = App::make($model);

		$slug = str_slug($name);

		$count = $m->whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

		$s = $count ? "{$slug}-{$count}" : $slug;

		return $s;
	}

	public function addCookieToResponse(Request $request, array $data) {
	}


	public function generateOrderNumber($prefix, $type) {
		$data = App::make('App\OrderHeader')->whereOrderTypeId($type)->count();
		if ($data > 0) {
			$id = $data + 1;
		} else {
			$id = 1;
		}
		return $prefix . '-' . date("mdy") . '-' . $id;
	}

	public function logs($data){
		// return $data->message;

		$response = App::make('App\History');
		$response->admin_id = Auth::guard('admin')->user()->id;
		$response->action = $data['message'];
		$response->slug = $data['slug'];
		$response->branch_id = $data['branch_id'];
		$response->notification_id = $data['notification_id'];

		$response->save();
	}

	public function addDays($days,$date) {


		return date('Y-m-d H:i:s', strtotime('+30 day',$date));
	}


	public function getOrderType($action) {


		if ($action == 1) {
			$msg = "Franchise Order Created with Order Number: ";
		}
		if ($action == 2) {
			$msg = "InterOffice Order Created with Order Number: ";
		}
		if ($action == 3) {
			$msg = "Walkin Order Created with Order Number: ";
		}
		if ($action == 4) {
			$msg = "DR sample Created with Order Number: ";
		}
		if ($action == 5) {
			$msg = "Quotation Created with Order Number: ";
		}

		return $msg;
	}

	public function getItemNotification(){

	}

	// public function getNotification($paymentStatus, $action, $quantity, $customer, $branch, $orderNumber, $orderType, $customerId, $paymentMethod, $invoiceNumber, $preOrderSavePayment, $returnedItemsQuantity, $storePreOrderAction) {

	//Generate order notifications and history message with business logic
	public function getNotification($data) {
		$customer = $data['order_type_id'] != 3 && $data['order_type_id'] != 4 && $data['order_type_id'] != 5 ? App\Branch::find($data['customer_branch_id'])->name : App\User::find($data['customer_id'])->name;
		$branch = App\Branch::find($data['branch_id'])->name;
		$orderMsg = $this->orderType($data['order_type_id']);
		$user = App\Branch::find(Auth::guard('admin')->user()->branch_id)->name;
		$client = $branch == $user ? $customer : $branch;

		if ($data['status_option'] == 1 || $data['status_option'] == 10) {
			if($data['status_option'] == 10){
				$msgs = [
					'customer' => $branch . ' created ' . $orderMsg . ' with ' . $data['total_quantity'] . ' item/s for ' . $customer . ' with Order number: ' . $data['order_number'],
				];
			} else {
				$msgs = [
					'customer' => $customer . ' created ' . $orderMsg . ' with ' . $data['total_quantity'] . ' item/s to ' . $branch . ' with Order number: ' . $data['order_number'],
					'branch' => $customer . ' sent ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' to ' . $branch . ' with Order number: ' . $data['order_number']
				];
			}
			
		}

		if(isset($data['payment_method'])) {
			if($data['payment_method'] == 'None'){
				$msgs = [
					'customer' => $branch . ' with client ' . $branch . ' did not chose any payment note with Invoice number: ' . $data['invoice_number'],
				];
			} else {
				$msgs = [
					'customer' => $branch . ' with client ' . $customer . ' chose a  ' . $data['payment_method'] . ' Payment note with Invoice number: ' . $data['invoice_number'],
				];
			}
			
		}

		if ($data['status_option'] == 2) {
			$msgs = [
				'customer' => $branch . ' approved ' . $orderMsg . ' with ' . $data['total_quantity'] . ' item/s for ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if ($data['status_option'] == 16) {
			$msgs = [
				'customer' => $branch . ' with ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' for ' . $customer . ' are moved to Pre Order page with Order number: ' . $data['order_number'],
			];
		}

		if ($data['status_option'] == 8) {
			$msgs = [
				'customer' => $branch . ' sent ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' to ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if ($data['status_option'] == 3) {
			$msgs = [
				'customer' => $branch . ' rejected ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' in ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if ($data['status_option'] == 10 && $data['order_type_id'] != 3) {
			$msgs = [
				'branch' => $customer . ' received ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' to ' . $branch . ' with Order number: ' . $data['order_number'],
				'customer' => $customer . ' successfully received ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' to ' . $branch . ' with Order number: ' . $data['order_number']
			];
		}

		if($data['order_type_id'] == 4){
			$msgs = [
				'customer' => $user . ' created ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' for ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if($data['order_type_id'] == 5){
			$msgs = [
				'customer' => $user . ' created ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' for ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if($data['order_type_id'] == 4 && $data['status_option'] == 18){
			$msgs = [
				'customer' => $user . ' returned ' . $data['total_quantity'] . ' item/s of ' . $orderMsg . ' for ' . $customer . ' with Order number: ' . $data['order_number'],
			];
		}

		if (isset($data['payment_status'])) {
			$msgs = [
				'customer' => $user . ' mark as paid the  ' .  $orderMsg . ' to ' . $client . ' with Order number: ' . $data['order_number'],
			];
		}

		if(isset($data['returned_items_quantity'])){
			$msgs = [
				'customer' => $branch . ' with ' . $data['returned_items_quantity'] . ' item/s of ' . $orderMsg . ' for ' . $customer . ' was returned with Order number: ' . $data['order_number']
			];
		}

		if (isset($data['pre_order_save_payment'])) {
			$msgs = [
				'customer' => $user . ' with ' .  $data['total_quantity'] . ' item/s of ' . $orderMsg . ' for '. $client . ' are completed and moved to Walk in page with Order number: ' . $data['order_number'],
			];
		}

		if (isset($data['store_pre_order_items'])) {
			$msgs = [
				'customer' => $user . ' with ' .  $data['returned_items_quantity'] . ' item/s of ' . $orderMsg . ' for '. $client . ' are delivered with Order number: ' . $data['order_number'],
			];
		}

		return $msgs;
	}

	public function orderType($action){
		if ($action == 1) {
			$msg = "Franchise order";
		}
		if ($action == 2) {
			$msg = "Inter Office order";
		}
		if ($action == 3) {
			$msg = "Walkin order";
		}
		if ($action == 4) {
			$msg = "DR sample order";
		}
		if ($action == 5) {
			$msg = "Quotation order";
		}

		return $msg;
	}


	public function updateOrderType($action) {


		if ($action == 1) {
			$msg = "Franchise Order updated with Order Number: ";
		}
		if ($action == 2) {
			$msg = "InterOffice Order updated with Order Number: ";
		}
		if ($action == 3) {
			$msg = "Walkin Order updated with Order Number: ";
		}
		if ($action == 4) {
			$msg = "DR sample updated with Order Number: ";
		}
		if ($action == 5) {
			$msg = "Quotation updated with Order Number: ";
		}

		return $msg;
	}

}





//
//$lastId = InternalOrder::where('status','<',14)->count();
//if($lastId > 0){;
//	$autoIncrement = $lastId + 1;
//} else {
//	$autoIncrement = 1;
//}
//
//$orderNo = 'OR-'.date("mdy").'-'.$autoIncrement;
