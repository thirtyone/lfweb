<?php

namespace App\Http\Controllers;

use App\OrderHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrintController extends Controller {
	//


	/**
	 * @var OrderHeader
	 */
	private $orderHeader;

	public function __construct(OrderHeader $orderHeader) {

		$this->orderHeader = $orderHeader;
	}

	public function receipt(Request $request) {

		$data = $request->all();

		$response = $this->orderHeader->with(['branch','returnOrderHeader' => function ($query){
			$query->with(['hasManyReturnOrderDetail' => function ($query){
				$query->with(['item' => function ($query) {
					$query->with(['hasOneItemColor','package']);
				}]);
			}]);
		}, 
		'customer',
		'hasManyOrderReference' => function ($query) use ($data) {
			$query->with(['item' => function ($query){ $query->with(['hasOneItemColor','package']);}, 'hasManyOrderDetail' => function ($query) use ($data) {
				$query->whereOrderHeaderId($data['id']);
			}])
				->withCount([
					'hasManyOrderDetail as has_many_order_detail_count_quantity' => function ($query) use ($data) {

						$query->select(DB::raw("SUM(quantity)"))->whereOrderHeaderId($data['id']);
						
					},
					
				]);
		}])
		->withCount([
			'hasManyOrderReference'
		])
		->find($data['id']);

		$countItems = $response->has_many_order_reference_count;
		// dd($response->hasManyOrderReference[0]->item->hasOneItemColor->name);
		// dd($response->hasManyOrderReference->item->hasOneItemColor->name);
		if ($data['order_type'] == 2) {
			$view = "print.inter-office";
		}

		if ($data['order_type'] == 3) {


			if ($data['order_payment_type_id'] == 1) {
				
				// if($countItems > 7){
					// $view = "print.client-cdr-long";
				// }else{
					$view = "print.client-cdr";
				// }
					
			}

			if ($data['order_payment_type_id'] == 2) {
				if($countItems > 7){
					$view = "print.client-invoice-long";
				}else{
					$view = "print.client-invoice";
				}
				
			}

		}
		if ($data['order_type'] == 4) {
			$view = "print.dr-sample";


		}

		if ($data['order_type'] == 5) {
			$view = "print.quotation";


		}
		if ($data['order_type'] == 1) {
			$view = "print.franchise";


		}
		// echo var_dump($view);
		
		// return dd($response->hasManyOrderReference);
		return view($view, compact('response'));


	}
}
