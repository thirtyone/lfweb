<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin') {
        if (!Auth::guard($guard)->check()) {

            if (strpos($request->segment(1), 'api') !== false) {
                return response()->json(['status' => "error"], 401);
            }

            return redirect('/' . \Config::get('urlsegment.admin_prefix') . '/login');
        }

        return $next($request);
    }
}
