<?php

namespace App\Http\Middleware;

use App\BranchItemDetail;
use Closure;

class ValidateNegativeQuantity
{


	/**
	 * @var BranchItemDetail
	 */
	private $branchItemDetail;

	public function __construct(BranchItemDetail $branchItemDetail){

		$this->branchItemDetail = $branchItemDetail;
	}
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	$this->branchItemDetail->where('quantity', '<', 0)->update(['quantity' => 0]);

        return $next($request);
    }
}
