<?php

namespace App\Http\Middleware;

use Closure;
use http\Env\Request;

class Override
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        foreach ($request->all() as $key => $res){
            $request->all()[$key] =  str_replace('&', 'and', $res);
        }

        return $next($request);
    }
}
