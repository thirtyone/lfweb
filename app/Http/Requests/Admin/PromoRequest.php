<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PromoRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}





	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$data = [
			//
            "name" => 'required|unique:promos,name,NULL,id,deleted_at,NULL',
            "amount" => 'required',


		];
        if ($this->isMethod('put')){
            $data['name'] = 'required';
        }

		return $data;
	}
}
