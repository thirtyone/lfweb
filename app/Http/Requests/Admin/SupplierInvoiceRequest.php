<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SupplierInvoiceRequest extends FormRequest
{
	public function authorize() {
		return true;
	}

	public function messages() {


		$message = [
			'has_many_supplier_order_detail.*.quantity.not_in'   => 'The quantity field is invalid.',
			'has_many_supplier_order_detail.*.quantity.required' => 'The quantity field required.',
			'has_many_supplier_order_detail.*.amount.required' => 'The amount field is invalid.',
			'has_many_supplier_order_detail.*.amount.not_in' => 'Zero Quantity is Invalid.',
		];



		return $message;
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			'has_many_supplier_order_detail.*.quantity' => 'required|not_in:0',
			'has_many_supplier_order_detail.*.amount' => 'required|not_in:0',
			'invoice_number' => 'required|unique:supplier_order_headers',
		];


		return $rules;

	}
}
