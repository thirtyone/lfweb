<?php
namespace App\Http\Requests\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
	public function authorize()
	{
		return true;
	}




	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(Request $request)
	{
		// $user = User::find($this->users);

		// return [
		// 	//
		// 	'name' => 'required|unique:suppliers,name,' ,
		// 	'contact_number' => 'required',
		// 	'payment_terms' => 'required|integer',
		// 	'address' => 'required',

		// ];


		switch($this->method())
    {
        case 'GET':
        case 'DELETE':
        {
            return [];
        }
        case 'POST':
        {
            return [
				//
				'name' => 'required|unique:suppliers' ,
				'contact_number' => 'required',
				'payment_terms' => 'required|integer',
				'address' => 'required',
				'supplier_type_id' => 'required'
	
			];
        }
        case 'PUT':
        case 'PATCH':
        {
            return [
				//
				'name' => 'required|unique:suppliers,name,' . $this->get('id'),
				'contact_number' => 'required',
				'payment_terms' => 'required|integer',
				'address' => 'required',
	
			];
        }
        default:break;
    }
	}
}
