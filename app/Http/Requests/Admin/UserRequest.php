<?php
namespace App\Http\Requests\Admin;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

	public function messages()
	{
		return [

			"user_type_id.required" => 'The category field required.',
			'admin_id.required' => 'Sales agent is required.',
			'payment_terms.required' => 'Payment terms is required.',
			'name.unique' => 'Client name already exist.'

		];
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */

	public function rules()
	{
		$id = $this->route('post');
		$userId = $this->route('client');
		return [
			//
			"name" => 'required|unique:users,name,{$id},id,deleted_at,NULL'. $userId,
			"payment_terms" => 'required',
			"user_type_id" => 'required',
			"address" => 'required',
			"contact_number" => 'required|max:11',
			'admin_id' => 'required',

		];
	}
}
