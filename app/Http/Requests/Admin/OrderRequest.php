<?php

namespace App\Http\Requests\Admin;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	public function messages() {
		// return $request;

		$message = [
			"has_many_order_reference.*.quantity.not_in"   => 'The quantity field is invalid.',
			"has_many_order_reference.*.quantity.required" => 'The quantity field required.',
			"branch_id.required"                           => 'The branch field required.',
			"has_many_order_detail.required"			   => 'Item needs to be organized.',
		];

		foreach ($this->request->get('has_many_order_detail') as $key => $val) {
			$message[ 'has_many_order_detail.' . $key . '.quantity.not_in' ] = 'The quantity field is invalid.';
			$message[ 'has_many_order_detail.' . $key . '.quantity.required' ] = 'The quantity field required.';
		}

		if ($this->isMethod('put') || $this->request->get('order_type_id') == 3) {
			if($this->request->get('order_type_id') != 1){
				$message['has_many_order_detail.required'] = 'The orders required.';
				$message['has_many_order_detail.min'] = 'The orders required.';
			}
		}

		$message['has_many_order_reference.required'] = 'The orders required.';

		return $message;
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			"has_many_order_reference.*.quantity" => 'required|not_in:0',
			"has_many_order_reference.*.quantity" => 'required|not_in:0',
			"branch_id"                           => "required",
			"has_many_order_reference"            => "required|array|min:1",
			"customer_id"           			  => "required",
			'invoice_number' 					  => 'unique:order_headers',

		];
		
		if ($this->isMethod('put') || $this->request->get('order_type_id') == 3) {
			$rules['invoice_number'] = 'required|unique:order_headers,invoice_number,' . $this->get('id');
			if($this->request->get('order_type_id') != 1){
				$rules['has_many_order_detail'] = "required|array|min:1";
			}
			if ($this->request->get('status_option_id') == 3) {

				unset($rules['has_many_order_detail']);
				unset($rules['invoice_number']);
			}

			if ($this->request->get('status_option_id') == 2) {
				unset($rules['invoice_number']);
			}

			if ($this->request->get('for_payment')) {
				unset($rules['invoice_number']);
				unset($rules['has_many_order_detail']);
			}


		}

		if ($this->isMethod('put') || $this->request->get('order_type_id') == 1) {
			if($this->request->get('status_option_id') == 2){
				$rules['invoice_number'] = "required";
				$rules['has_many_order_detail'] = "required|array|min:1";
			}
		}


		foreach ($this->request->get('has_many_order_detail') as $key => $val) {
			// $rules[ 'has_many_order_detail.' . $key . '.quantity' ] = 'required|not_in:0';
			$rules[ 'has_many_order_detail.' . $key . '.quantity' ] = 'required';

		}

		return $rules;

	}
}
