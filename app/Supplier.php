<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Supplier extends Model
{
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    public function hasOneSupplierType(){
        return $this->hasOne('App\SupplierType', 'id', 'supplier_type_id');
    }

    public function hasManyItem(){
        return $this->hasMany('App\Item');
    }

    public function hasManySupplierOrderHeader(){
        return $this->hasMany('App\SupplierOrderHeader');
    }
}

