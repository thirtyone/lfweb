<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Item extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $guarded = ['id', 'created_at', 'updated_at'];

	protected $fillable = ['category_header_id',
							'category_detail_id',
							'package_id',
							'package_option',
							'supplier_id',
							'brand_id',
							'name',
							'restock_at',
							'sku',
							'code',
							'srp',
							'price_a',
							'price_b',
							'price_c',
							'price_d',
							'description',
							'slug',
                            'status'
						];
//
//	protected $appends = ['colors'];

	public function hasManySrpHistory(){
		return $this->hasMany('App\SrpHistory');
	}

	public function categoryHeader() {
	    return $this->belongsTo('App\CategoryHeader');
	}


	public function supplier() {
	    return $this->belongsTo('App\Supplier');
	}


	public function brand() {
	    return $this->belongsTo('App\Brand');
	}

	public function category(){
		return $this->belongsTo('App\CategoryHeader', 'category_header_id', 'id');
	}

    public function hasManyOrderReference() {
        return $this->hasMany('App\OrderReference');
    }

    public function hasOneOrderReference() {
        return $this->hasOne('App\OrderReference');
    }

	public function hasOneBranchItemDetail() {
		return $this->hasOne('App\BranchItemDetail');
	}

	public function hasManyItemColor() {
	    return $this->hasMany('App\ItemColor');
	}

	public function hasOneItemColor() {
	    return $this->hasOne('App\ItemColor');
	}

	public function hasManyBranchItemDetail() {
	    return $this->hasMany('App\BranchItemDetail');
	}


	public function package(){
		return $this->belongsTo('App\Package');
	}

	public function hasOneSupplierOrderDetail(){
		return $this->hasOne('App\SupplierOrderDetail');
	}


	public function delete(){
		parent::delete();
		$this->hasManyItemColor()->delete();
	}

	public function deleteChildData(){
		$this->hasManyItemColor()->delete();
	}


//	public function getColorsAttribute() {
//
//		return $this->hasManyItemColor();
//	}


}
