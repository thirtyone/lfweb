<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Branch extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $guarded = ['id', 'created_at', 'updated_at'];


	public function branchType() {
	    return $this->belongsTo('App\BranchType');
	}

	public function hasManyBranchItemDetail() {
	    return $this->hasMany('App\BranchItemDetail');
	}




	public function hasManyOrderHeader() {
	    return $this->hasMany('App\OrderHeader');
	}


}
