<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BranchItemHeader extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
	protected $fillable = ['id', 'supplier_id','branch_id', 'inter_branch_id','inter_branch_id', 'status_option_id', 'deleted_at','created_at', 'updated_at'];

	public function hasManyBranchItemDetail() {
	    return $this->hasMany('App\BranchItemDetail');
	}
}
