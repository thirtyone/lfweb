<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnOrderHeader extends Model
{
    //

	protected $fillable = ["id","order_header_id","vat","total","grand_total","status_option_id","created_at","updated_at"];


	public function hasManyReturnOrderDetail() {
	    return $this->hasMany('App\ReturnOrderDetail');
	}

	public function statusOption() {
	    return $this->belongsTo('App\StatusOption');
	}

	public function orderHeader() {
	    return $this->belongsTo('App\OrderHeader');
	}

	public function delete(){

		parent::delete();

		$this->hasManyReturnOrderDetail()->delete();
	}

}
