<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPaymentNote extends Model {
    //
    protected $fillable = ['order_header_id', 'notes'];
}
