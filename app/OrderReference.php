<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class OrderReference extends Model {
    //
    use Compoships;

    protected $fillable = ["id", "item_id", "receive", "discount", "quantity", "amount", "total", "return_quantity", "comment", "deleted_at", "created_at", "updated_at"];


    public function item() {
        return $this->belongsTo('App\Item');
    }

    public function orderHeader() {
        return $this->belongsTo('App\OrderHeader');
    }

    public function hasManyOrderDetail() {
        return $this->hasMany('App\OrderDetail', 'item_id', 'item_id');
    }

    public function hasOneOrderDetail() {
        return $this->hasOne('App\OrderDetail', ["order_header_id", "item_id"], ["order_header_id", "item_id"]);
    }

//    public function getCreatedAtAttribute($date) {
//        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
//    }

}
