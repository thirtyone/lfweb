<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SupplierOrderHeader extends Model {
	//
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = ["id","admin_id","supplier_type_id","supplier_id","status_option_id","invoice_number","invoice_date","vat","total","sub_total","grand_total","notes","deleted_at","created_at","updated_at"];

	protected $appends = ['due_date'];


	public function supplier() {
		return $this->belongsTo('App\Supplier');
	}

	public function statusOption() {
		return $this->belongsTo('App\StatusOption');
	}

	public function hasManySupplierOrderDetail() {
		return $this->hasMany('App\SupplierOrderDetail');
	}


	public function getDueDateAttribute() {


		$date = $this->created_at->addDays($this->supplier ? $this->supplier->payment_terms : 0);

		$date = $date->format('Y-m-d');


		return $date;
	}


}
