<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SrpHistory extends Model
{
    protected $fillable = ['srp', 'price_a', 'price_b', 'price_c', 'price_d', 'item_id'];
}
