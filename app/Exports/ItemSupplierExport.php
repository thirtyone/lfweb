<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class ItemSupplierExport implements FromView {



    private $brand;
    private $supplier;

    public function __construct(Array $data) {
		$this->supplier = App::make('App\Supplier');
		$this->branch = App::make('App\Branch');
		$this->data = $data;
	}

	public function view(): View {
		$data = $this->data;

		$response = $this->supplier->all();

		return view('exports.item-suppliers', [
			'response' => $response,
		]);
	}
}
