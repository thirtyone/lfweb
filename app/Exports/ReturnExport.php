<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class ReturnExport implements FromView {

	private $data;
    private $returnOrderDetail;
    private $orderReference;


    public function __construct(Array $data) {
		$this->orderDetail = App::make('App\OrderDetail');
		$this->orderReference = App::make('App\OrderReference');
		$this->returnOrderDetail = App::make('App\ReturnOrderDetail');
		$this->branchId = (Auth::guard('admin')->user()->admin_role_id == 1
						|| Auth::guard('admin')->user()->admin_role_id == 2
						|| Auth::guard('admin')->user()->admin_role_id == 3)
						? 0 : Auth::guard('admin')->user()->branch_id;
		$this->data = $data;
	}

	public function view(): View {

		$data = $this->data;

        $response = [];
        $result = $this->returnOrderDetail->with(['Item','returnOrderHeader' => function($query) use ($data){
            $query->with(['orderHeader' => function($query) {
                $query->with(['user']);
            }]);
        }]);

        $result = $result->whereHas('returnOrderHeader',function ($query) use ($data){
            $query->whereStatusOptionId(15)
                ->whereHas('orderHeader', function ($query) use ($data){
                    if (isset($data['date_from']) && isset($data['date_to'])) {
                        $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                    }

                    if (isset($data['keyword']) && $data['keyword']) {
                        $query->where('invoice_number', 'LIKE', '%' . $data['keyword'] . '%');

                    }


                });


        });
        $result = $result->get();

        foreach($result as $res){
            $orderReference = $this->orderReference->whereItemId($res->item->id)->whereOrderHeaderId($res->returnOrderHeader->order_header_id)->first();

            $input = [
                "name" => $res->item->name,
                "quantity" => $res->quantity,
                "amount" => $res->amount,
                "invoice_number" => $res->returnOrderHeader->orderHeader->invoice_number,
                "invoice_date" => $res->returnOrderHeader->orderHeader->invoice_date,
                "id" => $res->returnOrderHeader->orderHeader->id,
                "client_name" => $res->returnOrderHeader->orderHeader->user->name,
                "srp_type" => $res->returnOrderHeader->orderHeader->user->user_type_id,
                "discount" => $orderReference->discount,
                "reference_amount" => $orderReference->amount,
                "package_id" =>$res->item->package_id
            ];

            $input["total_amount"] = 0;
            if ($input["discount"] > 0) {
                $input["total_amount"] = $input["reference_amount"] * $input["quantity"] - (($input["reference_amount"] * $input["quantity"]) / $input["discount"]);
            }
            if ($input["discount"] == 0) {
                $input["total_amount"] = $input["reference_amount"] * $input["quantity"];
            }

            array_push($response,$input);
        }

        usort($response, function($a, $b) {
            return $b['id'] <=> $a['id'];
        });

        $response = json_decode(json_encode($response), FALSE);

        return view('exports.return.returns', [
			'response' => $response,
		]);
	}
}
