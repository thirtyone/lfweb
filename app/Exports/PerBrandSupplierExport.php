<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PerBrandSupplierExport implements FromArray, WithHeadings, WithMapping
{	
	
    private $data;
    private $auth;


    public function __construct(Array $data) {

		$this->data = $data;
        $this->auth = Auth::guard('admin')->user();
		
	}

    public function hasCost() {

        if ($this->auth->admin_role_id == 1 || $this->auth->admin_role_id == 2 || $this->auth->admin_role_id == 3) {
            return true;
        }

        return false;
    }

    public function map($data): array {

        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column
        return [
            [

                $data['brand_name'],
                $data['category_name'],
                $data['color_name'] ? $data['color_name'] : 'N/A',
                $data['supplier_invoice_number'],
                $data['supplier_invoice_date'],
                $data['supplier_name'],
                $data['name'],
                $data['quantity'],
                 $data['package_id'] == '1' ? 'PC' : 'METER',
                number_format($data['total_amount'], 2, '.', ','),
            ],

        ];
    }

    public function headings(): array {
        return [

            'Brand',
            'Category',
            'Color',
            'Invoice#',
            'Invoice Date',
            'Supplier Name',
            'Item Name',
            'Quantity',
            'Unit',
            'Total Amount',
        ];
    }

            public function array(): array {

        return $this->data;
    }


}
