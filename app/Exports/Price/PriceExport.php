<?php

namespace App\Exports\Price;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class PriceExport implements FromView
{
	private $data;
	private $orderHeader;
    
    public function __construct(Array $data) {
		$this->orderHeader = App::make('App\OrderHeader');
		$this->data = $data;
		$this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

	}

    public function view(): View
    {
        $data = $this->data;
        

        $response = $this->orderHeader->with(['hasManyOrderDetail', 
												'user'
												])->whereHas('user', function($q) use ($data){
													$q->whereUserTypeId($data['srp_type']);
												});

			if(isset($data['client']) && $data['client']) {

				$response = $response->whereHas('user', function ($query) use ($data) {
					$query->where('name', $data['client']);
				});
				
			}
		$response = $response->where(function ($query) {
			if ($this->branchId != 0) {
				$query->where('order_headers.branch_id', $this->branchId);
			}
		});
		$response = $response->whereOrderTypeId([3])->get();

            return view('exports.price.price', [
                'response' => $response,
            ]);

       
    }
}