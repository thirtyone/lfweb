<?php

namespace App\Exports\Price;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class PriceSummaryExport implements FromView
{
	private $data;
	private $orderHeader;

    public function __construct(Array $data) {
		$this->client = App::make('App\User');
		$this->orderHeader = App::make('App\OrderHeader');
		$this->data = $data;
		$this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

	}

    public function view(): View
    {
        $data = $this->data;

        $response = $this->client;
        if (isset($data['srp_id']) && $data['srp_id']) {
            $response = $response->where('user_type_id', $data['srp_id']);
        }

        $response = $response->get();
        $count = $response->count();
        foreach ($response->toArray() as $key => $res) {

            $response[ $key ]['total_paid'] = $this->orderHeader->with(['user'])
                ->whereHas('user', function ($query) use ($res) {
                    $query->whereCustomerId($res['id']);
                })->whereCustomerType('users')->wherePaymentStatusOptionId(4)->where(function ($q) {
                    if ($this->branchId != 0) {
                        $q->whereBranchId($this->branchId);
                    }
                })->whereStatusOptionId(10);

            $response[ $key ]['total_balance'] = $this->orderHeader->with(['user'])
                ->whereHas('user', function ($query) use ($res) {
                    $query->whereCustomerId($res['id']);
                })->whereCustomerType('users')->wherePaymentStatusOptionId(5)->where(function ($q) {
                    if ($this->branchId != 0) {
                        $q->whereBranchId($this->branchId);
                    }
                })->whereStatusOptionId(10);

            if (isset($data['date_from']) && isset($data['date_to'])) {

                $response[ $key ]['total_paid'] = $response[ $key ]['total_paid']->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->get();
                $response[ $key ]['total_balance'] = $response[ $key ]['total_balance']->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->get();
            }

            $response[ $key ]['total_paid'] = (int) $response[ $key ]['total_paid']->sum('grand_total');
            $response[ $key ]['total_balance'] = $response[ $key ]['total_balance']->sum('grand_total');
        }

        $inputs = [

        ];
        foreach ($response as $key => $res) {
            $arr = [
                "name"          => $res['name'],
                "credit_limit"  => $res['credit_limit'],
                "total_paid"    => $res['total_paid'],
                "total_balance" => $res['total_balance'],
                "payment_terms" => $res['payment_terms'],
                "id" => $res['id'],
            ];
            array_push($inputs,$arr);

        }

        usort($inputs, function($a, $b) {
            return $a['total_paid'] - $b['total_paid'];
        });
        $inputs = array_reverse($inputs);



		$collection = new Collection($inputs);



            return view('exports.price.price-summary', [
                'response' => $collection,
            ]);


    }
}
