<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class restockExport implements FromView
{
    
    private $item;
    private $branchId;

    public function __construct(){
        $this->item = App::make('App\Item');
        $this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;
    }

    public function view() : view
    {
        $response = $this->item->withCount([
			'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) {

				$query->select(DB::raw("SUM(quantity)"));
			},
		])->orderBy('has_many_branch_item_detail_count_quantity', 'ASC');
				// ->havingRaw('has_many_branch_item_detail_count_quantity <= restock_at');
        	// ->get();

        //filtered
        $response = $this->item->with(['hasOneItemColor'])->withCount([
			'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) {
				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


				$query->select(DB::raw("SUM(quantity)"));
			},
			"hasManyBranchItemDetail"                                               => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			},
		])->orderBy('has_many_branch_item_detail_count_quantity', 'ASC');
			// ->havingRaw('has_many_branch_item_detail_count_quantity < restock_at');
		if ($this->branchId != 0) {
			// return '1';
			$response = $response->whereHas("hasManyBranchItemDetail", function ($query){
				$query->whereBranchId($this->branchId);
			});
		}


		$response = $response->get();
		foreach ($response as $key => $value) {
			if($value->has_many_branch_item_detail_count_quantity == null){
				$response[$key]->has_many_branch_item_detail_count_quantity = 0;
			}
		}
        return view('exports.restock', [
            'response' => $response,
        ]);

    }
}
