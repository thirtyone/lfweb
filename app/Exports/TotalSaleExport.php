<?php

namespace App\Exports;

use App\OrderHeader;
use Maatwebsite\Excel\Concerns\FromQuery;

class TotalSaleExport implements FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {   
        $query = new OrderHeader();

        //TODO: mag query na naka sum yung total sale
        //! test query lang d pa nga nag reretrieve ng tama hahahaha
        $query->select('customer_type')->where('customer_id', '3');

        return $query;
    }
}
