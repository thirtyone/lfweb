<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;


class SupplierSummaryExport implements FromView {

	private $data;

	public function __construct(Array $data) {

		$this->branch = App::make('App\Branch');
		$this->data = $data;
	}

	public function view(): View {
		$data = $this->data;
        
		$response = DB::table('items')
                    ->join('brands', 'items.brand_id', 'brands.id')
                    ->join('category_headers', 'items.category_header_id', 'category_headers.id')
                    ->join('supplier_order_details', 'items.id', 'supplier_order_details.item_id')
                    ->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', 'supplier_order_headers.id')
                    ->join('suppliers', 'supplier_order_headers.supplier_id', 'suppliers.id')
                    ->select('items.*',
                        'supplier_order_details.quantity',
                        'suppliers.name as supplier_name',
                        'supplier_order_details.amount as item_cost',
                        DB::raw('SUM(supplier_order_details.quantity) as new_quantity'),
                        DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) / SUM(supplier_order_details.quantity) as avg_cost'),
                        DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) as amount'),
                        DB::raw('MAX(supplier_order_headers.created_at) as last_invoice_date'),
                        DB::raw('MIN(supplier_order_headers.created_at) as first_invoice_date')
                        );

                        if (isset($data['date_from']) && isset($data['date_to'])) {
                            $response = $response->select(
                                'items.*',
                                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) / SUM(supplier_order_details.quantity) as avg_cost'),
                                DB::raw('SUM(supplier_order_details.quantity) as new_quantity'),
                                DB::raw('MAX(supplier_order_headers.created_at) as last_invoice_date'),
                                DB::raw('MIN(supplier_order_headers.created_at) as first_invoice_date'),
                                DB::raw('SUM(supplier_order_details.quantity * supplier_order_details.amount) as amount')

                            )->whereBetween('supplier_order_headers.created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                        }

                    $response = $response->groupBy('items.name');

                    if (isset($data['supplier']) && $data['supplier']) {
                            $response = $response->where('suppliers.slug', $data['supplier']);
                        }
        
		$response = $response->get();


		return view('exports.supplier-summary', [
			'response' => $response,
		]);
	}
}
