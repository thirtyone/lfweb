<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class PerColorSupplierExport implements FromView
{
    private $data;

    public function __construct(Array $data) {
		$this->data = $data;
	}

    public function view(): View
    {
        $data = $this->data;

        $response = DB::table('items')
            ->join('item_colors', 'items.id', '=', 'item_colors.item_id')
            ->join('supplier_order_details', 'items.id', '=', 'supplier_order_details.item_id')
            ->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', '=', 'supplier_order_headers.id')
            ->select('items.*',
                'item_colors.name as item_color',
                'supplier_order_details.quantity as orderQuantity',
                DB::raw('supplier_order_details.quantity * supplier_order_details.amount as amount'))
            ->where('item_colors.deleted_at', null)
            ->groupBy('supplier_order_details.id');

        if (isset($data['keyword']) && $data['keyword']) {
            $response = $response->where('items.name', 'LIKE', '%' . $data['keyword'] . '%');
        }

        if (isset($data['date_from']) && isset($data['date_to']) && $data['date_from'] != 'null' && $data['date_to'] != 'null') {
            $date_from = date('Y-m-d', strtotime($data['date_from']));
            $date_to = date('Y-m-d', strtotime($data['date_to']));
            $response = $response->whereBetween('supplier_order_headers.created_at', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
        }

        $response = $response->get();


        return view('exports.per-color-supplier', [
			'response' => $response,
		]);
    }
}
