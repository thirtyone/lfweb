<?php

namespace App\Exports\Reports\InterOffice;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class InExport implements FromArray, WithHeadings, WithMapping {

    /**
     * @var array
     */
    private $data;
    private $auth;

    public function __construct(Array $data) {
        $this->data = $data;
        $this->auth = Auth::guard('admin')->user();
    }


    public function hasCost() {

        if ($this->auth->admin_role_id == 1 || $this->auth->admin_role_id == 2 || $this->auth->admin_role_id == 3) {
            return true;
        }

        return false;
    }

    public function map($data): array {

        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column
        return [
            [
                $data['created_at'],
                $data['invoice_number'],
                $data['item_name'],
                $data['quantity'],
                // number_format($this->hasCost() ? $data['cost'] : 0, 2, '.', ','),
                number_format($data['amount'], 2, '.', ','),
                number_format($data['discount']),
                // number_format($this->hasCost() ? $data['cost'] * $data['quantity'] : 0, 2, '.', ','),
                number_format($data['total'], 2, '.', ','),
                $data['order_type_id'] == 2 ? '0.00' : number_format($data['balance'], 2, '.', ','),

            ],

        ];
    }

    public function headings(): array {
        return [
            'Date',
            'Invoice Number',
            'Item Name',
            'Quantity',
            // 'Cost',
            'SRP',
            'Discount(%)',
            // 'Total Cost',
            'Total SRP',
            'Balance',
        ];
    }

    public function array(): array {

        return $this->data;
    }

}
