<?php

namespace App\Exports\Reports\InterOffice;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class InSummaryExport implements FromArray, WithHeadings, WithMapping {
    /**
     * @var array
     */
    private $data;
    private $auth;

    public function __construct(Array $data) {
        $this->data = $data;
        $this->auth = Auth::guard('admin')->user();
    }


    public function hasCost() {

        if ($this->auth->admin_role_id == 1 || $this->auth->admin_role_id == 2 || $this->auth->admin_role_id == 3) {
            return true;
        }

        return false;
    }
    public function map($data): array {

        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column

        if($this->hasCost()){
            $arr =   [
                $data['name'],
                number_format($this->hasCost() ? $data['total_cost'] : 0, 2, '.', ','),
                number_format($data['total_srp'], 2, '.', ','),
                number_format($data['total_balance'], 2, '.', ','),
            ];

        }else{
            $arr =   [
                $data['name'],
                number_format($data['total_srp'], 2, '.', ','),
                number_format($data['total_balance'], 2, '.', ','),
            ];
        }

        return [
            $arr
        ];
    }

    public function headings(): array {



        if($this->hasCost()){
                $arr = [
                    'Branch',
                    'Total Cost',
                    'Total Srp',
                    'Total Balance',
                ];

        }else{
            $arr = [
                'Branch',
                'Total Srp',
                'Total Balance',
            ];
        }

        return $arr;
    }

    public function array(): array {

        return $this->data;
    }

}
