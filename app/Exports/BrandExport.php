<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class BrandExport implements FromView {


	private $data;
    private $brand;

    public function __construct(Array $data) {
		$this->brand = App::make('App\Brand');
		$this->branch = App::make('App\Branch');
		$this->data = $data;
	}

	public function view(): View {
		$data = $this->data;

		$response = $this->brand->all();

		return view('exports.brand', [
			'response' => $response,
		]);
	}
}
