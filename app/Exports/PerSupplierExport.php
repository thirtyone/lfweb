<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PerSupplierExport implements FromArray, WithHeadings, WithMapping
{
    /**
     * @var array
     */
    private $data;
    private $auth;

    public function __construct(Array $data) {
        $this->data = $data;
        $this->auth = Auth::guard('admin')->user();
    }


    public function hasCost() {

        if ($this->auth->admin_role_id == 1 || $this->auth->admin_role_id == 2 || $this->auth->admin_role_id == 3) {
            return true;
        }

        return false;
    }

    public function map($data): array {

        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column
        return [
            [

                $data['supplier_invoice_number'],
                $data['supplier_invoice_date'],
                $data['supplier_payment_terms'],
                $data['supplier_name'],
                $data['quantity'],
                $data['package_id'] == '1' ? 'PC' : 'METER',
                $data['name'],
                $data['item_color'] ? $data['item_color'] : 'N/A' ,
                number_format($data['item_cost'], 2, '.', ','),
                number_format($data['total_amount'], 2, '.', ','),
            ],

        ];
    }

    public function headings(): array {
        return [

            'Invoice Number',
            'Invoice Date',
            'Terms',
            'Supplier Name',
            'Quantity',
            'Unit',
            'Item Name',
            'Color',
            'Unit Cost',
            'Total Amount',
        ];
    }

        public function array(): array {

        return $this->data;
    }

}
