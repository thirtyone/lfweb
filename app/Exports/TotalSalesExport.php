<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class TotalSalesExport implements FromView {


	private $data;

	public function __construct(Array $data) {
	
		$this->branch = App::make('App\Branch');
		$this->data = $data;
		$this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

	}

	public function view(): View {
		$data = $this->data;
		$response = $this->branch->with(['hasManyOrderHeader' => function ($query) use ($data) {
            $query->whereIn('order_type_id', [1, 3]);

        },

        ]);

        $response = $response->withCount([
            'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($q) {
                $q->select(DB::raw("SUM(grand_total)"))->whereIn('order_type_id', [1, 3])
                // ->whereStatusOptionId(10)
                ->whereIn('payment_status_option_id', [4, 5]);
            }
        ])->withCount([
            'hasManyOrderHeader as has_many_order_header_count_total' => function ($q) {
                $q->select(DB::raw("SUM(total)"))->whereIn('order_type_id', [1, 3])
                // ->whereStatusOptionId(10)
                ->whereIn('payment_status_option_id', [4, 5]);
            }
        ]);
        if (isset($data['date_from']) && isset($data['date_to'])) {
            $response = $response->withCount([
                'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($query) use ($data) {
                    $query->select(DB::raw("SUM(grand_total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3])
                    // ->whereStatusOptionId(10)
                    ->whereIn('payment_status_option_id', [4, 5]);
                },
            ])->withCount([
                'hasManyOrderHeader as has_many_order_header_count_total' => function ($query) use ($data) {
                    $query->select(DB::raw("SUM(total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3])
                    // ->whereStatusOptionId(10)
                    ->whereIn('payment_status_option_id', [4, 5]);
                },
            ]);
        }

        if(isset($data['date_from']) && isset($data['date_to'])){
             $response = $response->withCount([

            'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($query) use ($data) {
                $query->select(DB::raw("SUM(grand_total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3]);
            },
        ])->withCount([

            'hasManyOrderHeader as has_many_order_header_count_total' => function ($query) use ($data) {
                $query->select(DB::raw("SUM(total)"))->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"])->whereIn('order_type_id', [1, 3]);
            },

        ]);
        }
        
        if ($this->branchId != 0) {
            $response = $response->whereId($this->branchId);
        }
		
		$response = $response->get();

		return view('exports.total-sales', [
			'response' => $response,
		]);
	}
}