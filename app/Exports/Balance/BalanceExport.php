<?php

namespace App\Exports\Balance;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class BalanceExport implements FromView {
    private $data;
    private $orderHeader;

    public function __construct(Array $data) {
        $this->orderHeader = App::make('App\OrderHeader');
        $this->data = $data;
        $this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

    }

    public function view(): View {
        $data = $this->data;

        $response = $this->orderHeader->with(['hasManyOrderDetail' => function ($q) {
            $q->with(['item' => function ($q) {
                $q->with(['hasOneSupplierOrderDetail']);
            }]);
        }, 'user'])->whereStatusOptionId(10);


        $response = $response->whereHas('user', function ($query) use ($data) {

            $query->where('payment_terms','!=','');
            if (isset($data['srp']) && $data['srp']) {

                $query->where('user_type_id', $data['srp']);

            }
        });

        if (isset($data['date_from']) && isset($data['date_to'])) {
            // $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

            $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
        }
        $response = $response->where(function ($query) {
            if ($this->branchId != 0) {
                $query->where('order_headers.branch_id', $this->branchId);
            }
        });
        $res = $response->whereOrderTypeId([3])->has('user')->get()->toArray();

        foreach ($res as $key => $r) {
            $res[ $key ]['customer_name'] = $r['user']['name'];
        }
        usort($res, function ($a, $b) {
            return $a['customer_name'] <=> $b['customer_name'];
        });

//        return 1;

        $response = $res;

        return view('exports.balance.balance-summary', [
            'response' => $response,
        ]);


    }
}
