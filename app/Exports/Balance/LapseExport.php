<?php

namespace App\Exports\Balance;

use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class LapseExport implements FromView {
    private $data;
    private $orderHeader;

    public function __construct(Array $data) {
        $this->orderHeader = App::make('App\OrderHeader');
        $this->data = $data;
        $this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

    }

    public function view(): View {
        $data = $this->data;
        // $date_to = date_parse($data['date_to']);
        // $date_from = date_parse($data['date_from']);
        // $newDateTo = $date_to['year'] . '-' . $date_to['month'] . '-' . $date_to['day'];
        // $newDateFrom = $date_from['year'] . '-' . $date_from['month'] . '-' . $date_from['day'];
        $record = [];

        $response = $this->orderHeader->with(['user'])
            ->whereHas('user',function ($query) use ($data){

            })
            ->whereCustomerType('users')->whereStatusOptionId(10);

        if (isset($data['branch_id']) && $data['branch_id']) {
            $response = $response->whereBranchId($data['branch_id']);
        }


        if (isset($data['keyword']) && $data['keyword']) {

            $response = $response->whereHas('user', function ($query) use ($data) {

                $query->where('name', 'LIKE', '%' . $data['keyword'] . '%');

            });
        }

//
//		if (isset($data['date_to']) && isset($data['date_from'])) {
//
//			$response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);
//
//		}

        $response = $response->get();
        foreach ($response as $key => $res) {
            $response[ $key ]['due_date'] = $res->created_at->addDays($res->user ? $res->user->payment_terms : 1)->format('d-m-Y');
            if (strtotime(date("d-m-Y")) >= strtotime($response[ $key ]['due_date'])) {


                $due = new DateTime($response[ $key ]['due_date']);
                $res['before_due'] = $due->diff(new DateTime())->format("%a");

                array_push($record, $res);
            }

        }
        $tempRecord = [];
        if (isset($data['date_to']) && isset($data['date_from'])) {

            foreach ($record as $k => $d) {
                if ((strtotime($data['date_from']) <= strtotime($d['due_date']))) {

                    if ((strtotime($data['date_to'])) >= strtotime($d['due_date'])) {

                        array_push($tempRecord, $d);
                    }

                }
            }
            $record = $tempRecord;
        }

        usort($record, function ($a, $b) {
            return $b['before_due'] <=> $a  ['before_due'];
        });


        $response = $record;
        // $paginator = new Paginator($collection, 10);


        return view('exports.balance.lapse', [
            'response' => $response,
        ]);
    }
}
