<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

// use Maatwebsite\Excel\Concerns\FromCollection;

class FastMovingExport implements FromView
{
    private $item;
    private $branchId;
    
    public function __construct(){
        $this->item = App::make('App\Item');
        $this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;
       
    }
    public function view() : view
    {
        $response = $this->item->with(['hasOneItemColor',
			"hasManyBranchItemDetail" => function ($query) {

				if ($this->branchId != 0) {
					$query->whereBranchId($this->branchId);
				}


			}])
			->join('branch_item_details', 'branch_item_details.item_id', 'items.id')
			->join('order_references', 'order_references.item_id', 'items.id')
			->join('order_headers', 'order_headers.id', 'order_references.order_header_id')
			->select(DB::raw('SUM(order_references.quantity) as has_many_order_reference_count_quantity'), 'items.*')
			->groupBy('items.name')
			->orderBy('has_many_order_reference_count_quantity', 'DESC');

			if ($this->branchId != 0) {

			$response = $response->whereRaw('order_headers.branch_id = '. $this->branchId);
		}

		$response = $response->get()->take(50);
       
        return view('exports.fast-moving', [
            'response' => $response,
        ]);

    }
}
