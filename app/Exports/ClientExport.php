<?php

namespace App\Exports;

use App\Repositories\Cms\CmsRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;


/**
 * @property  supplierOrderDetail
 */
class ClientExport implements FromView
{


    private $user;

    private $cmsRepository;

    public function __construct(Array $data)
    {
        $this->user = App::make('App\User');

        $this->data = $data;
        $this->cmsRepository = new CmsRepository($this->user);

    }

    public function view(): View {
		$data = $this->data;

//		$response = $this->user->all();
        $response = $this->cmsRepository->getModel()->with(['admin', 'hasManyOrderHeader' => function ($query)
            {
                $query->where('payment_status_option_id', '!=', 4);
            }])->withCount([
                    'hasManyOrderHeader as has_many_order_header_count_grand_total' => function ($query) use ($data) {
                        $query->select(DB::raw("SUM(grand_total)"))->where('payment_status_option_id', '!=', 4);
            },])->whereHas('admin', function ($query) {
                if (Auth::guard('admin')->user()->branch_id != 1) {
                    $query->whereBranchId(Auth::guard('admin')->user()->branch_id);
                }
            });
            if (isset($data['keyword'])) {


                $response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
            }

            $response = $response->orderBy('name', 'asc')->get();


            foreach ($response as $key => $res) {
                $res->credit_status = true;
                if ($res->has_many_order_header_count_grand_total > $res->credit_limit) {
                    $res->credit_status = false;
                }

                foreach ($res->hasManyOrderHeader as $order) {
                    $order->payment_due_date = explode(' ', $order->created_at->addDays($res->payment_terms))[0];

                    if (date('Y-m-d') > $order->payment_due_date) {
                        $res->credit_status = false;
                    }
                }

            }

        return view('exports.client', [
            'response' => $response,
        ]);
    }
}
