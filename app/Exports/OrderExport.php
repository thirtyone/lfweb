<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class OrderExport implements FromView {

	private $data;
	private $orderDetail;

	/**
	 * ItemsExport constructor.
	 * @param array $data
	 */
	public function __construct(Array $data) {
		$this->orderDetail = App::make('App\OrderDetail');
		$this->branchId = (Auth::guard('admin')->user()->admin_role_id == 1 
						|| Auth::guard('admin')->user()->admin_role_id == 2 
						|| Auth::guard('admin')->user()->admin_role_id == 3) 
						? 0 : Auth::guard('admin')->user()->branch_id;
		$this->data = $data;
	}

	public function view(): View {

		$data = $this->data;
		
		// return dd($data['order_type_id']);
		$response = $this->orderDetail->with(['item',
            'orderHeader' => function ($q) {
                $q->with(['branch']);
            },
        ])->orderBy('quantity', 'DESC')
            ->whereHas('orderHeader', function ($q) use ($data) {
                $q->where(function ($q) use ($data) {
                    $q->whereOrderTypeId($data['order_type_id']);
                })->where(function ($q) use ($data) {
                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $q->whereBranchId($data['branch_id']);
                    }
                });
            });


            if (isset($data['keyword']) && $data['keyword']) {

                $response = $response->whereHas('item', function ($q) use ($data) {
                    $q->where('name', 'LIKE', '%' . $data['keyword'] . '%');
                });

            }

            if (isset($data['date_from']) && isset($data['date_to'])) {
                // $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);
                $date_from = date('Y-m-d', strtotime($data['date_from']));
                $date_to = date('Y-m-d', strtotime($data['date_to']));
                $response = $response->whereBetween('created_at', [$date_from . " 00:00:01", $date_to . " 23:59:59"]);
            }
		$response = $response->get();
		
		return view('exports.order', [
			'response' => $response,
		]);
	}
}