<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;


class SalePerColorExport implements FromView {

	private $item;
	private $data;
    /**
     * @var int
     */
    private $branchId;

    public function __construct(Array $data) {
		$this->item = App::make('App\Item');
		$this->branch = App::make('App\Branch');
		$this->data = $data;
		$this->branchId = Auth::guard('admin')->user()->admin_role_id == 1 ? 0 : Auth::guard('admin')->user()->branch_id;

	}

	public function view(): View {
		$data = $this->data;

        $data['branch_id'] = $this->branchId;
        $result = $this->item
            ->with(['hasOneItemColor'])
            ->withCount([
                'hasManyOrderReference as quantity' => function ($query) use ($data) {
                    $query->with(['orderHeader'])
                        ->whereHas('orderHeader', function ($query) use ($data) {
                            $query->where('order_type_id', '3');

                            if ($data['branch_id'] != 0) {
                                $query->where('branch_id', $data['branch_id']);
                            }
                            if (isset($data['date_from']) && isset($data['date_to'])) {

                                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                            }

                        })
                        ->select(DB::raw("SUM(quantity)"));
                },
                'hasManyOrderReference as grand_total' => function ($query) use ($data) {
                    $query->with(['orderHeader'])
                        ->whereHas('orderHeader', function ($query) use ($data){
                            $query->where('order_type_id', '3');

                            if($data['branch_id'] != 0){
                                $query->where('branch_id', $data['branch_id']);
                            }

                            if (isset($data['date_from']) && isset($data['date_to'])) {

                                $query->whereBetween('created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
                            }

                        })
                        ->select(DB::raw("SUM(total)"));
                }
            ])->having('quantity', '>', 0);
        if (isset($data['keyword']) && $data['keyword']) {
            $result = $result->where('name', 'LIKE', '%' . $data['keyword'] . '%');
        }

        $response = $result->get();

        foreach($response as $res){
            $res->color = isset($res->hasOneItemColor) && $res->hasOneItemColor ? $res->hasOneItemColor->name : '';
        }

		return view('exports.sales-per-color', [
			'response' => $response,
		]);
	}
}
