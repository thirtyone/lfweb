<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;


class SupplierExport implements FromView {

	private $data;

	public function __construct(Array $data) {

		$this->branch = App::make('App\Branch');
		$this->data = $data;
	}

	public function view(): View {
        $data = $this->data;

		$response = DB::table('supplier_order_headers')
            ->join('supplier_order_details', 'supplier_order_headers.id', '=', 'supplier_order_details.supplier_order_header_id')
            ->join('suppliers', 'supplier_order_headers.supplier_id', '=', 'suppliers.id')
            ->where(function ($query) use ($data) {
                $query->orWhere('suppliers.supplier_type_id', $data['supplier_type']);
            })
            ->groupBy('supplier_order_headers.invoice_number');


        if (isset($data['date_from']) && isset($data['date_to'])) {
            
            $response = $response->whereBetween('supplier_order_headers.created_at', [$data['date_from'] . " 00:00:01", $data['date_to'] . " 23:59:59"]);
        }

        if (isset($data['supplier']) && $data['supplier']) {
            $response = $response->where('suppliers.slug', $data['supplier']);
        }

		$response = $response->get();


		return view('exports.supplier', [
			'response' =>$response,
		]);
	}
}
