<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class PerItemSupplierExport implements FromView
{
    private $data;

    public function __construct(Array $data) {
		$this->data = $data;
	}

    public function view(): View
    {
        $data = $this->data;

        $response = DB::table('items')
						->join('brands', 'items.brand_id', 'brands.id')
						->join('category_headers', 'items.category_header_id', 'category_headers.id')
						->join('supplier_order_details', 'items.id', 'supplier_order_details.item_id')
						->join('supplier_order_headers', 'supplier_order_details.supplier_order_header_id', 'supplier_order_headers.id')
						->join('suppliers', 'supplier_order_headers.supplier_id', 'suppliers.id')
							->select('items.*',
									'brands.name as brand_name',
									'category_headers.name as category_name',
									'supplier_order_headers.invoice_number as supplier_invoice_number',
									'supplier_order_headers.invoice_date as supplier_invoice_date',
									'supplier_order_details.quantity',
									'suppliers.name as supplier_name',
									'suppliers.payment_terms as supplier_payment_terms',
									'supplier_order_details.amount as item_cost',
									DB::raw('supplier_order_details.quantity * supplier_order_details.amount as amount')
									);

			if (isset($data['keyword']) && $data['keyword']) {
				$response = $response->where('items.name', 'LIKE', '%' . $data['keyword'] . '%');
			}

        if (isset($data['date_to']) && isset($data['date_from'])) {



            $response = $response->whereBetween('supplier_order_headers.invoice_date', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

        }

        $response = $response->get();

        return view('exports.per-item-supplier', [
			'response' => $response,
		]);
    }
}
