<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class QuotationOrderExport implements FromView {


	private $order;
	private $data;
	private $orderHeader;

	/**
	 * ItemsExport constructor.
	 * @param array $data
	 */
	public function __construct(Array $data) {
		$this->orderHeader = App::make('App\OrderHeader');

		$this->data = $data;
	}

	public function view(): View {

		$data = $this->data;
		$response = $this->orderHeader->with([
			'hasManyOrderReference' => function ($query) {
				$query->with(['item' => function ($query) {
					$query->with(['hasOneItemColor']);
				}]);
			},
		])->find($data['id']);
		// dd($response);
		return view('exports.quotation-order', [
			'response' => $response,
		]);
	}
}