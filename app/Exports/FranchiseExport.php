<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class FranchiseExport implements FromView {


	private $order;
	private $data;
	private $orderHeader;

	/**
	 * ItemsExport constructor.
	 * @param array $data
	 */
	public function __construct(Array $data) {
		$this->orderHeader = App::make('App\OrderHeader');

		$this->data = $data;
	}

	public function view(): View {

		$data = $this->data;
		
		// $response = $response->whereOrderTypeId($data['order_type_id']);
		$response = $this->orderHeader->with([
			'customer',
			'branch',
			'admin' => function ($query) {
				$query->with(['branch']);
			},
			'statusOption',
			'paymentStatusOption',
			'orderPaymentType',

		])->whereOrderTypeId(1)->get(); 

		// $response = $response->whereOrderTypeId($data['order_type_id']);
		// if (isset($data['keyword'])) {


		// 	$response = $response->where('order_number', 'LIKE', '%' . $data['keyword'] . '%');

		// }

		// if (isset($data['payment_status_option_id']) && $data['payment_status_option_id']) {
		// 	$response = $response->wherePaymentStatusOptionId($data['payment_status_option_id']);
		// }

		// if (isset($data['branch_id']) && $data['branch_id']) {
		// 	$response = $response->whereBranchId($data['branch_id']);
		// }

		// if (isset($data['status_option_id']) && $data['status_option_id']) {
		// 	$response = $response->whereStatusOptionId($data['status_option_id']);
		// }


		// if (isset($data['date_to']) && isset($data['date_from'])) {

		// 	$response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

		// }

		return view('exports.franchise', [
			'response' => $response,
		]);
	}
}