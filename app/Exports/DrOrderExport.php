<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class DrOrderExport implements FromView {

	private $data;
	private $orderDetail;
    private $orderHeader;

    /**
	 * ItemsExport constructor.
	 * @param array $data
	 */
	public function __construct(Array $data) {
		$this->orderHeader= App::make('App\OrderHeader');

		$this->data = $data;
	}

	public function view(): View {

		$data = $this->data;
		$response = 	$this->orderHeader->with([
            'customer' => function ($query) use ($data) {

                if ($data['customer_type'] == "admins") {
                    $query->with(['branch']);
                } else {
                    $query->with(['userType']);
                }

            },
            'branch',
            'admin'    => function ($query) {
                $query->with(['branch']);
            },
            'statusOption',
            'paymentStatusOption',
            'orderPaymentType',
        ])->whereCustomerType($data['customer_type']);

        $response = $response->whereOrderTypeId($data['order_type_id']);
        if (isset($data['keyword'])) {


            $response = $response->where('invoice_number', 'LIKE', '%' . $data['keyword'] . '%');

        }

        if (isset($data['payment_status_option_id']) && $data['payment_status_option_id']) {
            $response = $response->wherePaymentStatusOptionId($data['payment_status_option_id']);
        }

        if (isset($data['status_option_id']) && $data['status_option_id']) {
            $response = $response->whereStatusOptionId($data['status_option_id']);
        }


        if ($data['customer_type'] == "admins") {


            $response = $response->where(function ($query) use ($data) {


                if (Auth::guard('admin')->user()->admin_role_id == 1) {

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->where('branch_id', $data['branch_id']);
                    }

                } else {

                    if (isset($data['branch_id']) && $data['branch_id']) {
                        $query->where('branch_id', $data['branch_id']);
                    }

                    $query->orWhereHas('admin', function ($query) use ($data) {
                        if (isset($data['customer_branch_id']) && $data['customer_branch_id']) {

                            $query->where('branch_id', $data['customer_branch_id']);
                        }

                    });


                }


            });

        }

        if ($data['customer_type'] == "users") {

            if (isset($data['branch_id']) && $data['branch_id']) {
                $response = $response->where('branch_id', $data['branch_id']);
            }

        }

        if (isset($data['date_to']) && isset($data['date_from'])) {

            $response = $response->whereBetween('created_at', [$data['date_from'] . " 00:00:00", $data['date_to'] . " 23:59:59"]);

        }


        $response = $response->orderBy('id', 'DESC')->get();
        
		return view('exports.dr-sample', [
			'response' => $response,
		]);
	}
}
