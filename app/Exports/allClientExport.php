<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class allClientExport implements FromView
{
    
    private $client;

    public function __construct(){
        $this->client = App::make('App\User');
    }

    public function view() : view
    {
        // $response = $this->client->where('user_type_id', '1')->get();
        $response = $this->client->with(['admin'])
                                 ->whereHas('admin', function ($query){
                                    if(Auth::guard('admin')->user()->branch_id != 1){
                                        $query->whereBranchId(Auth::guard('admin')->user()->branch_id);
                                    }
                                 });
        $response = $response->orderBy('name', 'ASC')->get();

      
        return view('exports.client', [
            'response' => $response,
        ]);

    }
}
