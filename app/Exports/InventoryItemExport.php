<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * @property  supplierOrderDetail
 */
class InventoryItemExport implements FromView {


	private $item;
	private $data;

	public function __construct(Array $data) {
		$this->item = App::make('App\Item');
		$this->branch = App::make('App\Branch');
		$this->data = $data;
	}

	public function view(): View {
		$data = $this->data;
		$branchName = 'All';

        $response = $this->item->with([
            'categoryHeader',
            'brand',
            'supplier',
            'hasManySrpHistory',
            'hasManyItemColor',
            'hasManyBranchItemDetail' => function ($query) use ($data) {

//				if (isset($data['branch']) && $data['branch']) {
//
//					$query->whereHas('branch', function ($query) use ($data) {
//						$query->whereSlug($data['branch']);
//					});
//
//				}


                $query->with(['branch']);

            },
        ])
            ->withCount([
                'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) use ($data) {

                    if (isset($data['branch']) && $data['branch']) {

                        $query->whereHas('branch', function ($query) use ($data) {
                            $query->whereSlug($data['branch']);
                        });

                    }

                    $query->select(DB::raw("SUM(quantity)"));
                },
            ]);


        if (isset($data['keyword']) && $data['keyword']) {


            $response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
        }


        if (isset($data['status']) && json_encode($data['status']) && $data['status'] != 'All') {

            $response = $response->where('status', $data['status']);
        }

        if (isset($data['category']) && $data['category']) {


            $response = $response->whereHas('categoryHeader', function ($query) use ($data) {
                $query->whereSlug($data['category']);
            });
        }

        if (isset($data['brand']) && $data['brand']) {


            $response = $response->whereHas('brand', function ($query) use ($data) {
                $query->whereSlug($data['brand']);
            });
        }

        if (isset($data['supplier']) && $data['supplier']) {


            $response = $response->whereHas('supplier', function ($query) use ($data) {
                $query->whereSlug($data['supplier']);
            });
        }

        if (isset($data['branch']) && $data['branch']) {

            $branch = $this->branch->whereSlug($data['branch'])->first();

            $branchName = $branch ? $branch->name : 'All';

            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->whereHas('branch', function ($query) use ($data) {
                    $query->whereSlug($data['branch']);
                });
            });

        }

        if (!isset($data['zero_quantity']) || !json_decode($data['zero_quantity'])) {


            $response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
                $query->where('quantity', '!=', 0);
            });

        }


        $response = $response->orderBy('created_at', 'DESC')->get();


        foreach ($response as $res){
            $colors = '';
            $res->branch_name = $branchName;
            foreach ($res->hasManyItemColor as $color) {
                $colors .= ', ' . $color->name;
            }
            $res->colors = $colors ? $colors : '';
        }

		return view('exports.item', [
			'response' => $response,
		]);
	}
}
