<?php namespace App\Repositories\Order;

use App\Repositories\Order\Contracts\OrderInterface;
use Illuminate\Support\Facades\App;


class OrderRepository implements OrderInterface
{


	private $branchItemDetail;

	public function __construct(){
		$this->branchItemDetail = App::make('App\BranchItemDetail');
	}


	public function validateIfItemStockExist($data, $branch) {

		$date = explode(' ', $data['created_at'])[0];
		//old
		// $response = $this->branchItemDetail->whereItemId($data['item_id'])->whereBranchId($branch)->whereAmount($data['amount'])->whereDate('created_at', $date)->first();

		//new
		$response = $this->branchItemDetail->whereItemId($data['item_id'])->whereBranchId($branch)->whereAmount($data['amount'])->first();
		if ($response) {
			$response->quantity = $data['quantity'] + $response->quantity;
			$response->save();
			return true;
		}
		return false;
	}



	public function removeBranchStockQuantity($quantity, $id) {

		$data = $this->branchItemDetail->find($id);
		if ($data->quantity >= $quantity) {
			$data->fill(['quantity' => $data->quantity - $quantity])->save();

			return true;
		}

	}



	public function addToLatestStock($itemId,$branch,$quantity) {

		$response = $this->branchItemDetail->whereItemId($itemId)->whereBranchId($branch)->first();
		$response->quantity = $response->quantity + $quantity;
		$response->save();
	}

	public function removeToLatestStock($itemId,$branch,$quantity) {

		$response = $this->branchItemDetail->whereItemId($itemId)->whereBranchId($branch)->get();
		
		foreach ($response as $key => $value) {
			if($value->quantity > 0 && $quantity > 0){
				$quantity = $quantity - $value->quantity;
				$value->quantity = $quantity >= 0 ? 0 : abs($quantity);
				$value->save();
			}
		}
		// $response->quantity = $response->quantity - $quantity;
		// $response->save();
	}


}