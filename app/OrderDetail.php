<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Awobaz\Compoships\Compoships;
class OrderDetail extends Model
{
    //
    use SoftDeletes;
    use Compoships;

    protected $dates = ['deleted_at'];

	protected $fillable = ["branch_item_detail_id","order_header_id","item_id","promo_id","discount","quantity","amount","total","return_quantity","comment","deleted_at","created_at","updated_at"];


	public function item() {
	    return $this->belongsTo('App\Item');
	}

	public function orderHeader(){
		return $this->belongsTo('App\OrderHeader');
	}

	public function branchItemDetail() {
	    return $this->belongsTo('App\BranchItemDetail');
	}

}
