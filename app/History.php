<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function admin(){
        return $this->belongsTo('App\Admin')->withTrashed();
    }
}
