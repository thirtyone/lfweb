<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ItemColor extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function item() {
	    return $this->belongsTo('App\Item');
	}
}
