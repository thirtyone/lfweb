<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderHeader extends Model {
	//
	use SoftDeletes;
	use Notifiable;

	protected $dates = ['deleted_at'];

	protected $fillable = ["id", "customer_type","customer_id", "order_type_id","payment_status_option_id","branch_id", "status_option_id", "order_number", "invoice_number", "invoice_date", "notes", "vat", "tin_number",  "total", "sub_total", "grand_total", "deleted_at", "created_at", "updated_at","order_payment_type_id","return_order_header_id","order_payment_mode_id","order_payment_notes","discount"];

    protected $appends = ["string_date"];

	public function branch() {
	    return $this->belongsTo('App\Branch');
	}
	public function statusOption() {
	    return $this->belongsTo('App\StatusOption');
	}
	public function paymentStatusOption() {
	    return $this->belongsTo('App\StatusOption');
	}
	public function orderPaymentType() {
	    return $this->belongsTo('App\OrderPaymentType');
	}

	public function hasManyOrderDetail() {
		return $this->hasMany('App\OrderDetail');
	}
	public function hasManyOrderReference() {
		return $this->hasMany('App\OrderReference');
	}
	public function customer()
	{
		return $this->morphTo()->withTrashed();
	}
	public function admin()
	{
		return $this->belongsTo('App\Admin','customer_id')->withTrashed();
	}
	public function user()
	{
		return $this->belongsTo('App\User','customer_id');
	}

	public function hasManyOrderReceivable() {
	    return $this->hasMany('App\OrderReceivable');
	}

	public function hasManyReturnOrderHeader() {
	    return $this->hasMany('App\ReturnOrderHeader');
	}

    public function hasManyOrderPaymentNote() {
        return $this->hasMany('App\OrderPaymentNote');
    }

	public function returnOrderHeader(){
		return $this->belongsTo('App\ReturnOrderHeader');
	}


	public function deleteChildData(){
		$this->hasManyOrderDetail()->delete();
		$this->hasManyOrderReference()->delete();
		$this->hasManyOrderPaymentNote()->delete();
	}

    public function getStringDateAttribute() {
        return date('Y-m-d g:i A',strtotime($this->attributes['created_at']));
    }

}
