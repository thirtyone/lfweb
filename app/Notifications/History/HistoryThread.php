<?php

namespace App\Notifications\History;

use App\Notifications\History\Channel\HistoryChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;


class HistoryThread extends Notification {
	use Queueable;
	protected $data;


	public function __construct($data) {
		$this->data = $data;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [HistoryChannel::class];
	}


	public function toDatabase($notifiable) {

		return $this->data;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
//			//
//			"notifiable_type" => "admins",
//			"notifiable_id"   => $this->orderHeader->id,
//			"data" =>2
		];
	}
}