<?php

namespace App\Notifications\History\Channel;

use App\History;
use Illuminate\Notifications\Notification;



class HistoryChannel {

	public function send($notifiable, History $history) {
		$data = $history->toDatabase($notifiable);


		return $notifiable->routeNotificationFor('database')->create([
			
			'message'     => isset($data['message']) ? $data['message'] : "",
		]);
	}

}