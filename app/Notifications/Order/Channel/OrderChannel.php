<?php

namespace App\Notifications\Order\Channel;

use Illuminate\Notifications\Notification;

class OrderChannel {

	public function send($notifiable, Notification $notification) {
		$data = $notification->toDatabase($notifiable);

		return $notifiable->routeNotificationFor('database')->create([
//			'id'          => $notification->id,
			"answer_type" => 'branches',
			"answer_id"   => $data['branch_id'],
			'type'        => get_class($notification),
			'data'        => $data,
			'message'     => isset($data['message']) ? $data['message'] : "",
			'status'      => "unread",
		]);
	}

}
