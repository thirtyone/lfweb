<?php

namespace App\Notifications\Item;

use App\Notifications\Item\Channel\ItemChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;


class ItemThread extends Notification {
	use Queueable;
	protected $data;


	public function __construct($data) {
		$this->data = $data;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [ItemChannel::class];
	}


	public function toDatabase($notifiable) {

		return $this->data;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable) 
	{
		return [

		];
	}
}
