<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SupplierOrderDetail extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
	protected $fillable = ['id', 'created_at', 'updated_at','item_id', 'supplier_order_header_id', 'branch_id','quantity', 'amount', 'deleted_at'];

	public function item() {
	    return $this->belongsTo('App\Item');
	}

	public function supplierOrderHeader() {
	    return $this->belongsTo('App\SupplierOrderHeader');
	}
}
