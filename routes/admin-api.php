<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$router->group(['prefix' => "upload"], function ($router) {

    $router->post('image', 'UploadController@uploadImage');
    $router->post('import', 'UploadController@importUpload');

});

$router->group(['prefix' => "dashboard"], function ($router) {

    $router->get('get-restock-item', 'DashboardController@getRestockItem');
    $router->get('get-restock-items', 'DashboardController@getRestockItems');
    $router->get('get-most-supply-item', 'DashboardController@getMostSupplyItem');
    $router->get('get-most-supply-items', 'DashboardController@getMostSupplyItems');
    $router->get('get-fast-moving-item', 'DashboardController@getFastMovingItem');
    $router->get('get-fast-moving-items', 'DashboardController@getFastMovingItems');

});


$router->get('agent-dashboards/orders', 'AgentDashboardController@indexOrder');
$router->post('my-profile/refresh-authorization', 'ProfileController@refreshAuthorization');


$router->resources([
    'brands'            => 'BrandController',
    'categories'        => 'CategoryController',
    'sub-categories'    => 'SubCategoryController',
    'items'             => 'ItemController',
    'item-branches'     => 'ItemBranchController',
    'suppliers'         => 'SupplierController',
    'clients'           => 'UserController',
    'branches'          => 'BranchController',
    'accounts'          => 'AccountController',
    'supplier-invoices' => 'SupplierInvoiceController',

    'orders'           => 'OrderController',
    'my-profile'       => 'ProfileController',
    'history-log'      => 'HistoryController',
    'promos'           => 'PromoController',
    'agent-dashboards' => 'AgentDashboardController',
    'admin-roles'      => 'AdminRoleController',
    'notifications'    => 'NotificationController',
]);

$router->get('supplier-invoices-abroad', 'SupplierInvoiceController@abroad');

$router->group(['prefix' => "orders"], function ($router) {
    $router->post('non-franchise-validate-quantities', 'OrderController@nonFranchiseValidateQuantities');
    $router->post('franchise-validate-quantities', 'OrderController@franchiseValidateQuantities');
    $router->post('non-franchise-orders', 'OrderController@nonFranchiseOrders');
    $router->post('order-to-transit', 'OrderController@orderToTransit');
    $router->post('order-to-receive', 'OrderController@orderToReceive');
    $router->post('franchise-orders', 'OrderController@franchiseOrders');
    $router->post('return-stock', 'OrderController@returnStock');
    $router->post('return-items-ticket', 'OrderController@returnItemsTicket');
    $router->post('update-return-items-ticket', 'OrderController@updateReturnItemsTicket');
    $router->put('update-dr-sample-status/{id}', 'OrderController@updateDrSampleStatus');
    $router->get('get-return-items-by-ticket-id/{id}', 'OrderController@showReturnItemsTicket');
    $router->delete('delete-return-items-ticket/{id}', 'OrderController@deleteReturnItemsTicket');
    $router->post('store-pre-order-item', 'OrderController@storePreOrderItem');

});

$router->group(['prefix' => "brands"], function ($router) {
    $router->post('activate/{id}', 'BrandController@activate');
});

$router->group(['prefix' => "categories"], function ($router) {
    $router->post('activate/{id}', 'CategoryController@activate');
});

$router->group(['prefix' => "suppliers"], function ($router) {
    $router->post('activate/{id}', 'SupplierController@activate');
});

$router->group(['prefix' => "accounts"], function ($router) {
    $router->post('activate/{id}', 'AccountController@activate');
});

$router->group(['prefix' => "admin-roles"], function ($router) {
    $router->post('activate/{id}', 'AdminRoleController@activate');
});

$router->group(['prefix' => "sub-categories"], function ($router) {
    $router->post('activate/{id}', 'SubCategoryController@activate');
});

$router->group(['prefix' => "promos"], function ($router) {
    $router->post('activate/{id}', 'PromoController@activate');
});

$router->group(['prefix' => "clients"], function ($router) {

    $router->get('/{id}/orders', 'UserController@orders');
    $router->post('activate/{id}', 'UserController@activate');
});


$router->group(['prefix' => "reports"], function ($router) {
    $router->get('/suppliers', 'ReportController@suppliers');
    $router->get('/suppliers-abroad', 'ReportController@suppliers');
    $router->get('/items', 'ReportController@items');
    $router->get('/orders', 'ReportController@orders');


    $router->get('/total-sale', 'ReportController@totalSale');
    $router->get('/sales-per-color', 'ReportController@salesPerColor');
    $router->get('/sales-per-category', 'ReportController@salesPerCategory');
    $router->get('/returns', 'ReportController@returns');



    $router->get('/per-item-supplier', 'ReportController@perItemSupplier');
    $router->get('/per-item-color-supplier', 'ReportController@perItemColorSupplier');
    $router->get('/supplier-summary', 'ReportController@supplierSummary');


    $router->group(['prefix' => "inter-office", "namespace" => "Report"], function ($router) {
        $router->get('items-in', 'InterOfficeController@in');
        $router->get('items-in-export', 'InterOfficeController@inExport');

        $router->get('items-out', 'InterOfficeController@out');
        $router->get('items-out-export', 'InterOfficeController@outExport');


        $router->get('items-in-summary', 'InterOfficeController@inSummary');
        $router->get('items-in-summary-export', 'InterOfficeController@inSummaryExport');


        $router->get('items-out-summary', 'InterOfficeController@outSummary');
        $router->get('items-out-summary-export', 'InterOfficeController@outSummaryExport');
    });


    $router->group(['prefix' => "sales", "namespace" => "Report"], function ($router) {
        $router->get('per-item', 'SalesController@perItem');
        $router->get('per-item-export', 'SalesController@perItemExport');




        $router->get('per-price', 'SalesController@perPrice');
        $router->get('per-price-export', 'SalesController@perPriceExport');




        $router->get('per-brand-cat-client', 'SalesController@perBrandCatClient');
        $router->get('per-brand-cat-client-export', 'SalesController@perBrandCatClientExport');
    });

    $router->group(['prefix' => "purchases", "namespace" => "Report"], function ($router) {
        $router->get('per-supplier', 'PurchasesController@perSupplier');
        $router->get('per-supplier-export', 'PurchasesController@perSupplierExport');

        $router->get('/per-brand', 'PurchasesController@perBrand');
        $router->get('/per-brand-export', 'PurchasesController@perBrandExport');

    });


    $router->group(['prefix' => "balance"], function ($router) {
        $router->get('lapsed', 'ReportController@lapsed');
        $router->get('walk-in', 'ReportController@balance');

    });

    $router->group(['prefix' => "price"], function ($router) {
        $router->get('summary', 'ReportController@priceSummary');
    });


    $router->get('/download', 'ExportController@testReport');



});





// $router->group(['prefix' => "cdr-order"], function($router){
//     $router->get('/download', 'ExportController@testReport');
// });



