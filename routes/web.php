<?php

use Maatwebsite\Excel\Facades\Excel;
use App\Supplier;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {



	// \Artisan::call('config:cache');
	// return redirect('/' . \Config::get('urlsegment.admin_prefix') . '/dashboard');
});

Auth::routes();

$router->group(['prefix' => 'print'], function ($router) {


	$router->get('receipt', 'PrintController@receipt');
});


$router->group(['prefix' => \Config::get('urlsegment.admin_prefix')], function ($router) {

	$router->get('clients/download','ExportController@getAllClients');

	$router->get('dashboard/restock-items/download','ExportController@restock');
	$router->get('dashboard/most-supply-items/download','ExportController@mostSupply');
	$router->get('dashboard/fast-moving-items/download','ExportController@fastMoving');
	$router->get('inventories/download','ExportController@inventories');


	$router->group(['prefix' => "reports"], function ($router) {
        $router->get('dr-samples-order/download','ExportController@drSamples');
		$router->get('suppliers/download','ExportController@suppliers');
		$router->get('brands/download','ExportController@brands');
		$router->get('item-suppliers/download','ExportController@itemSuppliers');
		$router->get('suppliers-abroad/download','ExportController@supplierAbroad');
		$router->get('items/download','ExportController@items');
		$router->get('orders/download','ExportController@orders');
		$router->get('inter-office/download','ExportController@interOffice');
		$router->get('franchise/download','ExportController@franchise');

		//sales report

		$router->get('total-sales/download','ExportController@totalSales');
		$router->get('sales-per-color/download','ExportController@salesPerColor');
		$router->get('returns/download','ExportController@returns');

		$router->get('per-item-supplier/download','ExportController@perItemSupplier');
		$router->get('per-item-color-supplier/download','ExportController@perColorSupplier');
		$router->get('supplier-summary/download', 'ExportController@supplierSummary');


		$router->group(['prefix' => 'balance'], function($router){
			$router->get('lapsed/download','ExportController@lapse');
			$router->get('walk-in/download','ExportController@balance');

		});

		$router->group(['prefix' => 'price'], function($router){
			$router->get('summary/download','ExportController@priceSummary');
		});



	});


	$router->group(['prefix' => "export"], function($router){
	    $router->get('quotation-order/download','ExportController@quotationOrder');
	});
});



$router->group(['prefix' => \Config::get('urlsegment.admin_prefix'), 'namespace' => 'Admin\Auth'], function ($router) {

	$router->get('/artisan-call/migrate', function () {
    	Artisan::call('migrate');
    	return 'Success';
 	});

	$router->get('login', 'LoginController@showLoginForm')->name(\Config::get('urlsegment.admin_prefix') . '.login');
	$router->post('login', 'LoginController@login');
	$router->get('logout', 'LoginController@logout');

//	$router->post('admin-password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//	$router->get('admin-password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//	$router->post('admin-password/reset', 'ResetPasswordController@reset');
//	$router->get('admin-password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
});

//
//Route::get('auth/reports/supplier/download', function () {
//	return Excel::download(new ItemsExport, 'items.csv');
//	// dd(Excel::download(new ItemsExport, 'items.csv'));
//});
//
//
//
//Route::get('auth/reports/inventory/download', function () {
//	return Excel::download(new SupplierExport, 'suppliers.csv');
//	// dd(Excel::download(new ItemsExport, 'items.csv'));
//});
//
//Route::get('auth/reports/invoice-order/download', function () {
//	return Excel::download(new InvoiceOrderExport, 'invoice-order.csv');
//	// dd(Excel::download(new ItemsExport, 'items.csv'));
//});
//
//Route::get('auth/reports/cdr-order/download', function () {
//	return Excel::download(new CdrOrderExport, 'cdr-order.csv');
//	// dd(Excel::download(new ItemsExport, 'items.csv'));
//});
//
//Route::get('auth/reports/total-sale/download', function () {
//	return Excel::download(new TotalSaleExport, 'total-sale.csv');
//	// dd(Excel::download(new ItemsExport, 'items.csv'));
//});

Route::get('/migrate', function () {
Artisan::call('migrate');
return 'Success';
});
