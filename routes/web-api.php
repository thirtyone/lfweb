<?php

use Illuminate\Http\Request;



$router->group(['prefix' => 'service'],function ($router){

    $router->get('get-categories','ServicesController@getCategoryHeaders');
    $router->get('get-sub-categories','ServicesController@getCategoryDetails');
    $router->get('get-packages','ServicesController@getPackages');
    $router->get('get-brands','ServicesController@getBrands');
    $router->get('get-users','ServicesController@getUsers');
    $router->get('get-suppliers','ServicesController@getSuppliers');
    $router->get('get-items','ServicesController@getItems');
    $router->get('get-item-by-stocks','ServicesController@getItemByStocks');
    $router->get('get-user-types','ServicesController@getUserTypes');
    $router->get('get-sales-agent','ServicesController@getSalesAgents');
    $router->get('get-branches','ServicesController@getBranches');
    $router->get('get-branch-types','ServicesController@getBranchTypes');
    $router->get('get-admin-roles','ServicesController@getAdminRoles');
    $router->get('get-status-options','ServicesController@getStatusOptions');
    $router->get('get-promos','ServicesController@getPromos');
    $router->get('get-payment-types','ServicesController@getPaymentTypes');
    $router->get('get-notifications','ServicesController@getNotifications');
    $router->get('get-payment-modes','ServicesController@getPaymentModes');
    $router->get('get-fr-type','ServicesController@getFrType');
    $router->get('get-supplier-type', 'ServicesController@getSupplierType');
    $router->get('get-unpaid-local', 'ServicesController@getUnpaidLocal');
    $router->get('get-unpaid-abroad', 'ServicesController@getUnpaidAbroad');
    $router->get('get-unpaid-client', 'ServicesController@getUnpaidClient');
    $router->get('get-colors','ServicesController@getColors');
    $router->get('get-users-by-filter','ServicesController@getUserByFilter');
});


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



